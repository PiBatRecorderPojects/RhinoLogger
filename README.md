# RhinoLogger

![RhinoLogger prototype 01](https://git.framasoft.org/PiBatRecorderPojects/RhinoLogger/raw/master/Etude%20Technique/DocsFabrication/RhinoLogger.PNG)
__Prototype 01__

__RhinoLogger__ est un système d'analyse en temps réel et en continue des émissions ultrasons des
rhinolophes. Il permet d'enregistrer les activités de 3 espèces (Grand Rhinolophe [GR], Rhinolophe
Euryale [RE] et Petit Rhinolophe [PR]) dans des fichiers CSV pour une analyse ultérieure. Compte tenu
du recouvrement entre RE et PR, une 4ème bande mémorise les activités dans la bande commune [EP].
Une cinquième bande (40-60kHz, [FM]) mémorise l'ensemble des émissions sonores de cette bande.

L'analyse de base est effectuée sur 200ms pour détecter les FC propres aux Rhinolophes. Une seconde est
déclarée positive si au moins une période de 200ms de cette seconde a détectée une émission dans une des
5 bandes. Une minute est déclarée positive si au moins une seconde de cette minute est déclarée positive.
La notion de « minute positive » est tirée du mémoire d'Alexandre HAQUART« ACTICHIRO -
REFERENTIEL D’ACTIVITE DES CHIROPTERES » (2013) :« ...La démarche prend également en
compte la nécessité de standardisation des méthodes de comptage déjà soulevé par MILLER (2001), en
utilisant la « Minute positive » comme unité de comptage. Celle-ci permet d’agréger des données issues
de différents matériels d’enregistrement et limite les biais de comptages liés à des facteurs abiotiques
(variations de température, d’hygrométrie, de matériel)... ».

Les résultats sont enregistrés sur une carte micro SD sous trois périodes temporelles :
* __Par minute :__ pour chaque minute, les fichiers CSV (un par jour) enregistrent le nombre de
secondes positives ainsi que la température et l'humidité mesurées à la fin de la minute.
* __Par heure :__ pour chaque heure, les fichiers CSV (un par jour) enregistrent le nombre de secondes
positives et le nombre de minutes positives ainsi que la température et l'humidité mesurées à la fin
de l'heure.
* __Par jour :__ pour chaque jour, le fichier CSV (un seul fichier par année) enregistre le nombre de
secondes positives et le nombre de minutes positives ainsi que la température minimale et
maximale et l'humidité minimale et maximale mesurées dans la journée. L’activité principale étant
nocturne, une journée commence à 12h00 pour se terminer à 11:59:59 le lendemain.

__Le projet est organisé selon les répertoires suivants :__

* __Fabrication d'un RhinoLogger__ Le répertoire __Frabrication__ contient l'ensemble des documents
expliquant comment le fabriquer. Les fichiers NomenclatureRhinoLogger.pdf et NomenclatureDevis.ods
listent les pièces nécessaires avec des liens vers un exemple de fournisseur mais ils ont une durée
de vie limitée. Le pdf montre des copies d'écran pour rechercher un nouveau fournisseur.

* __Exploitation d'un RhinoLogger__ Le répertoire __Exploitation__ regroupe le manuel d'utilisation
et des scripts R pour exploiter les données produites. Ces scripts sont paramétrable via des variables
à modifier dans l'entête de ces scripts.

* __Modification de RhinoLogger__ Les autres répertoires contiennent les fichiers permettant de faire
évoluer le matériel et le logiciel du RhinoLogger.

__Echanges entres utilisateurs :__

* __Liste de diffusion__ Les utilisateurs de RhinoLogger sont invités à s'abonner à la liste de diffusion
"Club_Utilisateurs_RhinoLogger" sur Framalistes pour recevoir des avis de modification de la part du concepteur
ou échanger entre utilisateurs pour partager leurs expériences.
Pour s'abonner, utilisez les liens ci-dessous :

S'abonner ➙ https://framalistes.org/sympa/subscribe/club_utilisateurs_rhinologger

Se désabonner ➙ https://framalistes.org/sympa/sigrequest/club_utilisateurs_rhinologger

Envoyer un message à chaque abonné, utilisez l'adresse : club_utilisateurs_rhinologger@framalistes.org

__Licence :__
__RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.__
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
