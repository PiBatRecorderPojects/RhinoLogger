Rem Fichier de commande pour telecharger une nouvelle version de RhinoLogger
Rem Utilisation :
Rem UpdateRhinoLogger.bat COM7 RhinoLoggerV0_9.bin
Rem Le fichier bin est à recuperer dans
Rem C:\Users\NomUser\AppData\Local\Temp\arduino_build_955640
bossac.exe -i -d --port=%1 -U true -e -w -v %2 -R