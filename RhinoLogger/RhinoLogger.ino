/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Programme de surveillance de l'activité des Grand Rhinolophe
// En suplément, surveillance de la bande FM (40-60kHz)
// des Rhinolophe Euryale et Petit Rhinolophe avec une bande commune
// Produit des fichiers CSV avec les activités à la minute, à l'heure et à la journée
// En option, émission d'un SMS journalier de synthèse
// Fonctionne avec la carte Feather M0 DataLogger de chez AdaFruit
/* ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION
 Il faut changer les options d'optimisation dans
 C:\Users\"nomuser"\AppData\Local\Arduino15\packages\adafruit\hardware\samd\1.0.17\platform.txt
 Remplacer -Os par -O3 (3 lignes) pour obtenir un gain de vitesse
 A réaliser à chaque nouvelle version AdaFruit
*/

#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <gfxfont.h>
#include "const.h"
#include "utilit.h"
#include "timer.h"
#include "gestMesure.h"
#include "GestScreen.h"

// Pour le test des touches clavier
//#define TESTCLAVIER
// Pour le test de la lecture des paramètres
//#define TEST_PARAMS

// Constantes de gestion date/heure via l'horloge interne au processeur
// Sauvegardées dans la mémoire flash du processeur
const byte seconds = 0;
const byte minutes = 0;
const byte hours   = 0;
const byte day     = 1;
const byte month   = 1;
const byte year    = 16;

// RTC interne au processeur et valeurs de l'heure et la date
RTCZero rtcZero;

// Mémo de la dernière frappe de SELECT en modification de paramètres pour éviter la répétition
long iLatsSelect = 0;

// Paramètres de fonctionnement avec les valeurs par défaut
StructParams params = { 5555, "RhinoLogger ", 20, "1234",  "          ", "          ", "          ", 5.2, 2.0, NORMAL, GAIN_4, "FM", 40.0, 76.0, 99.0, 102.6, 106.6, 60.0, 86.0, 102.5, 106.5, 117.0, 5, 15, 10, 10, 10, false, false, false, false, false};

// Nom des bandes
char sNomsBandes[][5] = {"FM", "GR", "RE", "EP", "PR"};

// Réservation d'une zone en flash pour la sauvegarde des paramètres
// ATTENTION Cette zone est effacée à chaque chargement d'un nouveau programme
FlashStorage(ParametresFlash, StructParams);

// Gestionnaire de la liaison série avec le GSM
Uart Serial2 (&sercom1, 11, 10, SERCOM_RX_PAD_0, UART_TX_PAD_2);

// Mode de fonctionnement
int iModeFonc = MODE_MESURE;

// Modif d'un paramètre en cours
bool bModifParam = false;

// Classe de mesure des échantillons
GestMesures mesures( &params);

// Gestionnaire du GSM
GSMManager gsm;

// Action courante du GSM
int iActionGSM;

// Temps de fin d'affichage du test SMS
int iTimeFinTestSMS;

// Mémo de la configuration de l'ADC
int iADC_CTRLBreg;
int iADC_AVGCTRLreg;
int iADC_SAMPCTRLreg;
int iADC_REFCTRLreg;

// Limite horaire pour retourner du mode paramètres au mode mesures en absence de frappe (2mn)
int iMaxSecParam = 0;

// Gestionnaire de l'écran
GestScreen display( &params);

// Mémo de la seconde courante
int iMemoSecCurr;

//------------------------------------------------
// Handler d'interruption de la liaison série
void SERCOM1_Handler()
{
  Serial2.IrqHandler();
}

//------------------------------------------------
// Test si un GSM est opérationnel
// Retourne true si c'est le cas
bool IsGSM()
{
  bool bOK = false;
  // On teste si un n° de téléphone est présent
  // Pas de test sur le code PIN, il peut être vide
  if (params.sNumTel1[0] != ' ' or params.sNumTel2[0] != ' ' or params.sNumTel3[0] != ' ')
    bOK = true;
  return bOK;
}

//------------------------------------------------
// Sauvegarde des paramètres dans la mémoire FLASH du processeur
void SauveParams()
{
  // Init du nom des bandes
  strcpy(sNomsBandes[BFM], params.sNomFM);
  // Sauvegarde des paramètres en flash
  params.sControle = 5555;
  ParametresFlash.write( params);
  #ifdef TEST_PARAMS
    Serial.println("SauveParams flash + SD");
  #endif
  // Sauvegarde cdes paramètres sur la carte SD
  if (SD.exists("RHIPARAM.TXT"))
  {
    // Effacement du fichier
    bool bRemove = SD.remove("RHIPARAM.TXT");
    #ifdef TEST_PARAMS
      if (bRemove)
        Serial.println("Effacement RHIPARAM.TXT OK");
      else
        Serial.println("Effacement RHIPARAM.TXT impossible !");
    #endif
  }
  // Création du fichier des  paramètres
  File paramFile = SD.open("RHIPARAM.TXT", FILE_WRITE);
  if (paramFile)
  {
    // Sauvegarde des paramètres dans le fichier
    paramFile.println(F("Parametres :"));
    // Nom de la station (12 caractères maximum)
    sprintf(sTempA, PSTR("Nom %s"), params.sNomStation);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Seuil relatif en dB au-dessus du bruit moyen
    sprintf(sTempA, PSTR("Seuil relatif %02ddB"), params.sSeuilRelatif);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Code PIN de l'option GSM
    sprintf(sTempA, PSTR("Code PIN %s"), params.sCodePin);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // N° de téléphone A pour le SMS
    sprintf(sTempA, PSTR("Num. A %s"), params.sNumTel1);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // N° de téléphone B pour le SMS
    sprintf(sTempA, PSTR("Num. B %s"), params.sNumTel2);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // N° de téléphone C pour le SMS
    sprintf(sTempA, PSTR("Num. C %s"), params.sNumTel3);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Coefficient du pont diviseur pour la mesure de la batterie externe pour compenser l'imprécision des résistances
    dtostrf( params.fCoefBExt, 1, 1, sTempB);
    sprintf(sTempA, PSTR("Coef Ext %s"), sTempB);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Coefficient du pont diviseur pour la mesure de la batterie interne pour compenser l'imprécision des résistances
    dtostrf( params.fCoefBInt, 1, 1, sTempB);
    sprintf(sTempA, PSTR("Coef Int %s"), sTempB);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Indique le type de test sur la broche 13
    sprintf(sTempA, PSTR("Signal test %02d"), params.iSignalTest);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // Gain numérique
    sprintf(sTempA, PSTR("Gain Num %02d"), params.iGain);
    #ifdef TEST_PARAMS
      Serial.println(sTempA);
    #endif
    paramFile.println(sTempA);
    // FMin, FMax et SMS des bandes
    for (int i=0; i<NBBANDES; i++)
    {
      dtostrf( params.fFMinB[i], 5, 1, sTempB);
      if (sTempB[0] == ' ')
        sTempB[0] = '0';
      if (sTempB[1] == ' ')
        sTempB[1] = '0';
      dtostrf( params.fFMaxB[i], 5, 1, sTempC);
      if (sTempC[0] == ' ')
        sTempC[0] = '0';
      if (sTempC[1] == ' ')
        sTempC[1] = '0';
      sprintf(sTempA, PSTR("Bande %d %s, FMin%s, FMax%s, SFC%02d, SMS%1d"), i, sNomsBandes[i], sTempB, sTempC, params.sSeuilFC[i], params.bSMSB[i]);
      #ifdef TEST_PARAMS
        Serial.println(sTempA);
      #endif
      paramFile.println(sTempA);
    }
    // Fermeture du fichier
    paramFile.println(" ");
    paramFile.flush();
    paramFile.close();
  }
}

//------------------------------------------------
// Lecture des paramètres sauvegardés dans la mémoire FLASH du processeur
void LectureParams()
{
  //Serial.println("LectureParams");
  // Initialisation aux valeurs par défaut
  display.InitParamsDefaut();
  // Lecture de la flash et vérification si elle est à 0
  StructParams par = ParametresFlash.read();
  if (par.sControle == 5555)
  {
    // Lecture correcte, on initialise les paramètres avec ceux de la sauvegarde flash
    params = par;
    // Vérification du signal de test
    if (params.iSignalTest < NORMAL or params.iSignalTest > TESTLoop)
      params.iSignalTest = NORMAL;
    // Vérification des longueurs des chaines
    if (strlen(params.sNomStation) > MAXLINE-1)
      params.sNomStation[MAXLINE-1] = 0;
    if (strlen(params.sCodePin) > MAXCODEPIN-1)
      params.sCodePin[MAXCODEPIN-1] = 0;
    if (strlen(params.sNumTel1) > MAXNUMTEL-1)
      params.sNumTel1[MAXNUMTEL-1] = 0;
    if (strlen(params.sNumTel2) > MAXNUMTEL-1)
      params.sNumTel2[MAXNUMTEL-1] = 0;
    if (strlen(params.sNumTel3) > MAXNUMTEL-1)
      params.sNumTel3[MAXNUMTEL-1] = 0;
    if (params.sNomFM[2] != 0)
      params.sNomFM[2] = 0;
    // On initialise le nom de la bande FM
    strcpy(sNomsBandes[BFM], params.sNomFM);
    #ifdef TEST_PARAMS
      Serial.print("LectureParams Flash NomStation=");
      Serial.print(params.sNomStation);
      Serial.print(" Seuil=");
      Serial.print(params.sSeuilRelatif);
      Serial.print(" Code PIN=");
      Serial.print(params.sCodePin);
      Serial.print(" Num A=");
      Serial.print(params.sNumTel1);
      Serial.print(" Num B=");
      Serial.print(params.sNumTel2);
      Serial.print(" Num C=");
      Serial.println(params.sNumTel3);
      Serial.print(" Coef EXT=");
      Serial.print(params.fCoefBExt);
      Serial.print(" Coef INT=");
      Serial.print(params.fCoefBInt);
      Serial.print(" Test=");
      Serial.println(params.iSignalTest);
      for (int i=0; i<NBBANDES; i++)
      {
        Serial.print(" Bande ");
        Serial.print(i);
        Serial.print(" ");
        Serial.print(sNomsBandes[i]);
        Serial.print(" FMin ");
        Serial.print(params.fFMinB[i]);
        Serial.print(", FMax ");
        Serial.print(params.fFMaxB[i]);
        Serial.print(", SMS ");
        Serial.println(params.bSMSB[i]);
      }
    #endif
  }
  else
  {
    // Tentative de lecture du fichier de la carte SD (sinon, on a les paramètres par défaut)
    //Serial.println("LectureParams file");
    // Ouverture du fichier des  paramètres
    File paramFile = SD.open("RhiParam.txt", FILE_READ);
    if ( paramFile)
    {
      // Lecture des paramètres dans le fichier
      // On passe la 1ère ligne
      ReadLine(paramFile, sTempA, 80);
      // Nom de la station (12 caractères maximum)
      // 012345678901234567890123456789
      // Nom RhinoLogger
      ReadLine(paramFile, sTempA, 80);
      strncpy(params.sNomStation, &(sTempA[4]), MAXLINE-1);
      params.sNomStation[MAXLINE-1] = 0;
      #ifdef TEST_PARAMS
        Serial.print("LectureParams SD NomStation=");
        Serial.print(params.sNomStation);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Seuil relatif en dB au-dessus du bruit moyen
      // 012345678901234567890123456789
      // Seuil relatif 30dB
      ReadLine(paramFile, sTempA, 80);
      if (sTempA[15] >= '0' and sTempA[15] <= '9')
        params.sSeuilRelatif = ((sTempA[14]-'0')*10) + (sTempA[15]-'0');
      else
        params.sSeuilRelatif = sTempA[14]-'0';
      #ifdef TEST_PARAMS
        Serial.print("sSeuilRelatif=");
        Serial.print(params.sSeuilRelatif);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Code PIN de l'option GSM
      // 012345678901234567890123456789
      // Code PIN 1234
      ReadLine(paramFile, sTempA, 80);
      strncpy(params.sCodePin, &(sTempA[9]), MAXCODEPIN-1);
      params.sCodePin[MAXCODEPIN-1] = 0;
      #ifdef TEST_PARAMS
        Serial.print("sCodePin=");
        Serial.print(params.sCodePin);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Téléphone A pour le SMS
      // 012345678901234567890123456789
      // Num. A 0633854439
      ReadLine(paramFile, sTempA, 80);
      strncpy(params.sNumTel1, &(sTempA[7]), MAXNUMTEL-1);
      params.sNumTel1[MAXNUMTEL-1] = 0;
      #ifdef TEST_PARAMS
        Serial.print("sNumTel1=");
        Serial.print(params.sNumTel1);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Téléphone B pour le SMS
      // 012345678901234567890123456789
      // Num. B 0633854439
      ReadLine(paramFile, sTempA, 80);
      strncpy(params.sNumTel2, &(sTempA[7]), MAXNUMTEL-1);
      params.sNumTel2[MAXNUMTEL-1] = 0;
      #ifdef TEST_PARAMS
        Serial.print("sNumTel2=");
        Serial.print(params.sNumTel1);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Téléphone C pour le SMS
      // 012345678901234567890123456789
      // Num. C 0633854439
      ReadLine(paramFile, sTempA, 80);
      strncpy(params.sNumTel3, &(sTempA[7]), MAXNUMTEL-1);
      params.sNumTel3[MAXNUMTEL-1] = 0;
      #ifdef TEST_PARAMS
        Serial.print("sNumTel3=");
        Serial.print(params.sNumTel3);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Coefficient du pont diviseur pour la mesure de la batterie externe pour compenser l'imprécision des résistances
      // 012345678901234567890123456789
      // Coef Ext 3.0
      ReadLine(paramFile, sTempA, 80);
      params.fCoefBExt = (float)(sTempA[9]-'0') + ((float)(sTempA[11]-'0') / 10.0);
      #ifdef TEST_PARAMS
        Serial.print("fCoefBExt=");
        Serial.print(params.fCoefBExt);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Coefficient du pont diviseur pour la mesure de la batterie interne pour compenser l'imprécision des résistances
      // 012345678901234567890123456789
      // Coef Int 2.0
      ReadLine(paramFile, sTempA, 80);
      params.fCoefBInt = (float)(sTempA[9]-'0') + ((float)(sTempA[11]-'0') / 10.0);
      #ifdef TEST_PARAMS
        Serial.print("fCoefBInt=");
        Serial.print(params.fCoefBInt);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif
      // Indique le type de test sur la broche 13
      // 012345678901234567890123456789
      // Signal test 00
      params.iSignalTest = NORMAL;  // On impose "Normal"
      ReadLine(paramFile, sTempA, 80);
      #ifdef TEST_PARAMS
        Serial.print("iSignalTest=");
        Serial.print(params.iSignalTest);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif

      // Gain numérique
      // 012345678901234567890123456789
      // Gain Num 00
      ReadLine(paramFile, sTempA, 80);
      if (sTempA[10] >= '0' and sTempA[10] <= '9')
        params.iGain = (GAINUM)(((sTempA[9]-'0')*10) + (sTempA[10]-'0'));
      else
        params.iGain = (GAINUM)(sTempA[9]-'0');
      #ifdef TEST_PARAMS
        Serial.print("iGain=");
        Serial.print(params.iGain);
        Serial.print(" [");
        Serial.print(sTempA);
        Serial.println("]");
      #endif

      // Fréquences min et max, seuil FC et SMS des 5 bandes
      // 0123456789012345678901234567890123456789012345
      // Bande 1 FM, FMin102.4, FMax104.6, SFC12, SMS0
      for (int i=0; i<NBBANDES; i++)
      {
        ReadLine(paramFile, sTempA, 80);
        // Vérification ligne
        if ((sTempA[6]-'0')==i and sTempA[15]=='n' and sTempA[26]=='x' and sTempA[36]=='C' and sTempA[43]=='S')
        {
          // Lecture des paramètres
          for (int j=16; j<20; j++)
          {
            if (sTempA[j] == ' ')
              sTempA[j] = '0';
          }
          for (int j=27; j<31; j++)
          {
            if (sTempA[j] == ' ')
              sTempA[j] = '0';
          }
          if (i == 0)
          {
            params.sNomFM[0] = sTempA[8];
            params.sNomFM[1] = sTempA[9];
            params.sNomFM[2] = 0;
            strcpy(sNomsBandes[BFM], params.sNomFM);
          }
          float fMin = ((float)(sTempA[16]-'0') * 100) + ((float)(sTempA[17]-'0') * 10) + ((float)(sTempA[18]-'0')) + ((float)(sTempA[20]-'0') / 10);
          if (fMin > 1 and fMin < 125.0)
            params.fFMinB[i] = fMin;
          float fMax = ((float)(sTempA[27]-'0') * 100) + ((float)(sTempA[28]-'0') * 10) + ((float)(sTempA[29]-'0')) + ((float)(sTempA[31]-'0') / 10);
          if (fMax > fMin and fMax < 125.0)
            params.fFMaxB[i] = fMax;
          params.sSeuilFC[i] = ((sTempA[37]-'0') * 10) + (sTempA[38]-'0');
          if (sTempA[44] == '1')
            params.bSMSB[i] = true;
          else
            params.bSMSB[i] = false;
        }
        else
        {
          // Valeurs par défaut
          switch (i)
          {
          case 0: // Bande FM
            params.fFMinB[i]   = 40.0;
            params.fFMaxB[i]   = 60.0;
            params.sSeuilFC[i] = 6;
            strcpy(params.sNomFM, "FM");
            strcpy(sNomsBandes[BFM], "FM");
            break;
          case 1: // Bande GR
            params.fFMinB[i]   = 76.0;
            params.fFMaxB[i]   = 86.0;
            params.sSeuilFC[i] = 10;
            break;
          case 2: // Bande RE
            params.fFMinB[i]   = 100.0;
            params.fFMaxB[i]   = 103.9;
            params.sSeuilFC[i] = 8;
            break;
          case 3: // Bande EP
            params.fFMinB[i]   = 104.0;
            params.fFMaxB[i]   = 106.5;
            params.sSeuilFC[i] = 8;
            break;
          case 4: // Bande PR
            params.fFMinB[i]   = 106.6;
            params.fFMaxB[i]   = 120.0;
            params.sSeuilFC[i] = 8;
            break;
          }
          params.bSMSB[i] = false;
        }
        #ifdef TEST_PARAMS
          Serial.print("Bande ");
          Serial.print(i);
          Serial.print(" ");
          Serial.print(sNomsBandes[i]);
          Serial.print(" FMin ");
          Serial.print(params.fFMinB[i]);
          Serial.print(", FMax ");
          Serial.print(params.fFMaxB[i]);
          Serial.print(", SFC ");
          Serial.print(params.sSeuilFC[i]);
          Serial.print(", SMS ");
          Serial.println(params.bSMSB[i]);
        #endif
      }
      
      paramFile.close();
    }
  }
//params.iSignalTest = TESTACQ; //$$
//params.iSignalTest = TESTCycle; //$$
//params.iSignalTest = TESTFFT; //$$
}

//----------------------------------------------------
// Test des caractéristiques de la carte SD
void TestCarteSD()
{
  Sd2Card card;
  SdVolume volume;
  uint32_t volumesize;
  float    fVolumeSize;

  //                           012345678901234567890
  strcat( mesures.mesBruitSD.sTypeSD, "SD ?!");
  strcat( mesures.mesBruitSD.sTypeSD, "Erreur carte SD !");
  strcpy( mesures.mesBruitSD.sFileSD, " ");
  //if (card.init(SPI_HALF_SPEED, SEL_SD))
  if (card.init(SPI_FULL_SPEED, SEL_SD))
  {
    switch(card.type())
    {
      case SD_CARD_TYPE_SD1:
        strcpy( mesures.mesBruitSD.sTypeSD, "SD1 ");
        break;
      case SD_CARD_TYPE_SD2:
        strcpy( mesures.mesBruitSD.sTypeSD, "SD2 ");
        break;
      case SD_CARD_TYPE_SDHC:
        strcpy( mesures.mesBruitSD.sTypeSD, "SDHC ");
        break;
      default:
        strcpy( mesures.mesBruitSD.sTypeSD, "SD?? ");
    }
  
    // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
    if (!volume.init(card))
      strcat( mesures.mesBruitSD.sTypeSD, "FAT?");
    else
    {
      sprintf( sTempA, "FAT%d ", volume.fatType());
      strcat(mesures. mesBruitSD.sTypeSD, sTempA);
    }
    // print the type and size of the first FAT-type volume
    volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
    volumesize *= volume.clusterCount();       // we'll have a lot of clusters
    volumesize *= 512;                         // SD card blocks are always 512 bytes
    volumesize /= 1024;
    volumesize /= 1024;
    fVolumeSize = (float)volumesize / 1024.0;
    dtostrf( fVolumeSize, 5, 2, sTempA);
    strcat( mesures.mesBruitSD.sTypeSD, sTempA);
    strcat( mesures.mesBruitSD.sTypeSD, " GO");
  }
}

//------------------------------------------------
// Init du logiciel
void setup()
{
  Serial.begin(115200);
  // Reset à 1 par défaut
  // Init de la liaison série et affichage de la version
  #ifdef TEST_PARAMS
    while(!Serial);
  #endif
  #ifdef DEBUGCONTEXT
    while(!Serial);
  #endif
  /*#ifdef TESTBRUIT
    while(!Serial);
  #endif*/
  #ifdef TRACENOISE
    while(!Serial);
  #endif
  //while(!Serial);
  Serial.println(" ");
  Serial.println(" ");
  Serial.print("RhinoLogger version ");
  Serial.print(VERSION);
  Serial.println(" copyright JDV 2016");
  Serial.println(" ");

  // Démarrage du modem du GSM
  Serial2.begin(115200);
  // Assign pins 0 & 1 SERCOM functionality
  pinPeripheral(10, PIO_SERCOM);
  pinPeripheral(11, PIO_SERCOM);

  // Init des entrées/sorties
  pinMode(TESTOUT   , OUTPUT);
  pinMode(KEYVAL    , INPUT_PULLUP);
  pinMode(WAKEUPGSM , OUTPUT);
  pinMode(LED_SD    , OUTPUT);
  // Led 13 éteinte
  digitalWrite(TESTOUT, LOW);
  // Led SD éteinte
  digitalWrite(LED_SD, LOW);
  // GSM éteint
  //Serial.print("GSM a 0 broche ");
  //Serial.println(WAKEUPGSM);
  digitalWrite(WAKEUPGSM, LOW);
  //digitalWrite(WAKEUPGSM, HIGH);
  pinMode(12 , OUTPUT);
  digitalWrite(12, LOW);

  // Initialisation de l'écran avec affichage du logo
  display.InitScreen();
  delay(TEMPSAFFLOGO);

  // Test des caractéristiques de la carte SD
  TestCarteSD();

  // Init de la carte SD
  if (!SD.begin(SEL_SD))
  {
    //                       123456789012345678901"
    strcpy( mesures.mesures.Erreur, "Erreur carte SD");
    Serial.println("Carte SD non detectee !");
    digitalWrite(LED_SD, HIGH);
    //mesures.MemoLog( mesures.mesures.Erreur);
  }
  else
    Serial.println("Carte SD presente");

  // On mémorise le lancement
  sprintf( sTempA, "Lancement RhinoLogger %s", VERSION);
  mesures.MemoLog( sTempA);

  // Test d'écriture de la carte SD
  mesures.TestFileSD();

  // Lecture des paramètres sauvegardés en flash
  LectureParams();
  Serial.println("LectureParams OK");
  // On passe en mode mesure
  iModeFonc = MODE_MESURE;

  // Init de l'horloge interne avec l'heure de l'horloge externe
  Wire.begin();
  rtcZero.begin();
  // On récupère l'heure courante de l'horloge sauvegardée
  byte second, minute, hour, day, month, year=0;
  readDS3231time( &second, &minute, &hour, &day, &month, &year);
  if (year == 0)
    // Seconde tentative de lecture
    readDS3231time( &second, &minute, &hour, &day, &month, &year);
  // Et on met à jour la date et l'heure du processeur
  rtcZero.setTime(hour, minute, second);
  rtcZero.setDate(day, month, year);
  if (rtcZero.getYear() == 0)
  {
    strcpy(mesures.mesures.Erreur, "Erreur Horloge");
    //mesures.MemoLog( mesures.mesures.Erreur);
  }
  // Init de l'alarme pour le reset à 12h30 chaque jour
  rtcZero.setAlarmTime(12, 30, 0);
  rtcZero.enableAlarm(rtcZero.MATCH_HHMMSS);
  rtcZero.attachInterrupt(alarmReset);
  // Init de la seconde courante pour l'arret éventuel de l'affichage
  iMemoSecCurr = rtcZero.getHours()*3600 + rtcZero.getMinutes()*60 + rtcZero.getSeconds();
  Serial.println("Init horloge OK");

  // Mémo du réglage de l'ADC pour les lectures normales
  ADC->CTRLA.bit.ENABLE = 0;                     // Stop ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  iADC_CTRLBreg    = ADC->CTRLB.reg;
  iADC_AVGCTRLreg  = ADC->AVGCTRL.reg;
  iADC_SAMPCTRLreg = ADC->SAMPCTRL.reg;
  iADC_REFCTRLreg  = ADC->REFCTRL.reg;
  ADC->CTRLA.bit.ENABLE = 1;                     // Lance l'ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  Serial.println("Init ADC OK");

  // Init d'un SMS sans info
  mesures.InitSMS();

  // On lance les mesures
  GoToMesures();
  mesures.bRunMesures = true;
}

//------------------------------------------------
// Passe le système en mode veille
void SystemSleep()
{
  display.AffVeille();
  // On clignotte 3 fois pour signaler la mise en veille
  for (int i=0; i<3; i++)
  {
    digitalWrite(TESTOUT, HIGH);
    delay(500);
    digitalWrite(TESTOUT, LOW);
    delay(500);
  }
  digitalWrite(TESTOUT, LOW);
  // Arrêt alarme
  rtcZero.disableAlarm();
  // Efface l'écran
  display.StopDisplay();
  // Mode veille profonde
  SCB->SCR |= 1<<2;
  // Attente interruption (Wait For Interrupt)
  __WFI();
  digitalWrite(TESTOUT, HIGH);
}

//------------------------------------------------
// Fonction exécutée sur l'heure d'alarme de l'horloge interne processeur
void alarmReset()
{
  // Mémo de l'heure du reset automatique
  sprintf( mesures.strDate , "%02d/%02d/%02d", rtcZero.getDay()  , rtcZero.getMonth()  , rtcZero.getYear());
  sprintf( mesures.strHeure, "%02d:%02d:%02d", rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  mesures.MemoLog( "Reset automatique");
  #ifdef RESET12H30
    //Serial.println("Reset...");
    // Reset soft du processeur
    NVIC_SystemReset();
  #else
    Serial.println("Pas de demande de Reset");
  #endif
}

//------------------------------------------------
// Lecture du clavier
// Retourne NOBUT ou la touche activée
int GetKey()
{
  // Tableau des valeurs min et max des boutons Haut, Bas, Droit, Gauche
  const uint16_t BMIN[NBANAKEY] PROGMEM = { 0,  87, 247, 617};
  const uint16_t BMAX[NBANAKEY] PROGMEM = {82, 242, 612, 999};

  int KeyA = NOBUT, KeyB = NOBUT;

  // Lecture de la touche digitale de validation
  if (digitalRead( KEYVAL) == LOW)
  {
    KeyA = BSELECT;
    // Attente rebond 100ms
    delay(100);
    if (digitalRead( KEYVAL) == LOW)
      KeyB = BSELECT;
  }
  // Lecture des touches analogique uniquement si on est en modification des paramètres
  // pour ne pas perturber l'acquisition qui utilise l'ADC dans un mode complètement différent
  else if (iModeFonc >= MODE_PARAMS1)
  {
    // 1ère lecture de la tension de la broche clavier
    analogReadResolution(10);
    int vKey = analogRead( KEYBOARD);
    #ifdef TESTCLAVIER
      Serial.print("vKeyA=");
      Serial.print(vKey);
    #endif
    for (int i = 0; i < NBANAKEY; i++)
    {
      if (vKey >= BMIN[i] && vKey <= BMAX[i])
      {
        KeyA = i + 1;
        break;
      }
    }
    if (KeyA != NOBUT)
    {
      // Attente rebond 100ms
      delay(100);
      // 2ème lecture de la tension de la broche clavier
      // Pour éliminer les rebonds
      vKey = analogRead( KEYBOARD);
      #ifdef TESTCLAVIER
        Serial.print(" vKeyB=");
        Serial.print(vKey);
        Serial.print("   ");
      #endif
      for (int i = 0; i < NBANAKEY; i++)
      {
        if (vKey >= BMIN[i] && vKey <= BMAX[i])
        {
          KeyB = i + 1;
          break;
        }
      }
    }
  }
  if (KeyA == KeyB)
  {
    #ifdef TESTCLAVIER
      switch (KeyA)
      {
      case NOBUT:
        Serial.println("Pas de bouton");
        break;
      case BUP:
        Serial.println("Bouton HAUT");
        break;
      case BDOWN:
        Serial.println("Bouton BAS");
        break;
      case BRIGHT:
        Serial.println("Bouton DROITE");
        break;
      case BLEFT:
        Serial.println("Bouton GAUCHE");
        break;
      case BSELECT:
        Serial.println("Bouton SELECT");
        break;
      default:
        Serial.print("Bouton inconnu ");
        Serial.println(KeyA);
      }
    #endif
    return KeyA;
  }
  else
  {
    #ifdef TESTCLAVIER
      Serial.println("Pas de Bouton");
    #endif
    return NOBUT;
  }
}

//------------------------------------------------
// Passe en mode mesure
void GoToMesures()
{
  // Effacement de l'écran
  display.RAZDisplay();
  // On passe en mode mesure
  iModeFonc = MODE_MESURE;
  // On lance les mesures
  mesures.InitMesures( display.isRAZDatas());
  // Init de la minute courante
  iMemoSecCurr = rtcZero.getHours()*3600 + rtcZero.getMinutes()*60 + rtcZero.getSeconds();
  mesures.mesBruitSD.ModeAffMes = MODEMESBRUIT;
  // On relance l'affichage
  display.StartDisplay();
}

//------------------------------------------------
// Passe en mode paramètres
void GoToParams()
{
  iModeFonc = MODE_PARAMS1;
  bModifParam = false;
  display.SetLigneActive(0);
  // On stoppe les mesures
  mesures.bRunMesures = false;  
  mesures.enable(false);
  mesures.SauveContexte();
  // On restitue la lecture normale des broches analogiques
  // Restitution du réglage de l'ADC pour les lecture normales
  ADC->CTRLA.bit.ENABLE = 0;                     // Stop ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  ADC->CTRLB.reg    = iADC_CTRLBreg;
  ADC->AVGCTRL.reg  = iADC_AVGCTRLreg;
  ADC->SAMPCTRL.reg = iADC_SAMPCTRLreg;
  ADC->REFCTRL.reg  = iADC_REFCTRLreg;
  ADC->CTRLA.bit.ENABLE = 1;                     // Lance l'ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  analogReadResolution(10);
  analogReference(AR_DEFAULT);
  analogRead( KEYBOARD);
  // Calcul du temps max d'affichage des paramètres pour retourner en mesures en absence de frappes
  // Plusieurs minutes pour laisser le temps de copier la carte SD
  iMaxSecParam = (rtcZero.getHours() * 3600) + (rtcZero.getMinutes() * 60) + rtcZero.getSeconds() + TEMPS_RETOUR_MES_AUTO;
}

//------------------------------------------------
// Traitements dans la boucle principale du logiciel en mode mesures
void loopOnMesures()
{
  // Exécute le loop des mesures
  if (mesures.loop())
  {
    // La seconde courante à changée, Affichage
    // On suspend les mesures
    mesures.bRunMesures = false;
    switch (params.iSignalTest)
    {
      case TESTAffMax:
        // Affichage en mode debug des 7 niveaux max
        display.AffDebugNivMax( mesures.mesDebug, mesures.mesures.memoM.iBruitMoyen);
        mesures.RAZDebugNivMax();
        break;
      case TESTAffFM :  // Permet d'afficher les niveaux des 7 1er canaux de la bande FM
      case TESTAffGR1:  // Permet d'afficher les niveaux des 7 1er canaux de la bande GR
      case TESTAffGR2:  // Permet d'afficher les niveaux des 7 derniers canaux de la bande GR
      case TESTAffRE :  // Permet d'afficher les niveaux des 7 1er canaux de la bande RE
        // Affichage mode Debug
        display.AffDebugFreq( mesures.mesFreq, mesures.mesures.memoM.iBruitMoyen);
        mesures.RAZDebugFreq(false);
        break;
      case NORMAL:  // Pas de signal de test, l'écran s'éteint automatiquement après le 1er changement de minute
      default:
        if (display.IsActif())
        {
          int iCurSecond = mesures.currentHh * 3600 + mesures.currentMn * 60 + mesures.currentSec;
          if (mesures.mesBruitSD.ModeAffMes < MODEMESAFFMES)
          {
            display.AffBruitAndSD( &(mesures.mesBruitSD), &(mesures.mesures));
            if (mesures.mesBruitSD.ModeAffMes == MODEMESAFFBR && iCurSecond > (iMemoSecCurr+TEMPSAFFBRUIT))
            {
              iMemoSecCurr = iCurSecond;
              mesures.mesBruitSD.ModeAffMes = MODEMESAFFMES;
              Serial.println("Fin mode affichage bruit et SD, passage en mode mesure");
            }
          }
          else
          {
            // Affichage des infos
            display.AffInfos( &(mesures.mesures));
            if (params.iSignalTest == NORMAL and iCurSecond > (iMemoSecCurr+TEMPSAFFMES))
              display.StopDisplay();
          }
        }
        break;
    }
    // On reprend les mesures
    mesures.bRunMesures = true;
  }
  // Lecture du clavier (uniquement la touche sélect)
  int iKey  = GetKey();
  if (iKey == BSELECT)
  {
    if (display.IsActif() == false)
    {
      // Init de la minute courante
      iMemoSecCurr = rtcZero.getHours()*3600 + rtcZero.getMinutes()*60 + rtcZero.getSeconds();
      // On relance l'affichage
      display.StartDisplay();
    }
    else
      // On passe en mode affichage des paramètres
      GoToParams();
  }
  // Test si l'heure de l'attente ou du SMS (12h20, on a 10mn max avant le reset pour envoyer les SMS)
  int iMN = rtcZero.getMinutes();
  if (rtcZero.getHours() == 12 and iMN >= 20 and iMN <= 21)
  {
    // On stoppe les mesures
    GoToParams();
    // Test si un GSM est opérationnel
    if (IsGSM())
    {
      // Passage en mode SMS
      iModeFonc = MODE_SMS;
      iActionGSM = STARTGSM;
      mesures.MemoLog( "Envoie SMS journalier");
    }
    else
    {
      // Passage en mode attente du reset
      iModeFonc = MODE_ATTENTE;
      mesures.MemoLog( "Mode attente Reset");
    }
  }
  if (params.iSignalTest == TESTLoop)
      digitalWrite(TESTOUT, LOW);  

  // Attente pour un prochain loop
  delay(200);
}

//------------------------------------------------
// Emission d'un SMS
void SendSMS(
  bool bTestSMS,  // Indique si c'est le SMS de test
  char *psTxtSMS  // SMS à envoyer
  )
{
  if (gsm.GetSequence() == GSMManager::STARTGSMOK || gsm.GetSequence() == GSMManager::SMSOK)
  {
    // Récupération du N°
    char *psNumTel;
    switch (iActionGSM)
    {
    case SMSA:
    case SMSFMA:
    case SMSGRA:
    case SMSREA:
    case SMSEPA:
    case SMSPRA:
      psNumTel = params.sNumTel1;
      break;
    case SMSB:
    case SMSFMB:
    case SMSGRB:
    case SMSREB:
    case SMSEPB:
    case SMSPRB:
      psNumTel = params.sNumTel2;
      break;
    case SMSC:
    case SMSFMC:
    case SMSGRC:
    case SMSREC:
    case SMSEPC:
    case SMSPRC:
      psNumTel = params.sNumTel3;
      break;
    }
    // Emission du SMS $$
    if (psNumTel[0] != ' ' and psTxtSMS!= NULL)
      gsm.SendSMS(psNumTel, psTxtSMS);
    else
    {
      // Pas de n° ou texte, passage à la séquence suivante
      iActionGSM++;
      if (bTestSMS and iActionGSM > SMSC)
        #ifdef TSTMAIL
          // Passage en mode GPRS
          iActionGSM = STARTGPRS;
        #else
          // Arret GSM
          iActionGSM = STOPGSM;
        #endif
      else if (!bTestSMS and iActionGSM > SMSPRC)
        #ifdef TSTMAIL
          // Passage en mode GPRS
          iActionGSM = STARTGPRS;
        #else
          // Arret GSM
          iActionGSM = STOPGSM;
        #endif
    }
  }
  else
  {
    // Exécution des séquences d'émission du SMS
    gsm.GestionGSM();
    if (gsm.GetSequence() == GSMManager::SMSOK)
      // Emission avec succès, passage à la séquence suivante
      iActionGSM++;
    else if (gsm.GetSequence() == GSMManager::ERRORGSM)
    {
      // GSM en erreur
      mesures.MemoLog( "Test SMS, probleme envoie SMS");
      iActionGSM = STOPGSM;
    }
  }
}

//------------------------------------------------
// Boucle principale dans le cas de gestion du GSM
void loopGSM(
  bool bTestSMS // Indique si on est en mode test SMS
  )
{
  // Affichage du mode SMS
  display.AffSMS( bTestSMS, &gsm);
  
  if (iActionGSM < SMSA)
  {
    iTimeFinTestSMS = 0;
    // Démarrage du GSM
    if (iActionGSM == STARTGSM)
    {
      // Démarrage du GSM
      gsm.StartGSM( params.sCodePin);
      iActionGSM = INITGSM;
    }
    else
    {
      // Exécution des séquences de démarrage
      gsm.GestionGSM();
      if (gsm.GetSequence() == GSMManager::STARTGSMOK)
      {
        // GSM démarré avec succès, passage à la séquence suivante
        iActionGSM = SMSA;
      }
      else if (gsm.GetSequence() == GSMManager::ERRORGSM)
      {
        if (gsm.IsError())
        {
          // GSM en erreur
          sprintf(sTempC, "Erreur GSM séquence %d: %s", gsm.GetSequenceError(), gsm.GetErrorGSM());
          mesures.MemoLog( sTempC);
        }
        iActionGSM = STOPGSM;
      }
    }
  }
  else if (bTestSMS and iActionGSM >= SMSA and iActionGSM <= SMSC)
  {
    // Emission SMS
    if (iActionGSM == SMSA)
      SendSMS( true, "RhinoLogger Test SMS Num A\nNe pas repondre");
    else if (iActionGSM == SMSB)
      SendSMS( true, "RhinoLogger Test SMS Num B\nNe pas repondre");
    else if (iActionGSM == SMSC)
      SendSMS( true, "RhinoLogger Test SMS Num C\nNe pas repondre");
  }
  else if (!bTestSMS and iActionGSM >= SMSA and iActionGSM <= SMSPRC)
  {
    // Emission des SMS
    char *pTxtSMS = NULL;
    if (iActionGSM >= SMSA && iActionGSM <= SMSC)
      pTxtSMS = mesures.strSMS;
    else if (iActionGSM >= SMSFMA && iActionGSM <= SMSFMC)
    {
      if (params.bSMSB[BFM])
        pTxtSMS = mesures.strBSMS[BFM];
    }
    else if (iActionGSM >= SMSGRA && iActionGSM <= SMSGRC)
    {
      if (params.bSMSB[BGR])
        pTxtSMS = mesures.strBSMS[BGR];
    }
    else if (iActionGSM >= SMSREA && iActionGSM <= SMSREC)
    {
      if (params.bSMSB[BRE])
        pTxtSMS = mesures.strBSMS[BRE];
    }
    else if (iActionGSM >= SMSEPA && iActionGSM <= SMSEPC)
    {
      if (params.bSMSB[BEP])
        pTxtSMS = mesures.strBSMS[BEP];
    }
    else if (iActionGSM >= SMSPRA && iActionGSM <= SMSPRC)
    {
      if (params.bSMSB[BPR])
        pTxtSMS = mesures.strBSMS[BPR];
    }
    // Emission SMS
    SendSMS( false, pTxtSMS);
  }
  else if (iActionGSM == STARTGPRS)
  {
    // Passage en mode GPRS
    if (gsm.GetSequence() < GSMManager::SETGPRS)
      // Démarrage du mode GPRS
      gsm.StartGPRS( NULL);
    else
    {
      // Exécution des séquences de démarrage
      gsm.GestionGSM();
      if (gsm.GetSequence() == GSMManager::GPRSOK)
      {
        //  GPRS OK, passage à la séquence suivante
        iActionGSM = SENDMAIL;
      }
      else if (gsm.GetSequence() == GSMManager::ERRORGSM or gsm.GetSequence() == GSMManager::STOPGPRSOK)
      {
        if (gsm.IsError())
        {
          // GSM en erreur
          sprintf(sTempC, "Erreur GSM séquence %d", gsm.GetSequenceError());
          mesures.MemoLog( sTempC);
        }
        if (gsm.GetSequenceError() >= GSMManager::SETGPRS)
          iActionGSM = STOPGPRS;
        else
          iActionGSM = STOPGSM;
      }
    }
  }
  else if (iActionGSM == SENDMAIL)
  {
    // Emission du mail
    if (gsm.GetSequence() < GSMManager::STARTMAIL)
      // Emission mail
      gsm.SendMail( NULL, NULL, NULL, NULL, NULL);
    else
    {
      // Exécution des séquences d'émission mail
      gsm.GestionGSM();
      if (gsm.GetSequence() == GSMManager::ENDMAIL)
      {
        // Mail envoyé, passage à la séquence suivante
        iActionGSM = STOPGPRS;
        // Test du succès de l'émission
        int iCodeMail = gsm.GetiCodeMail();
        switch (iCodeMail)
        {
        case  1: strcpy( sTempC, "Emission mail OK"); break;
        case 61: strcpy( sTempC, "SendMail Network error"); break;
        case 62: strcpy( sTempC, "SendMail DNS resolve error"); break;
        case 63: strcpy( sTempC, "SendMail SMTP TCP error"); break;
        case 64: strcpy( sTempC, "SendMail Timeout of SMTP server response"); break;
        case 65: strcpy( sTempC, "SendMail SMTP server response error"); break;
        case 66: strcpy( sTempC, "SendMail Not authentification"); break;
        case 67: strcpy( sTempC, "SendMail Authentification failed (username or password)"); break;
        case 68: strcpy( sTempC, "SendMail Bad recipient"); break;
        default: sprintf(sTempC, "SendMail Erreur inconnue : %d", iCodeMail); break;
        }
        if (iCodeMail != 1)
          mesures.MemoLog( sTempC);
      }
      else if (gsm.GetSequence() == GSMManager::ERRORGSM or gsm.GetSequence() == GSMManager::STOPGPRSOK)
      {
        if (gsm.IsError())
        {
          // GSM en erreur
          sprintf(sTempC, "Erreur GSM séquence %d", gsm.GetSequenceError());
          mesures.MemoLog( sTempC);
        }
        iActionGSM = STOPGPRS;
      }
    }
  }
  else if (iActionGSM == STOPGPRS)
  {
    // Arrêt du mode GPRS
    if (gsm.GetSequence() < GSMManager::STOPGPRS or (gsm.GetSequence() == GSMManager::ERRORGSM and gsm.GetSequence() != GSMManager::STOPGPRS))
      // Arret du GPRS
      gsm.StopGPRS();
    else
    {
      // Exécution des séquences d'arret du GSM
      gsm.GestionGSM();
      if (gsm.GetSequence() == GSMManager::STOPGPRSOK)
        // Arret avec succès, passage à la séquence suivante
        iActionGSM = STOPGSM;
      else if (gsm.GetSequence() == GSMManager::ERRORGSM)
      {
        if (gsm.IsError())
        {
          // GSM en erreur
          sprintf(sTempC, "Erreur GSM séquence %d", gsm.GetSequenceError());
          mesures.MemoLog( sTempC);
        }
        iActionGSM = STOPGSM;
      }
    }
  }
  else if (iActionGSM == STOPGSM)
  {
    // Arrêt du GSM
    if (gsm.GetSequence() < GSMManager::RAZSMS or gsm.GetSequence() == GSMManager::ERRORGSM)
      // Arret du GSM
      gsm.StopGSM();
    else
    {
      // Exécution des séquences d'arret du GSM
      gsm.GestionGSM();
      if (gsm.GetSequence() == GSMManager::WAKEUPGSMOK)
        // Arret avec succès, passage à la séquence suivante
        iActionGSM = FINGSM;
      else if (gsm.GetSequence() == GSMManager::ERRORGSM)
      {
        if (gsm.IsError())
        {
          // GSM en erreur
          sprintf(sTempC, "Erreur GSM séquence %d", gsm.GetSequenceError());
          mesures.MemoLog( sTempC);
        }
        iActionGSM = FINGSM;
      }
    }
  }
  else if (iActionGSM == FINGSM)
  {
    if (bTestSMS)
    {
      if (iTimeFinTestSMS == 0)
        iTimeFinTestSMS = millis() + 15000;
      // Attente 15s fin affichage
      if (millis() > iTimeFinTestSMS)
      {
        // Arret du test
        display.StopTestSMS();
        // Passage en mode paramètres page 2
        iModeFonc = MODE_PARAMS2;
      }
    }
    else
    {
      // Passage en mode Attente
      iModeFonc = MODE_ATTENTE;
      mesures.MemoLog( "Mode attente Reset");
    }
  }
}

//------------------------------------------------
// Traitements dans la boucle principale du logiciel en mode paramètres
void loopOnParams()
{
  //Serial.println("loopOnParams");
  // Selon le mode de fonctionnement
  switch (iModeFonc)
  {
  case MODE_PARAMS1:
    // Affichage de la page 1 des paramètres
    display.AffParamsP1();
    break;
  case MODE_PARAMS2:
    // Affichage de la page 2 des paramètres
    display.AffParamsP2( &(mesures.mesures));
    break;
  case MODE_PARAMS3:
    // Affichage de la page 3 des paramètres
    display.AffParamsP3();
    break;
  case MODE_TESTSMS:
    // Traitement GSM
    loopGSM( true);
    break;
  }
  // Lecture du clavier, y compris les touches analogiques
  int iKey  = GetKey();
  if (iKey == BSELECT and display.GetLigneActive() > 0 and millis() > (iLatsSelect + 500))
  {
    // On mémorise la dernière frappe de SELECT
    iLatsSelect = millis();
    // Calcul du temps max d'affichage des paramètres pour retourner en mesures en absence de frappes
    // Plusieurs minutes pou laisser le temps de copier la carte SD
    iMaxSecParam = (rtcZero.getHours() * 3600) + (rtcZero.getMinutes() * 60) + rtcZero.getSeconds() + TEMPS_RETOUR_MES_AUTO;
    if (iModeFonc == MODE_TESTSMS)
    {
      // On sort du test SMS et on retourne en modif param page 2
      iModeFonc = MODE_PARAMS2;
      // Arrêt du GSM
      gsm.StopGSM();
    }
    // On passe en mode modif ou on valide une modif
    else if (!bModifParam)
    {
      // On passe en modif d'un paramètre
      bModifParam = display.SetModifOn();
      if (!bModifParam)
      {
        // Un seul paramètre retourne false : le retour en mesure
        // Effacement de l'écran
        display.RAZDisplay();
        // Sauvegarde des paramètres en flash
        SauveParams();
        // Retour au mode mesures
        GoToMesures();
      }
    }
    else if (bModifParam)
    {
      // On valide un paramètre et on stoppe la modif
      display.ValidModif();
      bModifParam = false;
      // Test si on passe en test SMS
      if (display.GetTestSMS())
      {
        // On passe en mode SMS
        iActionGSM = STARTGSM;
        iModeFonc  = MODE_TESTSMS;
      }
      // Test si on passe en mode veille
      if (display.GetModeVeille())
      {
        // On passe en mode veille, seul un Reset manuel peut sortir du mode veille
        iModeFonc = MODE_VEILLE;
      }
      // Test si validation d'un coefficient de mesure
      if (display.GetModifCoeff())
      {
        // On refait une mesure de tension pour afficher la nouvelle valeur
        mesures.LectureTHV( false);
      }
    }
  }
  else
  {
    // Gestion des touches UP, DOWN, LEFT et RIGHT uniquement en mode paramètres
    bool bChangePagePlus  = false;
    bool bChangePageMoins = false;
    switch( iKey)
    {
    case BDOWN:
      if (!bModifParam)
        bChangePagePlus = display.ScrollDown();
      else
        display.ModifDown();
      break;
    case BUP:
      if (!bModifParam)
        bChangePageMoins = display.ScrollUp();
      else
        display.ModifUp();
      break;
    case BRIGHT:
      if (bModifParam)
        // Champ suivant
        display.ScrollRight();
      else
      {
        // On passe en modif d'un paramètre
        bModifParam = display.SetModifOn();
        if (!bModifParam)
        {
          // Un seul paramètre retourne false : le retour en mesure
          // Sauvegarde des paramètres en flash
          SauveParams();
          // Retour au mode mesures
          GoToMesures();
        }
      }
      break;
    case BLEFT:
      display.ScrollLeft();
      break;
    }
    if (bChangePagePlus)
    {
      if (iModeFonc == MODE_PARAMS1)
        iModeFonc = MODE_PARAMS2;
      else if (iModeFonc == MODE_PARAMS2)
        iModeFonc = MODE_PARAMS3;
      else
        iModeFonc = MODE_PARAMS1;
    }
    else if (bChangePageMoins)
    {
      if (iModeFonc == MODE_PARAMS1)
        iModeFonc = MODE_PARAMS3;
      else if (iModeFonc == MODE_PARAMS2)
        iModeFonc = MODE_PARAMS1;
      else
        iModeFonc = MODE_PARAMS2;
    }
  }
  // Test si on est depuis trop longtemps en mode paramètres sans frappe du clavier
  if (iModeFonc <= MODE_PARAMS3 and (rtcZero.getHours() * 3600) + (rtcZero.getMinutes() * 60) + rtcZero.getSeconds() > iMaxSecParam)
  {
    // On passe automatiquement en mode mesure avec écran intermitent
    params.iSignalTest = NORMAL;
    // Sauvegarde des paramètres en flash
    SauveParams();
    // Retour au mode mesures
    GoToMesures();
  }
  if (params.iSignalTest == TESTLoop)
      digitalWrite(TESTOUT, LOW);  

  // Attente pour un prochain loop
  delay(50);
}

//------------------------------------------------
// Boucle principale du logiciel
void loop()
{
  /*Serial.print("loop iModeFonc = ");
  Serial.println(iModeFonc);*/
  if (params.iSignalTest == TESTLoop)
      digitalWrite(TESTOUT, HIGH);  

  // Test du mode de fonctionnement
  switch (iModeFonc)
  {
  case MODE_SMS:
    // Traitement GSM
    loopGSM( false);
    break;
  case MODE_ATTENTE:
    // Attente du reset, on ne fait rien
    display.AffAttenteReset();
    delay(1000);
    break;
  case MODE_PARAMS1:
  case MODE_PARAMS2:
  case MODE_PARAMS3:
  case MODE_TESTSMS:
    // Traitements en mode paramètres ou test SMS
    loopOnParams();
    break;
  default:
  case MODE_MESURE:
  case MODE_DEBUG:
    // Traitements en mode mesures
    loopOnMesures();
    break;
  case MODE_VEILLE:
    // On passe le processeur en veille
    SystemSleep();
  }
}

