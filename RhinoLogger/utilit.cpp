/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

//------------------------------------------------
// Regroupement de différents utilitaires


#include <Wire.h>
#include "utilit.h"
#include "const.h"

// Chaines de caractères générique
char sTempA[255], sTempB[255], sTempC[255];

//------------------------------------------------
// Retourne la taille de la mémoire libre
extern "C" char *sbrk(int i);
int GetFreeRam ()
{
  char stack_dummy = 0;
  return &stack_dummy - sbrk(0);
}


//----------------------------------------------------
// Classe de gestion d'un GSM type SIM800L
// Permet l'envoie d'un SMS
 
// Gestionnaire de la liaison série avec le GSM
extern Uart Serial2;

//----------------------------------------------------
// Constructeur du gestionnaire
GSMManager::GSMManager()
{
  // Init des variables
  bStart    = false;
  bPinOK    = false;
  bNetwork  = false;
  iSignalQ  = 0;
  bSMSOK    = false;
  memset(OpName, 0, 15);
  iSeqGSM   = STANDBYGSM;
}

//----------------------------------------------------
// Démarre le GSM (Mise à 1 de la broche Enable du régulateur)
// La séquence exécutée est la suivante :
// - Réveil du GSM (Enable à 1)
// - Répétition de la commande ATI pour vérifier la présence du GSM
// - Saisie éventuel du code PIN
// - Vérification opérateur et niveau
// La séquence est terminée lorsque IsWakeupOK retourne true ou lorsque IsError retourne true
void GSMManager::StartGSM(
  char *pCodePin  // Code PIN (4 chiffres) de la carte SIM
  )
{
  // Mémo du code PIN
  pCPin = pCodePin;
  // Init des variables
  bStart    = false;
  bPinOK    = false;
  bNetwork  = false;
  bWakeup   = false;
  bError    = false;
  iSignalQ  = 0;
  memset(OpName, 0, 15);
  // Démarre le GSM (Enable à 1)
  digitalWrite(WAKEUPGSM, HIGH);
  #ifdef TSTGSM
    Serial.println("Broche WAKEUPGSM a 1");
  #endif
  // Initialisation de la séquence en cours
  iSeqGSM = STARTGSM;
  iTimeFinSeq = millis() + 10000;
  iBoucles  = 0;
  iBouclesMax = 0;
}

//----------------------------------------------------
// Envoie du SMS vers un n°
// La séquence exécutée est la suivante :
// - Envoie du SMS
// La séquence est terminée lorsque IsSMSOK retourne true ou lorsque IsError retourne true
void GSMManager::SendSMS(
  char *pNumTel,  // Numéro de téléphone destinataire
  char *pTxtSMS   // Texte du SMS
  )
{
  // Mémo des paramètres
  pNumero = pNumTel;
  pTexteSMS = pTxtSMS;
  // Passage à la séquence d'émission SMS
  iSeqGSM = STARTSMS;
  bSMSOK = false;
  iTimeFinSeq = 0;
  iBoucles = 0;
  iBouclesMax = 5;
  GestionGSM();
}

//----------------------------------------------------
// Arret du GSM, on coupe l'alimentation en mettant Enable du régulateur à 0)
// La séquence exécutée est la suivante :
// - Effacement des SMS reçus ou envoyés
// - Arret de l'alimentation
// La séquence est terminée lorsque IsStarted retourne false ou lorsque IsError retourne true
void GSMManager::StopGSM()
{
  // Init des variables
  // On garde les variables OK de façon à maintenir l'affichage OK
  /*bStart    = false;
  bPinOK    = false;
  bNetwork  = false;
  bSMSOK    = false;
  iSignalQ  = 0;
  memset(OpName, 0, 15);*/
  // Passage à la séquence d'arrêt du GSM
  iSeqGSM = RAZSMS;
  iTimeFinSeq = 0;
  iBoucles = 0;
  iBouclesMax = 5;
  GestionGSM();
}

//----------------------------------------------------
// Passage en mode GPRS
// La séquence exécutée est la suivante :
// - Ouverture du mode GPRS et configuration du point d'accès (APN)
// - Test de l'adresse IP
// La séquence est terminée lorsque IsGPRS retourne true
void GSMManager::StartGPRS(
  char *pAPN  // Pointeur sur le nom du point d'accès
  )
{
  // Init des variables
  bGPRS = false;
  psAPN = pAPN;
  // Passage à la séquence de démarrage du mode GPRS
  iSeqGSM = SETGPRS;
  iTimeFinSeq = 0;
  iBoucles = 0;
  iBouclesMax = 5;
  GestionGSM();
}

//----------------------------------------------------
// Passage en mode Emission d'un mail
// La séquence exécutée est la suivante :
// - Ouverture du mode GPRS et configuration du point d'accès (APN)
// - Test de l'adresse IP
// La séquence est terminée lorsque IsSendMail retourne true
void GSMManager::SendMail(
  char *pAdrCompte,   // Pointeur sur l'adresse du compte
  char *pMotDePasse,  // Pointeur sur le mot de passe du compte
  char *pSujet,       // Pointeur sur le sujet
  char *pTexte,       // Pointeur sur le texte du mail
  char *pFileName     // Pointeur sur le nom du fichier joint
  )
{
  // Mémo des paramètres
  psAdrCompte  = pAdrCompte;
  psMotDePasse = pMotDePasse;
  psSujet      = pSujet;
  psTexte      = pTexte;
  psFileName   = pFileName;
  // Passage à la séquence d'émission d'un mail
  iSeqGSM = STARTMAIL;
  bSMSOK = false;
  iTimeFinSeq = 0;
  iBoucles = 0;
  iBouclesMax = 5;
  iCodeMail = 0;
  GestionGSM();
}

//----------------------------------------------------
// Arret du mode GPRS
// La séquence est terminée lorsque IsGPRS retourne false
void GSMManager::StopGPRS()
{
  // Passage à la séquence d'arrêt du mode GPRS
  iSeqGSM = STOPGPRS;
  iTimeFinSeq = 0;
  iBoucles = 0;
  iBouclesMax = 5;
  GestionGSM();
}

//----------------------------------------------------
// Exécution des commandes GSM
// Retourne true si la commande est correctement exécutée
bool GSMManager::CommandeGSM(
  char *psCmd,        // Commande à exécuter (NULL si aucune)
  char *psReponseA,   // Réponse A à tester (NULL si aucune)
  char *psReponseB,   // Réponse B à tester (NULL si aucune)
  int iSeqSuivanteA,  // Séquence suivante si la réponse A est OK
  int iSeqSuivanteB,  // Séquence suivante si la réponse B est OK
  int iTempoSuivante, // Tempo avant la prochaine commande en ms
  int iTempoErreur,   // Tempo en cas d'erreur avant la prochaine tentative
  int iNbBoucles      // Nombre max de boucle avant erreur
  )
{
  bool bOK = false;
  bool bReponseA = false;
  bool bReponseB = false;
  int iNbChar;
  iBoucles++;
  iBouclesMax = iNbBoucles;
  if (iBouclesMax == 0)
    iBouclesMax = 1;
  if (iTempoSuivante == 0)
    iTempoSuivante = 1000;
  if (iTempoErreur == 0)
    iTempoErreur = 1000;
  if (psCmd != NULL)
  {
    // Exécution de la commande
    #ifdef TSTGSM
      Serial.print("Commande [");
      Serial.print(psCmd);
      Serial.print("]");
    #endif
    Serial2.print(psCmd);
    if (iSeqGSM != TXTSMS and iSeqGSM != TXTMAIL)
      Serial2.print("\r");
    if (iSeqGSM == TXTSMS or iSeqGSM == TXTMAIL)
    {
      // CTR+Z pour indiquer la fin du message
      Serial2.print(char(26));        
      Serial2.println();
    }
  }
  if (psReponseA != NULL)
  {
    #ifdef TSTGSM
      Serial.print(" = [");
    #endif
    // Timeout en secondes
    int iTimeOut = 10;
    if (iSeqGSM == OPENGPRS)
      iTimeOut = 120;
    else if (iSeqGSM == COMPTEMAIL or iSeqGSM == SMTPMAIL or iSeqGSM == SENDMAIL)
      iTimeOut = 60;
    else if (iSeqGSM == ENTERPIN or (iSeqGSM >= SETGPRS and iSeqGSM <= STOPGPRS))
      iTimeOut = 30;     
    if (iSeqGSM == ENTERPIN)
      iNbChar = LectureGSM( sTempA, MAXCHARGSM, iTimeOut, ATT_READY);
    else if (iSeqGSM == SENDMAIL)
      iNbChar = LectureGSM( sTempA, MAXCHARGSM, iTimeOut, ATT_SENDMAIL);
    else
      iNbChar = LectureGSM( sTempA, MAXCHARGSM, iTimeOut);
    #ifdef TSTGSM
      Serial.print(sTempA);
      Serial.println("]");
    #endif
    if (iNbChar > 0 and strstr(sTempA, "ERROR") != NULL)
      bError = true;
    else if (iNbChar > 0 and strstr(sTempA, psReponseA) != NULL)
      bReponseA = true;
    else if (psReponseB != NULL and iNbChar > 0 and strstr(sTempA, psReponseB) != NULL)
      bReponseB = true;
  }
  else
    bReponseA = true;
  if (bReponseA)
  {
    // La commande est OK
    bOK = true;
    // Passage à la séquence suivante
    iSeqGSM = iSeqSuivanteA;
    iTimeFinSeq = millis()+iTempoSuivante;
    iBoucles = 0;
  }
  else if (bReponseB)
  {
    // La commande est OK
    bOK = true;
    // Passage à la séquence suivante
    iSeqGSM = iSeqSuivanteB;
    iTimeFinSeq = millis()+iTempoSuivante;
    iBoucles = 0;
  }
  else if (bError or iBoucles >= iBouclesMax)
  {
    // Limitation des chaînes
    if (strlen(psCmd) > 20)
      psCmd[20] = 0;
    if (strlen(psCmd) > 50)
      sTempA[50] = 0;
    // Génération de la chaine d'erreur
    if (bError)
      sprintf( sErrorGSM, "Commande en erreur [%s]=[%s]", psCmd, sTempA);
    else
      sprintf( sErrorGSM, "Commande fin tentatives (%d/%d) [%s]=[%s]", iBoucles, iBouclesMax, psCmd, sTempA);
    // On passe en erreur
    iSeqError = iSeqGSM;
    bError = true;
    iSeqGSM = ERRORGSM;
    iBoucles = 0;
  }
  else
    // Attente pour une prochaine tentative
    iTimeFinSeq = millis()+iTempoErreur;
  return bOK;
}

//----------------------------------------------------
// A appeler régulièrement pour exécuter les séquences GSM demandées
void GSMManager::GestionGSM()
{
  int iNbChar;
  char temp[80];
  bool bOK  = false;
  char *p   = NULL;

  // Selon la séquence
  switch (iSeqGSM)
  {
      // Séquences de réveil du GSM
  case STARTGSM      :  // Mise en route du GSM avec attente 10s
    // La commande est déjà exécutée attente de la tempo avant la prochaine
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( NULL, NULL, NULL, TESTATI, 0, 5000, 5000, 5);
    break;
  case TESTATI       :  // Envoie de la commande ATI et attente retour SIM800
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      bStart = CommandeGSM( "ATI", "SIM800", NULL, TESTPIN, 0, 1000, 5000, 5);
    break;
  case TESTPIN       :  // Envoi de la commande AT+CPIN? pour vérifier si le code PIN est déjà connue
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      bPinOK = CommandeGSM( "AT+CPIN?", "READY", "SIM PIN", ATTENTERESEAU, ENTERPIN, 1000, 5000, 5);
    break;
  case ENTERPIN      :  // Envoi du code PIN avec la commande AT+CPIN=
    if (millis() > iTimeFinSeq)
    {
      sprintf(sTempB, "AT+CPIN=%s", pCPin);
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      bPinOK = CommandeGSM( sTempB, "READY", NULL, ATTENTERESEAU, 0, 10000, 5000, 5);
    }
    break;
  case ATTENTERESEAU :  // Test présence réseau (15 tentatives pour les cas difficiles...)
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      bNetwork = CommandeGSM( "AT+CREG?", ",1", ",5", TESTOPERATEUR, TESTOPERATEUR, 1000, 10000, 15);
    break;
  case TESTOPERATEUR :  // Test de l'opérateur (5 tentatives pour les cas difficiles...)
    if (millis() > iTimeFinSeq)
    {
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      if (CommandeGSM( "AT+COPS?", "+COPS:", NULL, TESTSIGNAL, 0, 1000, 5000, 5))
      {
        // Extraction du nom de l'opérateur
        // 0123456789012345678901
        // +COPS: 0,0,"Orange F"
        // On recherche le 1er "
        p = strchr(sTempA, '"');
        if (p != NULL)
        {
          // Recherche du second "
          char *p2 = strchr(&(p[1]), '"');
          if (p2 != NULL)
            strncpy(OpName, &(p[1]), p2-&(p[1]));
          else
            strncpy(OpName, &(p[1]), strlen(&(p[1]))-2);
          bOK = true;
          // Passage à la séquence suivante
          iSeqGSM = TESTSIGNAL;
          iTimeFinSeq = millis()+1000;
          iBoucles = 0;
          iBouclesMax = 5;
        }
        else
        {
          // Nom d'opérateur absent
          strcpy(OpName, "?????");
          #ifdef TSTGSM
            Serial.println("Operateur introuvable !");
          #endif
        }
      }
    }
    break;
  case TESTSIGNAL    :  // Test de l'intensité du signal
    if (millis() > iTimeFinSeq)
    {
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      if (CommandeGSM( "AT+CSQ", "+CSQ:", NULL, STARTGSMOK, 0, 1000, 5000, 3))
      {
        p = strstr(sTempA, "+CSQ:");
        iSignalQ = 0;
        if (p != NULL)
        {
          // Extraction de l'intensité
          char *p2 = strstr(p, ",");
          if (p2 != NULL)
          {
            *p2 = 0;
            iSignalQ = atoi(&(p[6]));
            if (iSignalQ == 99)
              iSignalQ = 0;
            else
              iSignalQ /= 4; // Pour ramener la qualité de 0 à 7
            #ifdef TSTGSM
                Serial.print("Qualité=");
                Serial.println(iSignalQ);
            #endif
          }
          else
            Serial.println("P2 = NULL:");
        }
        else
          Serial.println("P = NULL:");
      }
    }
    break;

      // Séquences d'émission d'un SMS
  case STARTSMS      :  // Passage en mode SMS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+CMGF=1", "OK", NULL, NUMEROSMS, 0, 1000, 0, 5);
    break;
  case NUMEROSMS     :  // Numérotation du destinataire
    if (millis() > iTimeFinSeq)
    {
      sprintf(sTempC, "AT+CMGS=\"%s\"", pNumero);
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( sTempC, NULL, NULL, TXTSMS, 0, 1000, 1000, 3);
    }
    break;
  case TXTSMS        :  // Texte du SMS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      bSMSOK = CommandeGSM( pTexteSMS, "OK", NULL, SMSOK, 0, 1000, 1000, 3);
    break;

      // Séquences à exécuter pour l'arrêt du GSM
  case RAZSMS        :  // RAZ des SMS reçus et envoyés
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+CMGD=4", "OK", NULL, WAKEUP_GSM, 0, 5000, 1000, 3);
    break;
  case WAKEUP_GSM     :  // Extinction du GSM
    if (millis() > iTimeFinSeq)
    {
      // On stoppe le GSM (ENABLE = 0)
      digitalWrite(WAKEUPGSM, LOW);
      #ifdef TSTGSM
        Serial.println("Broche WAKEUPGSM a 0");
      #endif
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( NULL, NULL, NULL, WAKEUPGSMOK, 0, 0, 0, 1);
    }
    break;

      // Séquences à exécuter pour le passage en GPRS
  case SETGPRS :  // Passage en GPRS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"", "OK", NULL, CONFGPRS, 0, 0, 1000, 3);
    break;
  case CONFGPRS :  // Configuration du mode GPRS (nom du point d'accès ou APN)
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SAPBR=3,1,\"APN\",\"free\"", "OK", NULL, OPENGPRS, 0, 0, 1000, 3);
    break;
  case OPENGPRS :  // Ouverture du mode GPRS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SAPBR=1,1", "OK", NULL, TESTGPRS, 0, 0, 10000, 3);
    break;
  case TESTGPRS :  // Test de l'adresse IP du mode GPRS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SAPBR=2,1", "OK", NULL, GPRSOK, 0, 0, 1000, 3);
    break;

      // Séquences à exécuter pour émettre un mail
  case STARTMAIL :  // Passage en mode mail
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+EMAILCID=1", "OK", NULL, TIMEOUTMAIL, 0, 0, 1000, 0);
    iCodeMail = 0;
    break;
  case TIMEOUTMAIL:  // Init du timeout
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+EMAILTO=60", "OK", NULL, SETSSLMAIL, 0, 0, 1000, 0);
    break;
  case SETSSLMAIL:  // Init du mode SSL
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+EMAILSSL=1", "OK", NULL, SMTPMAIL, 0, 0, 1000, 0);
    break;
  case SMTPMAIL:     // Init de serveur SMTP
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SMTPSRV=\"smtp.free.fr\",465", "OK", NULL, COMPTEMAIL, 0, 0, 1000, 0);
      //CommandeGSM( "AT+SMTPSRV=\"aspmx.l.google.com\",25", "OK", NULL, COMPTEMAIL, 0, 0, 1000, 0);
    break;
  case COMPTEMAIL:   // Adresse mail et mot de passe du compte
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      //CommandeGSM( "AT+SMTPAUTH=1,\"jeando.vrignault\",\"kudu7qwk\"", "OK", "ERROR", MAILFROM, STOPGPRS, 0, 1000, 0);
      CommandeGSM( "AT+SMTPAUTH=1,\"rhinologger\",\"testrhino\"", "OK", NULL, MAILFROM, 0, 0, 1000, 0);
      //CommandeGSM( "AT+SMTPAUTH=1,\"jeando.vrignault@gmail.com\",\"jdv492070\"", "OK", NULL, MAILFROM, 0, 0, 1000, 0);
    break;
  case MAILFROM:     // Adresse mail de l'émetteur
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      //CommandeGSM( "AT+SMTPFROM=\"jeando.vrignault@free.fr\",\"JeanDo\"", "OK", NULL, MAILTO, 0, 0, 1000, 0);
      CommandeGSM( "AT+SMTPFROM=\"rhinologger@free.fr\",\"rhinologger\"", "OK", NULL, MAILTO, 0, 0, 1000, 0);
      //CommandeGSM( "AT+SMTPFROM=\"jeando.vrignault@gmail.com\",\"JeanDo\"", "OK", NULL, MAILTO, 0, 0, 1000, 0);
    break;
  case MAILTO:       // Adresse du destinataire
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      //CommandeGSM( "AT+SMTPRCPT=0,0,\"rhinologger@free.fr\",\"rhinologger\"", "OK", NULL, SUJETMAIL, 0, 0, 1000, 0);
      CommandeGSM( "AT+SMTPRCPT=0,0,\"jeando.vrignault@free.fr\",\"JeanDo\"", "OK", NULL, SUJETMAIL, 0, 0, 1000, 0);
      //CommandeGSM( "AT+SMTPRCPT=0,0,\"jeando.vrignault@gmail.com\",\"JeanDo\"", "OK", NULL, SUJETMAIL, 0, 0, 1000, 0);
    break;
  case SUJETMAIL:    // Sujet du mail
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SMTPSUB=\"RhinoLogger4\"", "OK", NULL, LENGTHMAIL, 0, 0, 1000, 0);
    break;
  case LENGTHMAIL:   // Longueur du texte du mail
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "AT+SMTPBODY=30", "DOWNLOAD", NULL, TXTMAIL, 0, 0, 1000, 0);
    break;
  case TXTMAIL:      // Texte du mail
    if (millis() > iTimeFinSeq) //$$
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      CommandeGSM( "Test emission mail via SIM800L", "OK", NULL, SENDMAIL, 0, 0, 1000, 0);
    break;
  case FILENAME:     // Nom de la pièce jointe
    break;
  case SENDMAIL:     // Emission du mail
    if (millis() > iTimeFinSeq)
    {
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      if (CommandeGSM( "AT+SMTPSEND", "+SMTPSEND:", NULL, ENDMAIL, 0, 0, 1000, 0))
      {
        // Décodage du code retour d'émission du mail [+SMTPSEND: 61]
        p = strstr(sTempA, "+SMTPSEND:");
        iSignalQ = 0;
        if (p != NULL)
        {
          // Extraction du code de retour
          p[13] = 0;
          iCodeMail = atoi(&(p[11]));
          #ifdef TSTGSM
            switch (iCodeMail)
            {
            case  1: Serial.println("Emission mail OK"); break;
            case 61: Serial.println("Network error"); break;
            case 62: Serial.println("DNS resolve error"); break;
            case 63: Serial.println("SMTP TCP error"); break;
            case 64: Serial.println("Timeout of SMTP server response"); break;
            case 65: Serial.println("SMTP server response error"); break;
            case 66: Serial.println("Not authentification"); break;
            case 67: Serial.println("Authentification failed (username or password)"); break;
            case 68: Serial.println("Bad recipient"); break;
            default:
                Serial.print("Erreur inconnue : ");
                Serial.println(iCodeMail);
                break;
            }
          #endif
        }
      }
    }
    break;
  case DATAFILE:     // Contenue de la pièce jointe
    break;
  case ENDMAIL:      // Fin de l'émission du mail
    break;

      // Séquences à exécuter pour stopper le mode GPRS
  case STOPGPRS :  // Arrêt GPRS
    if (millis() > iTimeFinSeq)
      // CommandeGSM( Commande, RéponseA, RéponseB, SeqSuivA, SeqSuivB, TempoNextSeq, TempoNextTentative, NbTentatives)
      if (!CommandeGSM( "AT+SAPBR=0,1", "OK", NULL, STOPGPRSOK, 0, 0, 1000, 0))
        // Arret forcé du GSM
        iSeqGSM = WAKEUP_GSM;
    break;
  }
}
    
//----------------------------------------------------
// Lecture de la ligne série du GSM
// Retourne le nombre de caractères lus
int GSMManager::LectureGSM(
  char *psReponse,    // Pointeur sur la chaine de reponse
  int iMaxChar,       // Taille de la chaine de reponse
  int iDelayMax,      // Temps d'attente max en secondes
  ATTENTEGSM attente  // Type de fin de chaine attendue
  )
{
  // Indice de réception des caractères
  int i=0;
  // Attente max en ms
  int iTimeMax = millis() + (iDelayMax * 1000);
  // Indicateur de réception de "OK" ou fin de message
  bool bOK   = false;
  char *p = NULL;
  // Init de la chaine à 0
  memset(psReponse, 0, iMaxChar);
  // Boucle de réception
  while (!bOK)
  {
    // Si caractères disponibles
    if (Serial2.available())
    {
      // Lecture
      char c = Serial2.read();
      psReponse[i] = c;
      i++;
      psReponse[i] = 0;
      if (i >= (iMaxChar-1))
        // Taille chaine trop courte, on stoppe la lecture
        break;
      switch (attente)
      {
      case ATT_READY:
        if (strstr(psReponse, "SMS Ready\r\n") != NULL)
          bOK = true;
        break;
      case ATT_OK:
        if (strstr(psReponse, "\r\nOK\r\n") != NULL)
          bOK = true;
        break;
      case ATT_SENDMAIL:
        // Recherche +SMTPSEND: 61...OK
        p = strstr(psReponse, "+SMTPSEND:");
        // Si +SMTPSEND: présente et un OK ensuite
        if (p != NULL and strstr(p, "OK") != NULL)
          bOK = true;
        break;
      }
    }
    else
    {
      // Sinon, attente timeout
      if (!bOK and millis() > iTimeMax)
        // Timout lecture
        break;
      else
        // Attente...
        delay(500);
    }
  }
  return i;
}

//------------------------------------------------
// Procédure de lecture d'une ligne dans un fichier
// ReadFile, le fichier à lire
// cLine, buffer de réception de la ligne
// iSize, taille du buffer en caractères
// Retourne le nombre de caractères lus
byte ReadLine(File ReadFile, char *cLine, byte iSize)
{
  byte inByte = 0;
  byte nbChar = 0;
  // Boucle de lecture jusqu'à la fin de la ligne
  while (ReadFile.available())
  {
    // Lecture caractère
    inByte = ReadFile.read();
    if (inByte == 0x0D)
    {
      // Lecture du second caractère de fin de ligne
      inByte = ReadFile.read();
      // Fin de ligne, on sort
      cLine[nbChar] = 0;
      break;
    }
    // Mémo du caractère et passage au suivant
    cLine[nbChar] = inByte;
    nbChar++;
    // Controle du nombre de caractères max
    if (nbChar == iSize - 1)
    {
      cLine[nbChar] = 0;
      break;
    }
  }
  return nbChar;
}


//------------------------------------------------
// Utilitaires pour la gestion de l'horloge externe

//------------------------------------------------
// Calcul du jour de la semaine en fonction de la date
byte dayOfWeek(
  int dayOfMonth,  // Jour du mois
  int month,       // Mois
  int year)        // Année (format raccourci)
{
  int y = year + 2000;
  int c = (14 - month) / 12;
  int a = y - c;
  int m = (month + 12) * (c - 2);
  int j = (dayOfMonth + a + a/4 - a/100 + a/400 + (31*m)/12 ) % 7;
  j += 1;
  return j;
}

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}

//------------------------------------------------
// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}

//------------------------------------------------
// Init de la date et de l'heure sur l'horloge externe
void setDS3231time(
  byte second,      // Secondes
  byte minute,      // Minutes
  byte hour,        // Heures
  byte day,         // Jour du mois
  byte month,       // Mois
  byte year)        // Année (format raccourci)
{
  // Calcul du jour de la semaine
  byte jourSemaine = dayOfWeek(day, month, year);
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second));      // set seconds
  Wire.write(decToBcd(minute));      // set minutes
  Wire.write(decToBcd(hour));        // set hours
  Wire.write(decToBcd(jourSemaine)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(day));         // set date (1 to 31)
  Wire.write(decToBcd(month));       // set month
  Wire.write(decToBcd(year));        // set year (0 to 99)
  Wire.endTransmission();
}

//------------------------------------------------
// Lecture de la date et de l'heure sur l'horloge externe
void readDS3231time(
  byte *second,      // Secondes
  byte *minute,      // Minutes
  byte *hour,        // Heures
  byte *day,         // Jour du mois
  byte *month,       // Mois
  byte *year)        // Année (format raccourci)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second     = bcdToDec(Wire.read() & 0x7f);
  *minute     = bcdToDec(Wire.read());
  *hour       = bcdToDec(Wire.read() & 0x3f);
  byte dayOfWeek  = bcdToDec(Wire.read());
  *day        = bcdToDec(Wire.read());
  *month      = bcdToDec(Wire.read());
  *year       = bcdToDec(Wire.read());
}


/*
  Copyright (c) 2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


static const uint32_t pageSizes[] = { 8, 16, 32, 64, 128, 256, 512, 1024 };

FlashClass::FlashClass(const void *flash_addr, uint32_t size) :
  PAGE_SIZE(pageSizes[NVMCTRL->PARAM.bit.PSZ]),
  PAGES(NVMCTRL->PARAM.bit.NVMP),
  MAX_FLASH(PAGE_SIZE * PAGES),
  ROW_SIZE(PAGE_SIZE * 4),
  flash_address((volatile void *)flash_addr),
  flash_size(size)
{
}

static inline uint32_t read_unaligned_uint32(const void *data)
{
  union {
    uint32_t u32;
    uint8_t u8[4];
  } res;
  const uint8_t *d = (const uint8_t *)data;
  res.u8[0] = d[0];
  res.u8[1] = d[1];
  res.u8[2] = d[2];
  res.u8[3] = d[3];
  return res.u32;
}

void FlashClass::write(const volatile void *flash_ptr, const void *data, uint32_t size)
{
  // Calculate data boundaries
  size = (size + 3) / 4;
  volatile uint32_t *dst_addr = (volatile uint32_t *)flash_ptr;
  const uint8_t *src_addr = (uint8_t *)data;

  // Disable automatic page write
  NVMCTRL->CTRLB.bit.MANW = 1;

  // Do writes in pages
  while (size) {
    // Execute "PBC" Page Buffer Clear
    NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_PBC;
    while (NVMCTRL->INTFLAG.bit.READY == 0) { }

    // Fill page buffer
    uint32_t i;
    for (i=0; i<(PAGE_SIZE/4) && size; i++) {
      *dst_addr = read_unaligned_uint32(src_addr);
      src_addr += 4;
      dst_addr++;
      size--;
    }

    // Execute "WP" Write Page
    NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_WP;
    while (NVMCTRL->INTFLAG.bit.READY == 0) { }
  }
}

void FlashClass::erase(const volatile void *flash_ptr, uint32_t size)
{
  const uint8_t *ptr = (const uint8_t *)flash_ptr;
  while (size > ROW_SIZE) {
    erase(ptr);
    ptr += ROW_SIZE;
    size -= ROW_SIZE;
  }
  erase(ptr);
}

void FlashClass::erase(const volatile void *flash_ptr)
{
  NVMCTRL->ADDR.reg = ((uint32_t)flash_ptr) / 2;
  NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER;
  while (!NVMCTRL->INTFLAG.bit.READY) { }
}

void FlashClass::read(const volatile void *flash_ptr, void *data, uint32_t size)
{
  memcpy(data, (const void *)flash_ptr, size);
}


/*
  dtostrf - Emulation for dtostrf function from avr-libc
  Copyright (c) 2015 Arduino LLC.  All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *dtostrf(double val, int width, unsigned int prec, char *sout)
{
  int decpt, sign, reqd, pad;
  const char *s, *e;
  char *p;
  s = fcvt(val, prec, &decpt, &sign);
  if (prec == 0 && decpt == 0) {
  s = (*s < '5') ? "0" : "1";
    reqd = 1;
  } else {
    reqd = strlen(s);
    if (reqd > decpt) reqd++;
    if (decpt == 0) reqd++;
  }
  if (sign) reqd++;
  p = sout;
  e = p + reqd;
  pad = width - reqd;
  if (pad > 0) {
    e += pad;
    while (pad-- > 0) *p++ = ' ';
  }
  if (sign) *p++ = '-';
  if (decpt <= 0 && prec > 0) {
    *p++ = '0';
    *p++ = '.';
    e++;
    while ( decpt < 0 ) {
      decpt++;
      *p++ = '0';
    }
  }   
  while (p < e) {
    *p++ = *s++;
    if (p == e) break;
    if (--decpt == 0) *p++ = '.';
  }
  if (width < 0) {
    pad = (reqd + width) * -1;
    while (pad-- > 0) *p++ = ' ';
  }
  *p = 0;
  return sout;
}

