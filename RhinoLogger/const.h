/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/* ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION
 Il faut changer les options d'optimisation dans
 C:\Users\"nomuser"\AppData\Local\Arduino15\packages\adafruit\hardware\samd\1.0.17\platform.txt
 Remplacer -Os par -O3 (3 lignes) pour obtenir un gain de vitesse
 A réaliser à chaque nouvelle version AdaFruit
*/

/////////////////////////////////////////////////////////
// Définition des constantes de RhinoLogger

#ifndef CONST_H
#define CONST_H

#include <Wire.h>

// Version du logiciel
//#define VERSION "V0.5" // Prototypes P01 et P02
/* Modifictions pour la version suivante
 * -Modification de la bande RE qui passe de 100-102 à 100-104
 * -Broche WAKEUPGSM utilisation de la broche 12, la broche 9 étant utilisé en interne de la carte Feather comme A7 pour la tension de la batterie interne
 * -Mémo dans les structures temporelles des dates et heures de début de période, la version précédente indiquant la date et heure de fin de période (par ex. 18h pour les infos de 17h à 18h)
 * -CalculFFT, simplification maximale pour gagner du temps CPU
 * -Réveil GSM, boucle de 10 tentatives avec attente de 30s entre chaque tentatives
 * -Emission SMS, attente de 30s entre chaque tentative d'émission si échec et départ à 12h20 à la place de 12h25 pour prendre en compte les attentes en cas d'échac
 * -Tableau de 51200 const short pour la transformation des niveaux en dB (saturation à -25dB)
 * -Mémo des FMin et FMax des contacts par période horaire
 * -Modification possible par l'opérateur des fréquences des bandes et reprise de l'ordre des menus pour avoir la mise en veille en 1ère page
 * -Gain vitesse FFT via utilisation optimisation compilateur - suite cycle mesure 8ms, utilisation 512 échantillons uniquement pour une meilleure précision en fréquence
 * -SMS optionnel sur l'activité par heure de chaque bande
 * -Sécurisation de la récupération des données sur la carte SD via l'attente de 15mn en mode paramètres
 * -Correction bug affichage changement de l'heure et de la date (caractères indésirables en fin de chaine)
 * -Ajout de résultats sous forme de contacts (nombre de tranches de 5s positives)
 * -Correction bug d'envoie des SMS (pas d'émission SMS de façon aléatoire !)
 * -Correction de la détection des bandes RE et PR pour prendre en compte d'éventuelles détections entièrement dans la bande spécifique avant de composer avec la bande commune
 * -Correction de formatage des paramètres de type char et du signal de test 
 * -Ajout d'un menu permettant d'effacer les données courantes
 * -Ajout d'un menu permettant de revenir aux paramètres par défaut
 ****** A réaliser
 * -Vérification bruit moyen et max pour réaliser un seuil pré établi avec gain de sensibilite sur RE et PR avec possibilité de modifier ce seuil de -3db à +20dB
 */
//#define VERSION "V0.7"  // Prototypes P03 à P11 (avec mise à jour P01 et P02)
//#define VERSION "V0.9"  // Série S01 à S32
/*
 * -Correction de l'effacement des données courantes pour revenir à "Non" une fois l'effacement effectué
 * -Ajout de la mémorisation des paramètres sur mise en veille pour garder les éventuelles corrections sans passage en mesure
 * -Amélioration du calcul du bruit moyen en ne prenant pas en compte les niveaux non significatif
 * -Tentative d'acquisition par DMA pour libérer du temps processeur mais ne fonctionne pas (bus processeur trop encombré probablement)
 * -Sécurisation Fe par l'utilisation de l'ADC pour la cadence de lecture
 * -Amélioration de la détection (période 200ms glissante et prise en compte des canaux FFT pour une détection de FM dans la bande FM et de FC dans les autres)
 * -Memorisation systématique du bruit des canaux dans le fichier TBBRUIT.csv
 * -Transmission Bruit max et min dans le SMS journalier
 * -Calcul du bruit uniquement au démarrage
 * -Vérification SD au démarrage
 * -Affichage Bruit et test SD au démarrage
 * -Mémorisation min, moy, max, NbPics et FMax du bruit dans le fichier jour
 * -Optimisation tableau de niveau vers dB pour occuper moins de place
 * -Optimisation RAM via la suppression des tables "temp[255]" locales et utilisation de sTempA[255], sTempB[80] et sTempC[80] globales
 * -Optimisation gestion GSM pour rendre la main à chaque séquence et pouvoir afficher la progression du test SMS
 * -Codage partiel émission mail (cf. TSTMAIL dans const.h)
 * -Par défaut, 99-102.5kHz pour les RE et 102.6-106.5 pour EP et 106.6-117 pour les PR (recommandations Barataud)
 * -Par défaut 20dB de seuil, Gain x4 et seuil FC 5, 15, 10, 10, 10
 */
//#define VERSION "V1.0"
/*
 * - Prise en compte de la possibilité d'un code PIN vide dans le test de présence GSM. Teste uniquement l présence d'au moins un numéro
 * - Magnitude de sortie de la FFT, prise en compte des valeurs négatives en faisant ABS(fft[n])
 */
#define VERSION "V1.1"
/*
 * - Correction décrémentation LstSommeDet dans gestMesure.cpp ( if (Lst25Det[i] & 0x00000001 > 1 and LstSommeDet[i] > 0) remplacé par  if (Lst25Det[i] & 0x00000001 > 0 and LstSommeDet[i] > 0))
 */
#define VERSION "V1.2"

// Pour un reset à 12h30 chaque jour
#define RESET12H30

// Temps d'affichage du logo au démarrage (en 1/10 s)
#define TEMPSAFFLOGO 2000

// Temps d'affichage des résultats de mesure de bruit et test SD (en s)
#define TEMPSAFFBRUIT 15

// Temps d'affichage des mesures avant extinction de l'écran (en s)
#define TEMPSAFFMES 30

// Temps de retour automatique en mode mesure en absence de frappe de touche en mode paramètres (en s)
#define TEMPS_RETOUR_MES_AUTO 1200  // 20 minutes pour laisser le temps d'un changement de carte

// Pour le fonctionnement en mode DMA
//#define TEST_DMA

// Pour le debug de la restitution du contexte
//#define DEBUGCONTEXT

// Définition des broches d'entrées/sorties
#define GPIO_RX     0  // Entrée console/USB RX
#define GPIO_TX     1  // Sortie console/USB TX
#define OLED_RESET  2  // Reset de l'écran OLED mais non utilisé
#define SEL_SD      4  // Chip select de la carte SD
#define KEYVAL      5  // Entrée digitale de la touche validation (cf. note ci-dessous)
#define DHTPIN      6  // Entrée digitale du détecteur de température et humidité DHT11
#define DET_SD      7  // Entrée de détection de la carte SD
#define LED_SD      8  // LED pour indiquer les lectures/écritures sur la carte SD
#define GPIO09      9  // A7 pour la lecture de la tension de la batterie interne
#define RX_GSM      10 // RX GSM
#define TX_GSM      11 // TX GSM
#define WAKEUPGSM   12 // Broche de réveil du GSM
#define TESTOUT     13 // Sortie digitale de mesure des temps
#define USINPUT     A1 // Entrée analogique audio
#define EXTPWR      A3 // Entrée analogique de mesure de l'alimentation externe
#define KEYBOARD    A4 // Entrée analogique des 4 touches du clavier directionnel
#define INTPWR      A7 // Entrée analogique de mesure de la batterie interne
// Note sur le clavier : impossible d'avoir toutes les touches clavier sur une entrée analogique,
// ce n'est pas compatible avec l'acquisition. Pour compenser, la touche de validation est de type logique
// et un appuis dessus est détecté sans utiliser l'ADC.
// Suite à un appuis sur la touche "Validation", on passe en mode paramètres
// Après 120s sans appuis, on repasse en mode acquisition

// Pour afficher la valeur max des échantillons sur une seconde
//#define TESTECHMAX

// Taille max d'une réponse reçue du GSM
#define MAXCHARGSM 255

// Mode d'affichage des mesures
enum MODEAFFMES
{
  MODEMESBRUIT  = 0,  // Mode affichage attente calcul bruit
  MODEMESAFFBR  = 1,  // Mode affichage infos bruit et SD
  MODEMESAFFMES = 2   // Mode affichage normal des mesures
};

// Pour des traces sur l'utilisation du GSM
#define TSTGSM

// Pour le test MAIL
//#define TSTMAIL

// Type d'attente sur lecture du GSM
enum ATTENTEGSM
{
  ATT_OK,       // Attente cr/lf/OKcr/lf/
  ATT_READY,    // Attente SMS Readycr/lf/
  ATT_SENDMAIL, // Attente OK...+SMTPSEND: 61...OK
  ATT_NO        // Attente sur timeout
};

// Liste des actions d'émission des SMS
enum LSTACTIONSSMS
{
  STARTGSM  = 0,   // Démarrage du GSM
  INITGSM   = 1,   // Initialisation du GSM
  SMSA      = 2,   // SMS du n°A
  SMSB      = 3,   // SMS du n°B
  SMSC      = 4,   // SMS du n°C
  SMSFMA    = 5,   // SMS bande FM du n°A
  SMSFMB    = 6,   // SMS bande FM du n°B
  SMSFMC    = 7,   // SMS bande FM du n°C
  SMSGRA    = 8,   // SMS bande GR du n°A
  SMSGRB    = 9,   // SMS bande GR du n°B
  SMSGRC    = 10,  // SMS bande GR du n°C
  SMSREA    = 11,  // SMS bande RE du n°A
  SMSREB    = 12,  // SMS bande RE du n°B
  SMSREC    = 13,  // SMS bande RE du n°C
  SMSEPA    = 14,  // SMS bande EP du n°A
  SMSEPB    = 15,  // SMS bande EP du n°B
  SMSEPC    = 16,  // SMS bande EP du n°C
  SMSPRA    = 17,  // SMS bande PR du n°A
  SMSPRB    = 18,  // SMS bande PR du n°B
  SMSPRC    = 19,  // SMS bande PR du n°C
  STARTGPRS = 20,  // Passage en mode GPRS
  SENDMAIL  = 21,  // Emission mail
  STOPGPRS  = 22,  // Arrêt du mode GPRS
  STOPGSM   = 23,  // Arrêt du GSM
  FINGSM    = 24   // Fin des actions GSM
};

// Définition des adresses I2C
#define DS3231_I2C_ADDRESS 0x68   // Horloge externe

// Définition des constantes des valeurs des boutons
#define NOBUT    0  // Pas de bouton pressé
#define BDOWN    1  // Bouton bas
#define BUP      2  // Bouton haut
#define BRIGHT   3  // Bouton droit
#define BLEFT    4  // Bouton gauche
#define BSELECT  5  // Bouton sélection/validation
#define NBANAKEY 4  // Nombre de boutons analogique

// Définition des modes de fonctionnement
#define MODE_MESURE  0  // Mode mesure
#define MODE_DEBUG   1  // Mode debug
#define MODE_PARAMS1 2  // Mode modification des paramètres page 1
#define MODE_PARAMS2 3  // Mode modification des paramètres page 2
#define MODE_PARAMS3 4  // Mode modification des paramètres page 3
#define MODE_SMS     5  // Mode émission d'un SMS
#define MODE_TESTSMS 6  // Mode émission d'un SMS de test
#define MODE_ATTENTE 7  // Mode attente du reset
#define MODE_VEILLE  8  // Mode veille

// Type du capteur de température et d'humidité
//#define DHTTYPE DHT11 // DHT 11
#define DHTTYPE DHT22 // DHT 22 (AM2302)
//#define DHTTYPE DHT21 // DHT 21 (AM2301)

// Nombre max de caractère d'une ligne de l'écran OLD +1 pour le dimensionnement des chaines
#define MAXLINE 22

// Nombre max de lignes sur l'écran
#define NBLINE  8

// Pour tester les différents timer avec un scope sur la pin 13 (un seul à la fois)
enum SIGNAL_TEST {
  NORMAL    =  0,  // 'N' Pas de signal de test, l'écran s'éteint automatiquement après le 1er changement de minute
  NORMAL_AFF=  1,  // 'n' Pas de signal de test, l'écran reste allumé
  TESTACQ   =  2,  // 'A' Permet de mesurer le temps d'acquisitions des échantillons
  TESTCycle =  3,  // '1' Permet de tester la callback d'un cycle de mesure (8ms)
  TESTFFT   =  4,  // 'F' Permet de tester le calcul FFT
  TEST200ms =  5,  // '2' Permet de tester la callback 200ms
  TESTMes   =  6,  // ' ' Permet de tester le temps de mesures (Acquisition, FFT et Traitements)
  TESTAffMax=  7,  // 'M' Permet d'afficher les 7 fréquences de niveaux les plus élevés
  TESTAffFM =  8,  // 'a' Permet d'afficher les niveaux des 7 1er canaux de la bande FM
  TESTAffGR1=  9,  // 'b' Permet d'afficher les niveaux des 7 1er canaux de la bande GR
  TESTAffGR2= 10,  // 'c' Permet d'afficher les niveaux des 7 dernier canaux de la bande GR
  TESTAffRE = 11,  // 'd' Permet d'afficher les niveaux des 7 1er canaux de la bande RE
  TESTLoop  = 12   // 'L' Permet de tester le temps de traitement du loop
};

// Pour un gain numérique (translation de n bits), uniquement pour DAC en 12 bits
enum GAINUM
{
  GAIN_1  = 0,  // Pas de gain numérique                  (avec les résultats ci-dessous max ech  2048 dB = -69)
  GAIN_2  = 1,  // Multiplication des échantillons par 2  (avec les résultats ci-dessous max ech  4096 dB = -64)
  GAIN_4  = 2,  // Multiplication des échantillons par 4  (avec les résultats ci-dessous max ech  8192 dB = -58)
  GAIN_8  = 3,  // Multiplication des échantillons par 8  (avec les résultats ci-dessous max ech 16384 dB = -52)
  GAIN_16 = 4   // Multiplication des échantillons par 16 (avec les résultats ci-dessous max ech 32765 dB = -46)
};

/* Infos de calcul pour la transformation en dB
 * Niveau en dB = 20*LOG(Niveau)-120
 * En-dessous de 1024, on a un tableau de short qui nous donne les dB en dixièmes de dB directement (taille du tableau 2048)
 * Ce tableau va de -120.0dB à -59.7dB
 * Au-dessus, on a un tableau de 5120 de byte qui nous donne les dB pour chaque 1/10 de niveau
 * Ce tableau va de -60dB (niveau 1025) à -25.6dB (niveau 52224)
 * Au-dessus, on impose -25dB signifiant la saturation
*/
#define MAXTBDBA    1024   // Taille de la table A niveau vers dB
#define MAXTBDBB    5120   // Taille de la table B niveau vers dB
#define MINDBNS     -1200   // Valeur min du niveau non significatif. Les valeurs inférieures ou égales ne sont pas prises en compte dans le calcul du bruit
#define MAXDIFBRUIT 200     // Différence maximum entre le bruit moyen de toute la bande et le bruit moyen d'un canal pour ne pas prendre en compte un canal avec un son actif

// Pour des traces sur le calcul du bruit
//#define TRACENOISE

// Nombre max d'échantillons
#define MAXECH 512

// Lecture 8 ou  12 bits sur le DAC
//#define DIV8_8BITS
#define DIV8_12BITS

// Nombre de mesures pour le calcul du bruit
#define MAXMESURESNOISE 500

// Définition des indexs des bandes et du nombre de bandes
#define NBBANDES  5 // 5 bandes de surveillance
#define BFM 0       // Indice de la bande FM 40-60kHz
#define BGR 1       // Indice de la bande Grand Rhinolophe 76-86kHz
#define BRE 2       // Indice de la bande Rhinolophe Euryale 100-104kHz
#define BEP 3       // Indice de la bande commune Euryale Petit Rhino 104-106.4kHz
#define BPR 4       // Indice de la bande Petit Rhinolophe 106.4-120kHz

//------------------------------------------------
// Structure et variable de sauvegarde des infos de la période de 200ms
struct StructMemo200ms
{
    // Liste du nombre de contacts pour les différentes espèces
    int LstContacts[NBBANDES];
    // Liste des niveaux max des différentes espèces sans se préocuper si détection ou non
    short LstNivMax[NBBANDES];
    // Indice de la Fréquence max détectée pour euryale et petit rhino
    unsigned short iFFMax;
    // Contact d'indice min
    unsigned short LstFMin[NBBANDES];
    // Contact d'indice max
    unsigned short LstFMax[NBBANDES];
};

//------------------------------------------------
// Structure et variable de sauvegarde des infos de la seconde
struct StructMemoS
{
    // Liste des booléens de présence de contacts des différentes espèces
    bool LstContacts[NBBANDES];
    // Liste des niveaux max des différentes espèces sans se préocuper si détection ou non
    short LstNivMax[NBBANDES];
    // Indice de la Fréquence max détectée pour euryale et petit rhino
    unsigned short iFFMax;
    // Contact d'indice min
    unsigned short LstFMin[NBBANDES];
    // Contact d'indice max
    unsigned short LstFMax[NBBANDES];
};

//------------------------------------------------
// Structure de sauvegarde des infos d'une période temporelle
struct StructMemoPeriodeT
{
    // Liste du nombre de secondes positives des différentes espèces
    unsigned long LstSecPos[NBBANDES];
    // Liste du nombre de minutes positives des différentes espèces
    unsigned long LstMnPos[NBBANDES];
    // Liste du nombre de contacts (tranches de 5s positives) des différentes espèces
    unsigned long LstContacts[NBBANDES];
    // Liste des niveaux max des différentes espèces sans se préocuper si détection ou non
    short LstNivMax[NBBANDES];
    // Contact d'indice min
    unsigned short LstFMin[NBBANDES];
    // Contact d'indice max
    unsigned short LstFMax[NBBANDES];
    // Date de début de la période
    char  strDate[12];
    // Heure de début de la période
    char  strHeure[12];
    // Bruit moyen de 40 à 120kHz
    short iBruitMoyen;
};

//------------------------------------------------
// Structure de sauvegarde des mesures annexes
struct StructMesuresAnnexes
{
    // Température en degrées (min dans le cas des jours)
    float fTemp;
    // Humidité en % (min dans le cas des jours)
    float fHumid;
    // Température en degrées (min pour le cas des jours)
    float fTempMin;
    // Humidité en % (min pour le cas des jours)
    float fHumidMin;
    // Température en degrées (max pour le cas des jours)
    float fTempMax;
    // Humidité en % (max pour le cas des jours)
    float fHumidMax;
    // Tension de la batterie externe en v
    float fBatExt;
    char  sBatExt[15];
    // Tension de la batterie interne en v
    float fBatInt;
    char  sBatInt[15];
};

//------------------------------------------------
// Structure de sauvegarde des mesures courantes
struct StructAllMesures
{
  StructMemo200ms      memo200ms;               // Mémo de la période de 200ms courante
  StructMemoS          memoSec;                 // Mémo de la seconde courante
  StructMemoS          saveSec;                 // Mémo de la seconde précédente pour l'affichage
  StructMemoPeriodeT   memoM;                   // Mémo de la minute courante
  StructMemoPeriodeT   memoH;                   // Mémo de l'heure courante
  StructMemoPeriodeT   memoJ;                   // Mémo de la journée courante
  StructMesuresAnnexes memoAnnexes;             // Mémo des mesures annexes
  short                TbActivJ[NBBANDES][24];  // Tableau des activités par heure pour les SMS optionnels
  char                 Erreur[MAXLINE];         // Eventuel texte de la dernière erreur rencontrée
  short                iEchMax;                 // Valeur max des échantillons sur une seconde
  int                  iJour;                   // Jour de la dernière mesure pour ne pas prendre en compte les contextes de plus de 24h
};

//------------------------------------------------
// Structure de sauvegarde des infos de bruit et carte SD
struct StructNoiseAndSD
{
    short      iNoiseMin;           // Bruit minimum
    short      iNoiseMoy;           // Bruit moyen
    short      iNoiseMax;           // Bruit maximum
    short      iNbPicNoise;         // Nombre de pics de bruit > 5dB
    float      fFreqMaxNoise;       // Fréquence du bruit max
    float      fFreeRam;            // RAM libre en kO
    char       sTypeSD[32];         // Type de la carte SD
    char       sFileSD[32];         // Test écriture fichier
    MODEAFFMES ModeAffMes;          // Pour afficher une seule fois les infos de bruit et SD
};

#define MAXCODEPIN  5 // Nombre de caractères du code pin
#define MAXNUMTEL  11 // Nombre de caractères composant un n° de téléphone

//------------------------------------------------
// Structure de sauvegarde des paramètres du programme (modifiables par l'opérateur)
struct StructParams
{
  short sControle;            // Mot de controle pour le stockage en flash (5555)
  char  sNomStation[MAXLINE]; // Nom de la station (12 caractères maximum)
  short sSeuilRelatif;        // Seuil relatif en dB au-dessus du bruit moyen
  char  sCodePin[MAXCODEPIN]; // Code PIN de l'option GSM
  char  sNumTel1[MAXNUMTEL];  // Liste des n° de téléphone pour le SMS
  char  sNumTel2[MAXNUMTEL];  // Liste des n° de téléphone pour le SMS
  char  sNumTel3[MAXNUMTEL];  // Liste des n° de téléphone pour le SMS
  float fCoefBExt;            // Coefficient du pont diviseur pour la mesure de la batterie externe pour compenser l'imprécision des résistances
  float fCoefBInt;            // Coefficient du pont diviseur pour la mesure de la batterie interne pour compenser l'imprécision des résistances
  SIGNAL_TEST iSignalTest;    // Indique le type de test sur la broche 13
  GAINUM iGain;               // Gain numérique
  char  sNomFM[5];            // Nom de la bande FM qui est modifiable
  float fFMinB[NBBANDES];     // Liste des FMin des bandes
  float fFMaxB[NBBANDES];     // Liste des FMax des bandes
  short sSeuilFC[NBBANDES];   // Seuil du nombre de détections dans 200ms pour décrèter une FC
  bool  bSMSB[NBBANDES];      // Liste des booléens indiquant si SMS pour la bande
};

//------------------------------------------------
// Structure de sauvegarde des résultats en mode debug niveau max
struct StructDebug
{
  short iLevelMax;  // Niveau max
  short idBMax;     // Niveau max en 1/10 de dB
  short idxMax;     // Index du niveau max
  float fFreqMax;   // Fréquence du niveau max
};

//------------------------------------------------
// Structure de sauvegarde des résultats en mode debug fréquences
struct StructDebFr
{
  short idB;        // Niveau max en 1/10 de dB sur 1 seconde
  short idBMax;     // Niveau max en 1/10 de dB sur 1 minute
  short iNoise;     // Bruit du canal en 1/10 de dB
  short idxMax;     // Index du niveau max
  float fFreqMax;   // Fréquence du niveau max
};

#endif

