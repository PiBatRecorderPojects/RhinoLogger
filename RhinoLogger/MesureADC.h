/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de gestion de l'acquisition
// des échantillons via l'ADC

#ifndef MESUREADC_H
#define MESUREADC_H

#include "const.h"

//----------------------------------------------------
// Classe de mesure des échantillons
class MesureADC
{
  public:
    //----------------------------------------------------
    // Constructeur
    MesureADC(
      const StructParams *pParams // Pointeur sur les paramètres
      );

    //----------------------------------------------------
    // Initialisation des mesures du convertisseur
    void InitMesures();

    //----------------------------------------------------
    // Initialisation du convertisseur
    // (à faire après une lecture normale d'un convertisseur - analogRead)
    void InitADC();

    //----------------------------------------------------
    // Initialisation du DMA
    void InitDMA();

    //----------------------------------------------------
    // Lecture des échantillons via l'ADC et le DMA
    void DMA_ADC();

    //----------------------------------------------------
    // Lance une acquisition des échantillons
    void LanceMesures();

    //----------------------------------------------------
    // Fonction de traitement des interruptions DMA
    void OnITDMA();

    //----------------------------------------------------
    // Retourne true si les échantillons sont lus
    bool lectureOK();

    //----------------------------------------------------
    // Retourne le tableau des mesures (partie réelle)
    short *getEchs();

    //----------------------------------------------------
    // Retourne le tableau des mesures (partie immaginaire à 0)
    short *getImm();

    //----------------------------------------------------
    // Retourne la fréquence d'échantillonnage en Hz
    int getFE() {return FE;};

    static MesureADC *pThis;          // Pointeur sur l'instance de la classe
    
  protected:
    short    TabEchA[MAXECH];         // Tableau A des échantillons (partie réelle), 512 échantillons max
    short    TabEchB[MAXECH];         // Tableau B des échantillons (partie réelle), 512 échantillons max
    short    TabImm [MAXECH];         // Tableau des échantillons (partie immaginaire à 0)
    uint16_t iNbEch;                  // Nombre d'échantillons lus
    uint16_t iMaxEch;                 // Nombre d'échantillons max (256 ou 512);
    int      FE;                      // Fréquence d'échantillonnage
    int      iGainNum;                // Gain numérique
    const StructParams *params;       // Pointeur sur les paramètres
    uint32_t chnl;                    // Canal du DMA
    volatile uint32_t dmadone;        // Indicateur de fin d'acquisition
    volatile short *pDataIn;          // Pointeur sur les données en cours d'acquisition
    volatile short *pDataOK;          // Pointeur sur les données disponibles
    volatile bool bBlocEnCours;       // True indique qu'un transfert est en cours
    volatile bool bBlocOK;            // True si l'acquisition du dernier bloc est OK
    volatile bool bBlocStop;          // True si l'acquisition à été suspendu
    volatile bool bBlocErreur;        // True si l'acquisition est en erreur
    // Structure et variables pour l'initialisation du DMA
    typedef struct {
        uint16_t btctrl;
        uint16_t btcnt;
        uint32_t srcaddr;
        uint32_t dstaddr;
        uint32_t descaddr;
    } dmacdescriptor;
    volatile dmacdescriptor wrb[12] __attribute__ ((aligned (16)));
    dmacdescriptor descriptor_section[12] __attribute__ ((aligned (16)));
    dmacdescriptor descriptor __attribute__ ((aligned (16)));
};

#endif

