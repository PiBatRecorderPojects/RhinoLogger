/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de gestion des timers
// et des séquences temporelles du programme
// N'effectue aucun traitement, il faut redéfinir
// la classe et les méthodes de gestion des différentes
// séquences pour y effectuer des traitements
// ATTENTION une seule instance n'est autorisée par n° de timer

#ifndef TIMER_H
#define TIMER_H

#include "Arduino.h"

//----------------------------------------------------
// Classe de gestion d'un timer
class ZeroTimer
{
  public:
    //----------------------------------------------------
    // Constructeur du timer
    ZeroTimer(
      uint8_t  tn,         // N° du timer (3, 4 ou 5) Attention le 4 semble utilisé pour la fonction micros()
      uint16_t prescaler,  // Prédivision de l'horloge
      uint8_t  period);    // n ms x 5.587 (approximativement) pour un prescaler TC_CTRLA_PRESCALER_DIV64
      
    //----------------------------------------------------
    // Initialisation de nouvelles valeurs pour le prescaler et la période
    void SetPeriod(
      uint16_t prescaler,  // Prédivision de l'horloge
      uint8_t  period);    // n ms x 5.587 (approximativement) pour un prescaler TC_CTRLA_PRESCALER_DIV64
      
    //----------------------------------------------------
    // Fonction d'initialisation du timer
    // Retourne true si le timer est OK
    bool initTimer();

    //----------------------------------------------------
    // Lance ou stoppe le timer
    void enable(
      boolean en); // true pour lancer, false sinon

    //----------------------------------------------------
    // Fonction virtuelle exécutée sur l'évenement timer
    virtual void timerHandler();

  protected:
    //----------------------------------------------------
    // Fonction d'initialisation du timer 3
    // Retourne true si le timer est OK
    bool InitTimer3();
    
    //----------------------------------------------------
    // Fonction d'initialisation du timer 4
    // Retourne true si le timer est OK
    bool InitTimer4();
    
    //----------------------------------------------------
    // Fonction d'initialisation du timer 5
    // Retourne true si le timer est OK
    bool InitTimer5();
    
    // Numéro du timer
    uint8_t  _timernum;
    // Prédivision
    uint16_t _prescaler;
    // Période du timer
    uint8_t  _period;
    // Inique si le timer est actif ou non
    bool     _bEnable;
};

#endif

