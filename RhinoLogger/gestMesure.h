/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de gestion des périodes de mesures
// Gère les périodes 8ms, 200ms, 1s, 1mn, 1h et 1 jour
//
// Les périodes 8ms et 200ms sont gérées par timer
// Les autres via la fonction loop et le test de l'heure interne

#ifndef GESTMESURE_H
#define GESTMESURE_H

#include <RTCZero.h>
#include "const.h"
#include "timer.h"
#include "MesureADC.h"


//----------------------------------------------------
// Classe de mesure des échantillons
class GestMesures: public ZeroTimer
{
  public:
    //----------------------------------------------------
    // Constructeur du timer, utilise le timer 5 pour cadencer à 8ms
    GestMesures(
      StructParams *pParams // Pointeur sur les paramètres
      );

    //----------------------------------------------------
    // Initialisation des mesures
    void InitMesures(
      bool bRAZ // Indique s'il faut faire une RAZ des données suite à une demande opérateur
      );

    //----------------------------------------------------
    // Fonction virtuelle exécutée sur l'évenement timer 8ms
    virtual void timerHandler();

    //----------------------------------------------------
    // Test de présence d'activités
    void TestActivites();
    
    //----------------------------------------------------
    // Calcul du bruit moyen
    void CalculBruit();
    
    //----------------------------------------------------
    // Fonction de traitement sur 200 ms
    void CallBack200ms();

    //----------------------------------------------------
    // Fonction de traitement des périodes 1s, 1h et 1j appelée
    // dans la boucle loop du programme principal
    // Retourne true si traitement une seconde
    bool loop();

    //----------------------------------------------------
    // Fonction de traitement sur 1 seconde
    void CallBack1s();

    //----------------------------------------------------
    // Fonction de traitement sur 1 mn
    void CallBack1mn();

    //----------------------------------------------------
    // Fonction de traitement sur 1 h
    void CallBack1h();

    //----------------------------------------------------
    // Fonction de traitement sur 1 jour
    void CallBack1j();

    //----------------------------------------------------
    // Fonction de mémorisation des infos de debug des niveaux max
    void MemoDebugNivMax();

    //----------------------------------------------------
    // Fonction de mémorisation des infos de debug des fréquences
    void MemoDebugFreq();

    //----------------------------------------------------
    // Fonction de mémorisation des infos dans un fichier csv
    void MemoSD(
      char *pFileName,          // Nom du fichier
      StructMemoPeriodeT *data, // Données à écrire
      bool bJour=false          // Indique si c'est un fichier jour
      );

    //----------------------------------------------------
    // RAZ des infos de la période 200ms
    void RAZMemo200ms();

    //----------------------------------------------------
    // RAZ des infos de la seconde
    void RAZMemoS();

    //----------------------------------------------------
    // RAZ des infos de mémorisation
    void RAZMemoT(
      StructMemoPeriodeT *memoX  // Pointeur sur les infos à effacer
      );

    //----------------------------------------------------
    // RAZ des infos de debug d'affichage des niveaux max
    void RAZDebugNivMax();

    //----------------------------------------------------
    // RAZ des infos de debug d'affichage des fréquences par bloc de 7
    void RAZDebugFreq(
      bool bMax1mn  // Pour effacer le max toute les minutes
      );

    //----------------------------------------------------
    // RAZ des infos du bruit
    void RAZNoise();

    //----------------------------------------------------
    // Lecture des mesures complémentaires
    void LectureTHV(
      bool bInitADC = true // True pour initialiser l'ADC en mode mesure, false sinon
      );

    //----------------------------------------------------
    // Construction d'un SMS vide
    void InitSMS();

    //----------------------------------------------------
    // Construction du SMS de synthèse journalier
    void CreateSMSSynthese();

    //----------------------------------------------------
    // Construction du SMS de synthèse par bande
    void CreateSMSBande(
      int iB  // Indice de la bande
      );

    //----------------------------------------------------
    // Sauvegarde des résultats partiel en quittant le mode mesure
    void SauveContexte();

    //----------------------------------------------------
    // Lecture des résultats partiels en reprenant le mode mesure
    void LectureContexte();

    //----------------------------------------------------
    // Mémorisation d'une info dans le LOG
    void MemoLog(
      char *pStr    // Info à sauvegarder
      );

    //----------------------------------------------------
    // Remplace le point décimal par une virgule
    char *SwapDecimal(
      char *pStr    // Chaîne à modifier
      );

    //----------------------------------------------------
    // Fonction de transformation des niveaux en dB
    inline short GetNivDB( int iNiv);
    
    //----------------------------------------------------
    // Mémorisation du bruit du bruit des canaux dans un fichier
    void MemoFichierBruit();

    //----------------------------------------------------
    // Test d'écriture de la carte SD
    void TestFileSD();

    // Variables publiques
    char       strDate[12];         // Date courante
    char       strHeure[12];        // Heure courante
    char       strSMS[255];         // Texte du SMS de synthèse journalier
    char       strBSMS[NBBANDES][200]; // Textes des SMS de synthèse des bandes
    bool       bRunMesures;         // Indique si les mesures sont actives ou non
    StructAllMesures mesures;       // Mesures courantes
    StructDebFr mesFreq[NBLINE];    // Mesures en mode debug affichage des niveaux de chaques canaux par bloc de 7
    StructDebug mesDebug[NBLINE+3]; // Mesures en mode debug affichage des 7 fréquences de niveaux les plus élevés
    short      TabIdxMin[NBBANDES]; // Table des indices min des bandes surveillées
    short      TabIdxMax[NBBANDES]; // Table des indices max des bandes surveillées
    int        idxFreq;             // Index de la 1ère fréquence en mode debug affichage des fréquences
    int        currentSec;          // Seconde courante
    int        currentMn;           // Minute courante
    int        currentHh;           // Heure courante
    StructNoiseAndSD mesBruitSD;    // Mémo des infos de bruit et carte SD
    
  protected:
    // Variables privées
    MesureADC          mesureADC;             // Mesure des échantillons
    long               lms;                   // Compteur 200ms
    long               ltTimer;               // Temps du timer en fonction du nombre d'échantillons
    short              *pEch;                 // Pointeur sur le tableau des échantillons (partie réelle)
    short              *pImm;                 // Pointeur sur le tableau des échantillons (partie immaginaire à 0)
    char               FileNameMN[15];        // Nom du fichier de mémo des minutes
    char               FileNameH[15];         // Nom du fichier de mémo des heures
    char               FileNameJ[15];         // Nom du fichier de mémo des jours
    short              sSeuilQFC[NBBANDES];   // Seuil FC pour chaque bande
    StructParams       *params;               // Pointeur sur les paramètres
    float              fPrecis;               // Précision d'un pas en kHz
    float              fDemi;                 // Valeur d'un demis pas en kHz
    int                iPrecis;               // Précision d'un pas en Hz
    int                iDemi;                 // Valeur d'un demis pas en Hz
    int                iTabNoise[MAXECH/2];   // Table des niveaux pour le calcul et la mémorisation du bruit de chaque canaux
    int                iTabNbNoise[MAXECH/2]; // Table du nombre de niveaux >-120dB lors du calcul du bruit de chaque canaux
    short              iTabSeuil[MAXECH/2];   // Table des seuils de détection de chaque canaux
    bool               bNoiseOK;              // Indique que la mesure de bruit est OK
    int                iNbNoise;              // Nombre de mesures pour le calcul du bruit
    int                iNbEch;                // Nombre d'échantillons
    float              fFE;                   // Fréquence d'échantillonnage
    int                iSizeFFT;              // Taille de la FFT en puissance de 2
    int                iH;                    // Indice de l'heure courante
    short              iCompteur5S;           // Compteur 5s pour le calcul des contacts
    uint32_t           Lst25Det[MAXECH/2];    // Liste de mots de 32bits. Les 25 bits de poids faibles représentent les 25 dernières mesures du canal FFT. 0=Pas de détection dans le canal, 1=Détection
    uint8_t            LstSommeDet[MAXECH/2]; // Liste d’octets. Chaque octet représente la somme des détections sur les 25 dernières mesures. Si ce nombre est supérieur au seuil, alors seconde positive

};

#endif

