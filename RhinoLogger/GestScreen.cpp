/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Classe de gestion de l'afficheur OLED

#include <RTCZero.h>
#include "GestScreen.h"
#include "utilit.h"

extern const unsigned char LogoGR [];

// RTC interne au processeur et valeurs de l'heure et la date
extern RTCZero rtcZero;

//----------------------------------------------------
// Constructeur du gestionnaire d'écran OLED
GestScreen::GestScreen(
  StructParams *pPar // Pointeur sur les paramètres
  ):screen(OLED_RESET)
{
  pmesures = NULL;
  pParams = pPar;
  bModif = false;
  bActif = true;
  iModifY = 0;
  iModifX = 0;
  iLigneActive = 0;
  bTestSMS = false;
  mSMS = false;
  iMnErreur = 0;
  bVeille = false;
  bModeVeille = false;
  bModifCoeff = false;
  bParamDef = false;
  bIsRAZ = false;
  memset(sLigneModif, 0, 80);
}

//----------------------------------------------------
// Initialisation de l'écran
void GestScreen::InitScreen()
{
  // Initialisation de l'écran
  screen.begin(SH1106_SWITCHCAPVCC, 0x3C);
  // Affichage du logo
  AffLogo();
}

//----------------------------------------------------
// Init de la ligne active
void GestScreen::SetLigneActive(
  int iLine // Ligne active (0 par défaut)
  )
{
  iLigneActive = iLine;
}
  
//----------------------------------------------------
// RAZ de l'écran
void GestScreen::RAZDisplay()
{
  screen.clearDisplay();
  screen.display();
}

//----------------------------------------------------
// Affichage d'infos du bruit et de la carte SD
void GestScreen::AffBruitAndSD(
  StructNoiseAndSD   *pMesNoiseSD, // Pointeur sur les mesures
  StructAllMesures   *pMesures     // Pointeur sur les mesures
  )
{
  int y = 0;
  if (pMesNoiseSD->ModeAffMes == MODEMESBRUIT)
  {
    // Effacement de l'écran
    screen.clearDisplay();
    screen.setTextSize(1);
    screen.setTextColor(WHITE, BLACK);
    //              012345678901234567890
    // Ligne 1     "Bruit(dB) Min Moy Max"
    screen.setCursor(0,y);
    screen.print("Mesure bruit...");
    y += 8;
    // Ligne 2 à 5
    for (int i=2; i<=5; i++)
      y += 8;
    // Ligne 6 "RAM libre 16kO  "
    dtostrf( pMesNoiseSD->fFreeRam, 5, 2, sTempB);
    sprintf( sTempA, "RAM libre %s kO", sTempB);
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
    // Ligne 7      "T+15H80 B 12,5 3,7 V"
    if (strlen(pMesures->memoAnnexes.sBatInt) > 0)
    {
      int t = (int)pMesures->memoAnnexes.fTemp;
      int h = (int)pMesures->memoAnnexes.fHumid;
      sprintf( sTempA, "T %+3d %2d B%s %s V", t, h, pMesures->memoAnnexes.sBatExt, pMesures->memoAnnexes.sBatInt);
    }
    else
      sprintf( sTempA, "T   H   B          ");
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
  }
  else if (pMesNoiseSD->ModeAffMes == MODEMESAFFBR)
  {
    // Effacement de l'écran
    screen.clearDisplay();
    screen.setTextSize(1);
    screen.setTextColor(WHITE, BLACK);
    screen.setCursor(0,y);
    //              012345678901234567890
    // Ligne 1     "Bruit(dB) Min Moy Max"
    screen.print("Bruit(dB) Min Moy Max");
    y += 8;
    // Ligne 2     "     -100  -100  -100"
    sprintf( sTempA, "     %-03d %-03d %-03d", pMesNoiseSD->iNoiseMin/10, pMesNoiseSD->iNoiseMoy/10, pMesNoiseSD->iNoiseMax/10);
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
    // Ligne 3     "Max 120.00kHz 100Pics"
    dtostrf( pMesNoiseSD->fFreqMaxNoise, 5, 1, sTempB);
    if (pMesNoiseSD->iNbPicNoise > 1)
      sprintf( sTempA, "Max %skHz %dPics ", sTempB, pMesNoiseSD->iNbPicNoise);
    else
      sprintf( sTempA, "Max %skHz %dPic  ", sTempB, pMesNoiseSD->iNbPicNoise);
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
    // Ligne 4     "SDHC FAT32 31.4 GO"
    screen.setCursor(0,y);
    screen.print( pMesNoiseSD->sTypeSD);
    y += 8;
    // Ligne 5     "Test fichier OK"
    screen.setCursor(0,y);
    screen.print( pMesNoiseSD->sFileSD);
    y += 8;
    // Ligne 6 "RAM libre 16kO  "
    dtostrf( pMesNoiseSD->fFreeRam, 5, 2, sTempB);
    sprintf( sTempA, "RAM libre %s kO", sTempB);
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
    // Ligne 7      "T+15H80 B 12,5 3,7 V"
    if (strlen(pMesures->memoAnnexes.sBatInt) > 0)
    {
      int t = (int)pMesures->memoAnnexes.fTemp;
      int h = (int)pMesures->memoAnnexes.fHumid;
      sprintf( sTempA, "T %+3d %2d B%s %s V", t, h, pMesures->memoAnnexes.sBatExt, pMesures->memoAnnexes.sBatInt);
    }
    else
      sprintf( sTempA, "T   H   B          ");
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
  }
  // Ligne 8 " 12/06/16  22:12:28  " ou message d'erreur
  if (strlen(pMesures->Erreur) > 0)
  {
    if (strcmp(pMesures->Erreur, sErreur) != 0)
    {
      // Nouvelle erreur
      iMnErreur = rtcZero.getMinutes();
      strcpy( sErreur, pMesures->Erreur);
    }
    sprintf( sTempA, " %s", sErreur);
    if (iMnErreur != rtcZero.getMinutes())
    {
      // Effacement de l'erreur
      pMesures->Erreur[0] = 0;
      sErreur[0] = 0;
      iMnErreur = 0;
    }
  }
  else
    sprintf( sTempA, " %02d/%02d/%02d  %02d:%02d:%02d", rtcZero.getDay(), rtcZero.getMonth(), rtcZero.getYear(), rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  screen.setCursor(0,y);
  screen.print(sTempA);
  screen.display();
}

//----------------------------------------------------
// Affichage du logo de départ
void GestScreen::AffLogo()
{
  screen.clearDisplay();
  screen.setTextSize(1);
  screen.setTextColor(WHITE, BLACK);
  screen.drawBitmap(0, 0, LogoGR, 128, 64, 1);
  screen.setCursor(0,30);
  screen.println(VERSION);
  screen.display();
  //delay(10000);
}

//----------------------------------------------------
// Affichage de la page d'infos
void GestScreen::AffInfos(
  StructAllMesures *pMesures // Pointeur sur les mesures
  )
{
  char tstBandes[NBBANDES][3] = { "FM", "GR", "RE", "EP", "PR" };
  int y = 0;
  if (bActif)
  {
    // Effacement de l'écran
    screen.clearDisplay();
    screen.setTextSize(1);
    screen.setTextColor(WHITE, BLACK);
    screen.setCursor(0,y);
    // Ligne 1 "  S MN-120dB  H     J"
    #ifdef TESTECHMAX
      sprintf( sTempA, "  S MN%+04ddB  H%5dJ", (pMesures->memoM.iBruitMoyen/10), pMesures->iEchMax);
      pMesures->iEchMax = 0;
    #else
      sprintf( sTempA, "  S MN%+04ddB  H S%2d J", (pMesures->memoM.iBruitMoyen/10), pParams->sSeuilRelatif);
    #endif
    screen.println(sTempA);
    // Init du nom de la bande FM
    strcpy(tstBandes[BFM], pParams->sNomFM);
    y += 8;
    // Ligne 2 à 5 "FM* 60-120 3600 99999"
    for (int i=0; i<NBBANDES; i++)
    {
      char c = ' ';
      if (pMesures->saveSec.LstContacts[i])
        c = '*';
      sprintf( sTempA, "%s%c %2d%+04d %4d %5d", tstBandes[i], c, pMesures->memoM.LstSecPos[i], (pMesures->memoM.LstNivMax[i]/10), pMesures->memoH.LstMnPos[i], pMesures->memoJ.LstMnPos[i]);
      screen.setCursor(0,y);
      screen.print(sTempA);
      y += 8;
    }
    // Ligne 6      "T+15H80 B 12,5 3,7 V"
    if (strlen(pMesures->memoAnnexes.sBatInt) > 0)
    {
      int t = (int)pMesures->memoAnnexes.fTemp;
      int h = (int)pMesures->memoAnnexes.fHumid;
      sprintf( sTempA, "T %+3d %2d B%s %s V", t, h, pMesures->memoAnnexes.sBatExt, pMesures->memoAnnexes.sBatInt);
    }
    else
      sprintf( sTempA, "T   H   B          ");
    screen.setCursor(0,y);
    screen.print(sTempA);
    y += 8;
    // Ligne 7 " 12/06/16  22:12:28  " ou message d'erreur
    if (strlen(pMesures->Erreur) > 0)
    {
      if (strcmp(pMesures->Erreur, sErreur) != 0)
      {
        // Nouvelle erreur
        iMnErreur = rtcZero.getMinutes();
        strcpy( sErreur, pMesures->Erreur);
      }
      sprintf( sTempA, " %s", sErreur);
      if (iMnErreur != rtcZero.getMinutes())
      {
        // Effacement de l'erreur
        pMesures->Erreur[0] = 0;
        sErreur[0] = 0;
        iMnErreur = 0;
      }
    }
    else
      sprintf( sTempA, " %02d/%02d/%02d  %02d:%02d:%02d", rtcZero.getDay(), rtcZero.getMonth(), rtcZero.getYear(), rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
    screen.setCursor(0,y);
    screen.print(sTempA);
    screen.display();
  }
}

//----------------------------------------------------
// Affichage d'une ligne de paramètres
void GestScreen::AffLigneParam(
  int iNumLigne,      // Indice de la ligne
  char *psLigne,      // Pointeur sur la chaine de la ligne
  char *psChamps,     // Chaine avec des x symbolisant l'emplacement des modifications (X début d'un champ x, suite d'un même champ)
  MODIF_LIST idx,     // Indice du modificateur courant
  MODIF_TYPES ModType // Type du modificateur courant
  )
{
  int y = (iNumLigne-1) * 8;
  int x;
  screen.setTextSize(1);
  screen.setTextColor(WHITE, BLACK);
  if (iNumLigne == iLigneActive)
  {
    // La ligne est sélectionnée, si on est pas en modif
    if (!bModif)
    {
      // On mémorise la ligne
      strcpy( sLigneModif, psLigne);
      // On copie la chaine des emplacements des champs
      strcpy( sChamps, psChamps);
      // Et on sauvegarde le type et l'indice du modificateur
      ModifType = ModType;
      idxModif  = idx;
    }
    else
    {
      // On est en modif sur cette ligne, on copie la ligne sauvegardée et éventuellement modifiée par l'opérateur
      strcpy( psLigne, sLigneModif);
    }
    // Et on met une flêche en début de ligne
    psLigne[0] = 0x10;
  }
  else
    // On met un blanc en début de ligne
    psLigne[0] = ' ';
  if (bModif and iNumLigne == iModifY and iModifX > 0)
  {
    // La ligne est en modification, on affiche en inverse la zone en cours de modif
    // Affichage de la partie normale
    strncpy( sTempB, psLigne, iModifX);
    sTempB[iModifX] = 0;
    screen.setCursor(0,y);    
    screen.print(sTempB);
    // Affichage de la partie en inverse
    screen.setTextColor(BLACK, WHITE);
    int i = iModifX;
    int j = 0;
    while (psChamps[i] == 'X' or psChamps[i] == 'x')
    {
      sTempB[j] = psLigne[i];
      j++;
      i++;
      if (psChamps[i] == 'X')
        break;
    }
    sTempB[j] = 0;
    x = iModifX * 6;
    screen.setCursor(x,y);    
    screen.print(sTempB);
    // Affichage du reste de la chaîne si présent
    if (strlen(psLigne) > i)
    {
      screen.setTextColor(WHITE, BLACK);
      strcpy( sTempB, &psLigne[i]);
      x = i * 6;
      screen.setCursor(x,y);    
      screen.print(sTempB);
    }
  }
  else
  {
    // Affichage normal de la ligne
    screen.setCursor(0,y);    
    screen.print(psLigne);
  }
}

//----------------------------------------------------
// Affichage des paramètres page 1
void GestScreen::AffParamsP1()
{
  char cDeb = ' ';
  // Effacement de l'écran
  screen.clearDisplay();
  //                       012345678901234567890
  // Ligne 1                RhinoLogger V0.1<MES (retour vers les mesures)
  sprintf( sTempA,        " RhinoLogger %s<MES", VERSION);
  AffLigneParam( 1,sTempA,"                     ", IDX_RETOUR, MODIF_RET);
  //                       012345678901234567890
  // Ligne 2                Mise en veille Oui
  if (bVeille)
    sprintf(sTempA,       " Mise en veille OUI  ");
  else
    sprintf(sTempA,       " Mise en veille NON  ");
  AffLigneParam( 2,sTempA,"                Xxx  ", IDX_VEILLE, MODIF_VEILLE);
  //                       012345678901234567890
  // Ligne 3                Date  12/06/16      
  if (!bModif && iLigneActive != 3)
  {
    mJJ = rtcZero.getDay();
    mMM = rtcZero.getMonth();
    mYY = rtcZero.getYear();
  }
  //                       012345678901234567890
  sprintf(sTempA,         " Date  %02d/%02d/%02d      ", mJJ, mMM, mYY);
  AffLigneParam(3,sTempA, "       Xx Xx Xx      ", IDX_DATE, MODIF_DATE);
  //                       012345678901234567890
  // Ligne 4                Heure 22:12:28      
  if (!bModif && iLigneActive != 4)
  {
    mHH = rtcZero.getHours();
    mMN = rtcZero.getMinutes();
    mSS = rtcZero.getSeconds();
  }
  //                       012345678901234567890
  sprintf(sTempA,         " Heure %02d:%02d:%02d      ", mHH, mMN, mSS);
  AffLigneParam(4,sTempA, "       Xx Xx Xx      ", IDX_HEURE, MODIF_HEURE);
  //                       012345678901234567890
  // Ligne 5                Code Pin 1234       
  sprintf(sTempA,         " Code Pin %s       ", pParams->sCodePin);
  AffLigneParam(5,sTempA, "          XXXX       ", IDX_CODEPIN, MODIF_DIGITB);
  //                       012345678901234567890
  // Ligne 6                N° A 0633854439     
  sprintf(sTempA,         " N%c A %s     ", 9, pParams->sNumTel1);
  AffLigneParam(6,sTempA, "      XXXXXXXXXX     ", IDX_NUMA, MODIF_DIGITB);
  //                       012345678901234567890
  // Ligne 7                N° B
  sprintf(sTempA,         " N%c B %s     ", 9, pParams->sNumTel2);
  AffLigneParam(7,sTempA, "      XXXXXXXXXX     ", IDX_NUMB, MODIF_DIGITB);
  //                       012345678901234567890
  // Ligne 8                N° C
  sprintf(sTempA,         " N%c C %s     ", 9, pParams->sNumTel3);
  AffLigneParam(8,sTempA, "      XXXXXXXXXX     ", IDX_NUMC, MODIF_DIGITB);
  screen.display();
}

//----------------------------------------------------
// Affichage des paramètres page 2
void GestScreen::AffParamsP2(
    StructAllMesures   *pMesures // Pointeur sur les mesures
    )
{
  // Mémo du pointeur des mesures
  pmesures = pMesures;
  // Effacement de l'écran
  screen.clearDisplay();
  //                       012345678901234567890
  // Ligne 1                RhinoLogger V0.1<MES (retour vers les mesures)
  sprintf(sTempA,         " RhinoLogger %s<MES", VERSION);
  AffLigneParam(1,sTempA, "                     ", IDX_RETOUR, MODIF_RET);
  //                       012345678901234567890
  // Ligne 2                Test SMS
  if (bTestSMS)
    sprintf(sTempA,       " Test SMS En cours...");
  else if (mSMS)
    sprintf(sTempA,       " Test SMS OUI        ");
  else
    sprintf(sTempA,       " Test SMS NON        ");
  AffLigneParam(2,sTempA, "          Xxx        ", IDX_SMS, MODIF_SMS);
  //                       012345678901234567890
  // Ligne 3                Seuil Rel. 30 dB
  sprintf(sTempA,         " Seuil Rel. %02d dB    ", pParams->sSeuilRelatif);
  AffLigneParam(3,sTempA, "            XX      ", IDX_SEUIL, MODIF_DIGIT);
  //                       012345678901234567890
  // Ligne 4                Gain num. x01
  if (!bModif && iLigneActive != 4)
    iGain  = pParams->iGain;
  sprintf(sTempA,         " Gain num. %s       ", GetStrGain(iGain));
  AffLigneParam(4,sTempA, "           Xxx      ", IDX_GAIN, MODIF_GAIN);
  //                       012345678901234567890
  // Ligne 5                Coef. 12V 3,5 12.1V
  if (pParams->fCoefBExt < 0)
    pParams->fCoefBExt = 0;
  dtostrf( pParams->fCoefBExt, 3, 1, sTempB);
  sprintf(sTempA,         " Coef.  12V %s %sV ", sTempB, pMesures->memoAnnexes.sBatExt);
  AffLigneParam(5,sTempA, "            X X      ", IDX_COEFEXT, MODIF_DIGIT);
  //                       012345678901234567890
  // Ligne 6                Coef. LiPo 2,1
  if (pParams->fCoefBInt < 0)
    pParams->fCoefBInt = 0;
  dtostrf( pParams->fCoefBInt, 3, 1, sTempB);
  sprintf(sTempA,         " Coef. LiPo %s %sV ", sTempB, pMesures->memoAnnexes.sBatInt);
  AffLigneParam(6,sTempA, "            X X      ", IDX_COEFINT, MODIF_DIGIT);
  //                       012345678901234567890
  // Ligne 7                Nom Rhinologger1
  sprintf(sTempA,         " Nom %s    ", pParams->sNomStation);
  AffLigneParam(7,sTempA, "     XXXXXXXXXXXX    ", IDX_NOM, MODIF_CHAR);
  //                       012345678901234567890
  // Ligne 8                Signal test Sans
  if (!bModif && iLigneActive != 8)
    mSignalTest = pParams->iSignalTest;
  strcpy(sTempA, GetStrTest(mSignalTest));
  //                       012345678901234567890
  AffLigneParam(8,sTempA, "      Xxxxxxxxxxxxxxx", IDX_TEST, MODIF_TEST);
  screen.display();
}

//----------------------------------------------------
// Affichage des paramètres page 3
void GestScreen::AffParamsP3()
{
  // Effacement de l'écran
  screen.clearDisplay();
  //                       012345678901234567890
  // Ligne 1                RhinoLogger V0.1<MES (retour vers les mesures)
  sprintf(sTempA,         " RhinoLogger %s<MES", VERSION);
  AffLigneParam(1,sTempA, "                     ", IDX_RETOUR, MODIF_RET);
  //                       012345678901234567890
  // Ligne 2                FM040.0 060.0 S02 M0
  dtostrf( pParams->fFMinB[BFM], 5, 1, sTempB);
  if (sTempB[0] == ' ')
    sTempB[0] = '0';
  if (sTempB[1] == ' ')
    sTempB[1] = '0';
  dtostrf( pParams->fFMaxB[BFM], 5, 1, sTempC);
  if (sTempC[0] == ' ')
    sTempC[0] = '0';
  if (sTempC[1] == ' ')
    sTempC[1] = '0';
  sprintf(sTempA,    PSTR(" %s%s %s S%02d M%1d"), pParams->sNomFM, sTempB, sTempC, pParams->sSeuilFC[BFM], pParams->bSMSB[BFM]);
  AffLigneParam(2,sTempA, " XXXXX X XXX X  XX  X", IDX_FRFM, MODIF_BANDE);
  //                       012345678901234567890
  // Ligne 3                GR076.0 086.0 S10 M0
  dtostrf( pParams->fFMinB[BGR], 5, 1, sTempB);
  if (sTempB[0] == ' ')
    sTempB[0] = '0';
  if (sTempB[1] == ' ')
    sTempB[1] = '0';
  dtostrf( pParams->fFMaxB[BGR], 5, 1, sTempC);
  if (sTempC[0] == ' ')
    sTempC[0] = '0';
  if (sTempC[1] == ' ')
    sTempC[1] = '0';
  sprintf(sTempA,    PSTR(" GR%s %s S%02d M%1d"), sTempB, sTempC, pParams->sSeuilFC[BGR], pParams->bSMSB[BGR]);
  AffLigneParam(3,sTempA, "   XXX X XXX X  XX  X", IDX_FRGR, MODIF_BANDE);
  //                       012345678901234567890
  // Ligne 4                RE100.0 104.0 S08 M0
  dtostrf( pParams->fFMinB[BRE], 5, 1, sTempB);
  if (sTempB[0] == ' ')
    sTempB[0] = '0';
  if (sTempB[1] == ' ')
    sTempB[1] = '0';
  dtostrf( pParams->fFMaxB[BRE], 5, 1, sTempC);
  if (sTempC[0] == ' ')
    sTempC[0] = '0';
  if (sTempC[1] == ' ')
    sTempC[1] = '0';
  sprintf(sTempA   , PSTR(" RE%s %s S%02d M%1d"), sTempB, sTempC, pParams->sSeuilFC[BRE], pParams->bSMSB[BRE]);
  AffLigneParam(4,sTempA, "   XXX X XXX X  XX  X", IDX_FRRE, MODIF_BANDE);
  //                       012345678901234567890
  // Ligne 5                EP104.1 106.4 S08 M0
  dtostrf( pParams->fFMinB[BEP], 5, 1, sTempB);
  if (sTempB[0] == ' ')
    sTempB[0] = '0';
  if (sTempB[1] == ' ')
    sTempB[1] = '0';
  dtostrf( pParams->fFMaxB[BEP], 5, 1, sTempC);
  if (sTempC[0] == ' ')
    sTempC[0] = '0';
  if (sTempC[1] == ' ')
    sTempC[1] = '0';
  sprintf(sTempA,    PSTR(" EP%s %s S%02d M%1d"), sTempB, sTempC, pParams->sSeuilFC[BEP], pParams->bSMSB[BEP]);
  AffLigneParam(5,sTempA, "   XXX X XXX X  XX  X", IDX_FREP, MODIF_BANDE);
  //                       012345678901234567890
  // Ligne 6                PR106.5 120.0 S08 M0
  //                       012345678901234567890
  dtostrf( pParams->fFMinB[BPR], 5, 1, sTempB);
  if (sTempB[0] == ' ')
    sTempB[0] = '0';
  if (sTempB[1] == ' ')
    sTempB[1] = '0';
  dtostrf( pParams->fFMaxB[BPR], 5, 1, sTempC);
  if (sTempC[0] == ' ')
    sTempC[0] = '0';
  if (sTempC[1] == ' ')
    sTempC[1] = '0';
  sprintf(sTempA,    PSTR(" PR%s %s S%02d M%1d"), sTempB, sTempC, pParams->sSeuilFC[BPR], pParams->bSMSB[BPR]);
  AffLigneParam(6,sTempA, "   XXX X XXX X  XX  X", IDX_FRPR, MODIF_BANDE);
  // Ligne 7               RAZ datas oui
  //                       012345678901234567890
  if (bIsRAZ)
    sprintf(sTempA,       " RAZ Datas/Files OUI ");
  else
    sprintf(sTempA,       " RAZ Datas/Files NON ");
  AffLigneParam(7,sTempA, "                 Xxx ", IDX_RAZ, MODIF_RAZ);
  // Ligne 8               Parm. par defaut OUI
  //                       012345678901234567890
  if (bParamDef)
    sprintf(sTempA,       " Param par defaut OUI");
  else
    sprintf(sTempA,       " Param par defaut NON");
  AffLigneParam(8,sTempA, "                  Xxx", IDX_PARAMD, MODIF_PARAMD);
  screen.display();
}

//----------------------------------------------------
// Affichage des paramètres page 4
void GestScreen::AffParamsP4()
{
  // Effacement de l'écran
  screen.clearDisplay();
  //                       012345678901234567890
  // Ligne 1                RhinoLogger V0.1<MES (retour vers les mesures)
  sprintf(sTempA,         " RhinoLogger %s<MES", VERSION);
  AffLigneParam(1,sTempA, "                     ", IDX_RETOUR, MODIF_RET);
  //                       012345678901234567890
  sprintf(sTempA,         " APN                 ");
  // Ligne 2                APN Xxxxxxxxxxxxxxxx
  AffLigneParam(1,sTempA, "     Xxxxxxxxxxxxxxxx", IDX_APN, MODIF_CHAR);
  //                       012345678901234567890
  // Ligne 3                Serveur SMTP
  sprintf(sTempA,         " Serveur SMTP        ");
  AffLigneParam(1,sTempA, "                     ", IDX_VIDE, NO_MODIF);
  //                       012345678901234567890
  // Ligne 4                Xxxxxxxxxxxxxxxxxxxx
  sprintf(sTempA,         "                     ");
  AffLigneParam(1,sTempA, " Xxxxxxxxxxxxxxxxxxxx", IDX_SMTP, MODIF_CHAR);
  //                       012345678901234567890
  // Ligne 5                Port Xxx
  sprintf(sTempA,         " Port Xxx            ");
  AffLigneParam(1,sTempA, "      Xxx            ", IDX_PORT, MODIF_DIGITB);
  //                       012345678901234567890
  // Ligne 6                Compte / Adresse
  sprintf(sTempA,         " Compte / Adresse    ");
  AffLigneParam(1,sTempA, "                     ", IDX_VIDE, NO_MODIF);
  //                       012345678901234567890
  // Ligne 7                Xxxxxxxxxxxxxxxxxxxx
  sprintf(sTempA,         "                     ");
  AffLigneParam(1,sTempA, " Xxxxxxxxxxxxxxxxxxxx", IDX_COMPTE, MODIF_CHAR);
  //                       012345678901234567890
  // Ligne 8                @Xxxxxxxxxxxxxxxxxxx
  sprintf(sTempA,         " @                   ");
  AffLigneParam(1,sTempA, "  Xxxxxxxxxxxxxxxxxxx", IDX_ADRESSE, MODIF_CHAR);
}

//----------------------------------------------------
// Affichage du mode attente Reset
void GestScreen::AffAttenteReset()
{
  // Effacement de l'écran
  screen.clearDisplay();
  // Ligne 1 "Attente Reset..."
  screen.setCursor(0,0);
  screen.print("Attente Reset...");
  // Ligne 2 " a 12h30..."
  screen.setCursor(0,8);
  screen.print(" a 12h30...");
  // Ligne 8 " 12/06/16  22:12:28  "
  sprintf( sTempA, " %02d/%02d/%02d  %02d:%02d:%02d", rtcZero.getDay(), rtcZero.getMonth(), rtcZero.getYear(), rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  screen.setCursor(0,56);
  screen.print(sTempA);
  screen.display();
}

//----------------------------------------------------
// Affichage du mode SMS
void GestScreen::AffSMS(
  bool bTest,           // True pour indiquer un test SMS, false pour un SMS journalier
  GSMManager *pGestSMS  // Pointeur sur le gestionnaire de GSM
  )
{
  // Effacement de l'écran
  screen.clearDisplay();
  // Ligne 1 "Envoie SMS..."
  screen.setCursor(0,0);
  if (bTest)
    screen.print("Test SMS...");
  else
    screen.print("Envoie SMS...");
  // Ligne 2 "GSM SIM800 OK"
  screen.setCursor(0,8);
  if (pGestSMS->IsStarted())
    screen.print("GSM SIM800 OK");
  else
    screen.print("GSM SIM800 ?");
  // Ligne 3 "Code PIN OK"
  screen.setCursor(0,16);
  if (pGestSMS->IsPinOK())
    screen.print("Code PIN OK");
  else
    screen.print("Code PIN ?");
  //          012345678901234567890
  // Ligne 4 "Orange_F  [|||-----]"
  screen.setCursor(0,24);
  screen.print(pGestSMS->GetOperatorName());
  screen.setCursor(60,24);
  screen.print("[");
  char c[2];
  c[1] = 0;
  for (int i=0; i<8; i++)
  {
    screen.setCursor(66+(i*6),24);
    if (pGestSMS->GetSignalQuality() == 0)
      c[0] = '-';
    else if (pGestSMS->GetSignalQuality() < i)
      c[0] = '-';
    else
      c[0] = 0xdc;
    screen.print(c);
  }
  screen.setCursor(66+(8*6),24);
  screen.print("]");
  // Ligne 5 "Envoie SMS en cours"
  screen.setCursor(0,32);
  if (pGestSMS->GetSequence() < GSMManager::STARTGSMOK)
    screen.print("Attente GSM...");
  else if (pGestSMS->GetSequence() < GSMManager::ATTENTERESEAU)
    screen.print("Attente reseau...");
  else if (pGestSMS->GetSequence() == GSMManager::ERRORGSM)
    screen.print("Erreur GSM !");
  else if (pGestSMS->IsSMSOK())
    screen.print("Envoie SMS OK");
  else if (pGestSMS->GetSequence() >= GSMManager::STARTGSMOK)
    screen.print("Envoie SMS en cours");
  
  // Ligne 8 " 12/06/16  22:12:28  "
  sprintf( sTempA, " %02d/%02d/%02d  %02d:%02d:%02d", rtcZero.getDay(), rtcZero.getMonth(), rtcZero.getYear(), rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  screen.setCursor(0,56);
  screen.print(sTempA);
  screen.display();
}

//----------------------------------------------------
// Affichage du mode veille
void GestScreen::AffVeille()
{
  // Effacement de l'écran
  screen.clearDisplay();
  // Ligne 1 "Mode veille..."
  screen.setCursor(0,0);
  screen.print("Mode veille...");
  screen.display();
}

//----------------------------------------------------
// Affichage en mode debug des 7 niveaux max
void GestScreen::AffDebugNivMax(
  StructDebug *pMesDebug, // Infos de debug à afficher
  short iBruitMoyen       // Bruit moyen
  )
{
  int y = 0;
  // Effacement de l'écran
  screen.clearDisplay();
  screen.setTextSize(1);
  screen.setTextColor(WHITE, BLACK);
  screen.setCursor(0,y);
  //                       012345678901234567890
  // Ligne 1               Idx Freq Bruit -120dB
  sprintf(sTempA,         "Idx Freq Bruit %04ddB", iBruitMoyen/10);
  screen.println(sTempA);
  y += 8;
  //                       012345678901234567890
  // Ligne 2 à 8           052  75,12k 860 -80.3
  for (int i=0; i<7; i++)
  {
    if (pMesDebug[i].iLevelMax > -250)
    {
      dtostrf( pMesDebug[i].fFreqMax, 3, 2, sTempB);
      dtostrf( (((float)pMesDebug[i].idBMax)/10.0)  , 2, 1, sTempC);
      sprintf(sTempA,     "%03d %s %03d %s",  pMesDebug[i].idxMax, sTempB,  pMesDebug[i].iLevelMax,  sTempC);
    }
    else
      sprintf(sTempA,     "                     ");
    screen.println(sTempA);
    y += 8;
  }
  screen.display();
}

//----------------------------------------------------
// Affichage en mode debug des fréquences par lot de 7
void GestScreen::AffDebugFreq(
  StructDebFr *pMesDebug, // Infos de debug à afficher
  short iBruitMoyen       // Bruit moyen
  )
{
  int idB, iNoise, iMax;
  int y = 0;
  // Effacement de l'écran
  screen.clearDisplay();
  screen.setTextSize(1);
  screen.setTextColor(WHITE, BLACK);
  screen.setCursor(0,y);
  //                       012345678901234567890
  // Ligne 1               Freq Niv Bruit -120dB
  sprintf(sTempA,         "Freq Niv Bruit %04ddB", iBruitMoyen/10);
  screen.println(sTempA);
  y += 8;
  //                       012345678901234567890
  // Ligne 2 à 8            40,5k -120 Max-088dB
  for (int i=0; i<7; i++)
  {
    dtostrf( pMesDebug[i].fFreqMax, 3, 1, sTempB);
    idB    = pMesDebug[i].idB / 10;
    iMax   = pMesDebug[i].idBMax / 10;
    iNoise = pMesDebug[i].iNoise / 10;
    sprintf(sTempA,       "%s %04d %04d %04d", sTempB, idB, iNoise, iMax);
    screen.println(sTempA);
    y += 8;
  }
  screen.display();
}

//----------------------------------------------------
// Indique si l'affichage est actif
bool GestScreen::IsActif()
{
  return bActif;
}

//----------------------------------------------------
// Force l'affichage
void GestScreen::StartDisplay()
{
  bActif = true;
}

//----------------------------------------------------
// Stoppe l'affichage
void GestScreen::StopDisplay()
{
    // Effacement de l'écran
    screen.clearDisplay();
    screen.display();
    bActif = false;
}

//----------------------------------------------------
// Stoppe le mode Test SMS
void GestScreen::StopTestSMS()
{
  bTestSMS = false;
  mSMS = 0;
}

//----------------------------------------------------
// Passe en modification d'un paramètre
// Retourne true si on passe en modif
bool GestScreen::SetModifOn()
{
  bModif = true;
  iModifY = iLigneActive;
  // On recherche la 1ère zone à modifier (X)
  int i = 0;
  while (sChamps[i] != 'X')
  {
    i++;
    if (i >= strlen(sChamps))
    {
      // Pas de champ à modifier sur cette ligne, on ne passe pas en modif
      i = 0;
      bModif = false;
      break;
    }
  }
  iModifX = i;
  if (bModif)
    iChamp = 0;

  return bModif;
}

//----------------------------------------------------
// Valide la modification d'un paramètre
void GestScreen::ValidModif()
{
  short iSeuilR, iSeuilQFC;
  float fMin, fMax;
  bModifCoeff = false;
  switch (idxModif)
  {
  case IDX_RETOUR  :  // Retour vers les mesures
    // On ne fait rien, le mécanisme de retour est géré par SetModifOn
    break;
  case IDX_DATE    :  // Modificateur de la date
    // Mise à la date de l'horloge sauvegardée et de l'horloge processeur
    setDS3231time( rtcZero.getSeconds(), rtcZero.getMinutes(), rtcZero.getHours(), mJJ, mMM, mYY);
    rtcZero.setDate( mJJ, mMM, mYY);
    break;
  case IDX_HEURE   :  // Modificateur de l'heure
    // Mise à la date de l'horloge sauvegardée et de l'horloge processeur
    setDS3231time( mSS, mMN, mHH, rtcZero.getDay(), rtcZero.getMonth(), rtcZero.getYear());
    rtcZero.setTime( mHH, mMN, mSS);
    break;
  case IDX_NOM     :  // Modificateur du nom de station
    strncpy( pParams->sNomStation, &(sLigneModif[5]), 12);
    break;
  case IDX_CODEPIN :  // Modificateur du code pin
    strncpy( pParams->sCodePin, &(sLigneModif[10]), 4);
    break;
  case IDX_NUMA    :  // Modificateur du n° de téléphone A
    strncpy( pParams->sNumTel1, &(sLigneModif[6]), 10);
    break;
  case IDX_NUMB    :  // Modificateur du n° de téléphone B
    strncpy( pParams->sNumTel2, &(sLigneModif[6]), 10);
    break;
  case IDX_NUMC    :  // Modificateur du n° de téléphone C
    strncpy( pParams->sNumTel3, &(sLigneModif[6]), 10);
    break;
  case IDX_SMS     :  // Modificateur du test SMS
    // Si bTestSMS == true, On passe en mode test SMS
    bTestSMS = mSMS;
    break;
  case IDX_VEILLE  :  // Modificateur du mode veille
    // Si bModeVeille == true, On passe en mode veille
    bModeVeille = bVeille;
    break;
  case IDX_SEUIL   :  // Modificateur du seuil relatif
    //                       012345678901234567890
    // Ligne 3                Seuil Rel. 30 dB
    iSeuilR   = ((sLigneModif[12]-'0')*10) + (sLigneModif[13]-'0');
    pParams->sSeuilRelatif = iSeuilR;
    break;
  case IDX_GAIN    :  // Modificateur du gain numérique
    //                       012345678901234567890
    // Ligne 4                Gain num. x01
    switch (sLigneModif[13])
    {
    case '0':
      pParams->iGain = GAIN_1;
      break;
    case '2':
      pParams->iGain = GAIN_2;
      break;
    case '4':
      pParams->iGain = GAIN_4;
      break;
    case '8':
    default:
      pParams->iGain = GAIN_8;
      break;
    case '6':
      pParams->iGain = GAIN_16;
      break;
    }
    break;
  case IDX_COEFEXT :  // Modificateur du coefficient de mesure de la batterie externe
    pParams->fCoefBExt = (float)(sLigneModif[12]-'0') + ((float)(sLigneModif[14]-'0') / 10.0);
    bModifCoeff = true;
    break;
  case IDX_COEFINT :  // Modificateur du coefficient de mesure de la batterie interne
    pParams->fCoefBInt = (float)(sLigneModif[12]-'0') + ((float)(sLigneModif[14]-'0') / 10.0);
    bModifCoeff = true;
    break;
  case IDX_TEST    :  // Modificateur du signal de test
    switch (sLigneModif[6])
    {
    //  012345678901234567890 N
    // " Test Normal Aff Int ");
    // " Test normal Aff Perm");
    // " Test Acquisition    ");
    // " Test Cycle mesures  ");
    // " Test FFT            ");
    // " Test 1 Mesure       ");
    // " Test Max Niveau     ");
    // " Test a Bande FM     ");
    // " Test b Bande GR 1   ");
    // " Test c Bande GR 2   ");
    // " Test d Bande RE     ");
    // " Test Loop           ");
    // " Test Record WAV     ");
    case 'N':
      pParams->iSignalTest = NORMAL;
      break;
    case 'n':
      pParams->iSignalTest = NORMAL_AFF;
      break;
    case 'A':
      pParams->iSignalTest = TESTACQ;
      break;
    case 'C':
       pParams->iSignalTest = TESTCycle;
      break;
    case 'F':
      pParams->iSignalTest = TESTFFT;
      break;
    case '2':
      pParams->iSignalTest = TEST200ms;
      break;
    case '1':
      pParams->iSignalTest = TESTMes;
      break;
    case 'M':
      pParams->iSignalTest = TESTAffMax;
      break;
    case 'a':
      pParams->iSignalTest = TESTAffFM;
      break;
    case 'b':
      pParams->iSignalTest = TESTAffGR1;
      break;
    case 'c':
      pParams->iSignalTest = TESTAffGR2;
      break;
    case 'd':
      pParams->iSignalTest = TESTAffRE;
      break;
    case 'L':
      pParams->iSignalTest = TESTLoop;
      break;
    }
    break;
  case IDX_FRFM:
    //         012345678901234567890
    // Ligne 2  FM040.0 060.0 S02 M0
    pParams->sNomFM[0] = sLigneModif[1];
    pParams->sNomFM[1] = sLigneModif[2];
    pParams->sNomFM[2] = 0;
    fMin = ((float)(sLigneModif[3]-'0') * 100) + ((float)(sLigneModif[4]-'0') * 10) + ((float)(sLigneModif[5]-'0')) + ((float)(sLigneModif[7]-'0') / 10);
    if (fMin > 1.0 and fMin < 125.0)
      pParams->fFMinB[BFM] = fMin;
    fMax = ((float)(sLigneModif[9]-'0') * 100) + ((float)(sLigneModif[10]-'0') * 10) + ((float)(sLigneModif[11]-'0')) + ((float)(sLigneModif[13]-'0') / 10);
    if (fMax > pParams->fFMinB[BFM] and fMax < 125.0)
      pParams->fFMaxB[BFM] = fMax;
    pParams->sSeuilFC[BFM] = ((sLigneModif[16]-'0') * 10) + (sLigneModif[17]-'0');
    if (sLigneModif[20] != '0')
      pParams->bSMSB[BFM] = true;
    else
      pParams->bSMSB[BFM] = false;
    break;
  case IDX_FRGR:
    //         012345678901234567890
    // Ligne 3  GR076.0 086.0 S10 M0
    fMin = ((float)(sLigneModif[3]-'0') * 100) + ((float)(sLigneModif[4]-'0') * 10) + ((float)(sLigneModif[5]-'0')) + ((float)(sLigneModif[7]-'0') / 10);
    if (fMin > 1 and fMin < 125.0)
      pParams->fFMinB[BGR] = fMin;
    fMax = ((float)(sLigneModif[9]-'0') * 100) + ((float)(sLigneModif[10]-'0') * 10) + ((float)(sLigneModif[11]-'0')) + ((float)(sLigneModif[13]-'0') / 10);
    if (fMax > pParams->fFMinB[BGR] and fMax < 125.0)
      pParams->fFMaxB[BGR] = fMax;
    pParams->sSeuilFC[BGR] = ((sLigneModif[16]-'0') * 10) + (sLigneModif[17]-'0');
    if (sLigneModif[20] != '0')
      pParams->bSMSB[BGR] = true;
    else
      pParams->bSMSB[BGR] = false;
    break;
  case IDX_FRRE:
    //         012345678901234567890
    // Ligne 4  RE100.0 103.9 S08 M0
    fMin = ((float)(sLigneModif[3]-'0') * 100) + ((float)(sLigneModif[4]-'0') * 10) + ((float)(sLigneModif[5]-'0')) + ((float)(sLigneModif[7]-'0') / 10);
    if (fMin > 1 and fMin < 125.0)
      pParams->fFMinB[BRE] = fMin;
    fMax = ((float)(sLigneModif[9]-'0') * 100) + ((float)(sLigneModif[10]-'0') * 10) + ((float)(sLigneModif[11]-'0')) + ((float)(sLigneModif[13]-'0') / 10);
    if (fMax > pParams->fFMinB[BRE] and fMax < 125.0)
      pParams->fFMaxB[BRE] = fMax;
    pParams->sSeuilFC[BRE] = ((sLigneModif[16]-'0') * 10) + (sLigneModif[17]-'0');
    if (sLigneModif[20] != '0')
      pParams->bSMSB[BRE] = true;
    else
      pParams->bSMSB[BRE] = false;
    break;
  case IDX_FREP:
    //         012345678901234567890
    // Ligne 5  EP104.0 106.5 S08 M0
    fMin = ((float)(sLigneModif[3]-'0') * 100) + ((float)(sLigneModif[4]-'0') * 10) + ((float)(sLigneModif[5]-'0')) + ((float)(sLigneModif[7]-'0') / 10);
    if (fMin > 1 and fMin < 125.0)
      pParams->fFMinB[BEP] = fMin;
    fMax = ((float)(sLigneModif[9]-'0') * 100) + ((float)(sLigneModif[10]-'0') * 10) + ((float)(sLigneModif[11]-'0')) + ((float)(sLigneModif[13]-'0') / 10);
    if (fMax > pParams->fFMinB[BEP] and fMax < 125.0)
      pParams->fFMaxB[BEP] = fMax;
    pParams->sSeuilFC[BEP] = ((sLigneModif[16]-'0') * 10) + (sLigneModif[17]-'0');
    if (sLigneModif[20] != '0')
      pParams->bSMSB[BEP] = true;
    else
      pParams->bSMSB[BEP] = false;
    break;
  case IDX_FRPR:
    //         012345678901234567890
    // Ligne 6  PR106.6 120.0 S08 M0
    fMin = ((float)(sLigneModif[3]-'0') * 100) + ((float)(sLigneModif[4]-'0') * 10) + ((float)(sLigneModif[5]-'0')) + ((float)(sLigneModif[7]-'0') / 10);
    if (fMin > 1 and fMin < 125.0)
      pParams->fFMinB[BPR] = fMin;
    fMax = ((float)(sLigneModif[9]-'0') * 100) + ((float)(sLigneModif[10]-'0') * 10) + ((float)(sLigneModif[11]-'0')) + ((float)(sLigneModif[13]-'0') / 10);
    if (fMax > pParams->fFMinB[BPR] and fMax < 125.0)
      pParams->fFMaxB[BPR] = fMax;
    pParams->sSeuilFC[BPR] = ((sLigneModif[16]-'0') * 10) + (sLigneModif[17]-'0');
    if (sLigneModif[20] != '0')
      pParams->bSMSB[BPR] = true;
    else
      pParams->bSMSB[BPR] = false;
    break;
  case IDX_RAZ:
    // On ne fait rien, c'est à la fermeture du mode paramètre que les actions seront effectuées
    break;
  case IDX_PARAMD:
    // Remise des paramètres aux valeurs par défaut
    if (bParamDef)
    {
      bParamDef = false;
      InitParamsDefaut();
    }
    break;
  }
  bModif = false;
}

//----------------------------------------------------
// On passe au paramètre suivant
// Retourne true si on doit passer à la page suivante
bool GestScreen::ScrollDown()
{
  bool bReturn = false;
  iLigneActive++;
  if (iLigneActive == 9)
  {
    iLigneActive = 1;
    bReturn = true;
  }
  return bReturn;
}

//----------------------------------------------------
// On passe au paramètre précédent
// Retourne true si on doit passer à la page précédente
bool GestScreen::ScrollUp()
{
  bool bReturn = false;
  if (iLigneActive > 0)
    iLigneActive--;
  if (iLigneActive == 0)
  {
    iLigneActive = 8;
    bReturn = true;
  }
  return bReturn;
}

//----------------------------------------------------
// On passe au caractère suivant
void GestScreen::ScrollRight()
{
  // Champ suivant
  iChamp++;
  // On recherche la zone suivante à modifier (X)
  int i = iModifX + 1;
  while (sChamps[i] != 'X')
  {
    i++;
    if (i >= strlen(sChamps))
    {
      i = 0;
      iChamp = 0;
    }
    else if (i == iModifX)
      break;
  }
  iModifX = i;
}

//----------------------------------------------------
//On passe au caractère précédent
void GestScreen::ScrollLeft()
{
  // Champ précédent
  iChamp--;
  // On recherche la zone précédente à modifier (X)
  int i = iModifX - 1;
  while (sChamps[i] != 'X')
  {
    i--;
    if (i <= 1)
    {
      i = strlen(sChamps) - 1;
      iChamp = 2;
    }
    else if (i == iModifX)
      break;
  }
  iModifX = i;
}

//----------------------------------------------------
// On décrémente le paramètre en modification
void GestScreen::ModifDown()
{
  char c;
  switch (ModifType)
  {
  case MODIF_DATE :   // Modificateur de type date
    switch (iChamp)
    {
    case 0:
      // Modif du jour
      mJJ--;
      if (mJJ < 1) mJJ = 31;
      break;
    case 1:
      // Modif du mois
      mMM--;
      if (mMM < 1) mMM = 12;
      break;
    case 2:
      // Modif de l'année
      mYY--;
      if (mYY < 16) mYY = 99;
      break;
    }
    // Reconstitution de la chaine
    sprintf( sLigneModif, " Date  %02d/%02d/%02d      ", mJJ, mMM, mYY);
    break;
  case MODIF_HEURE:  // Modificateur de type heure
    switch (iChamp)
    {
    case 0:
      // Modif de l'heure
      mHH--;
      if (mHH < 0) mHH = 23;
      break;
    case 1:
      // Modif des minutes
      mMN--;
      if (mMN < 0) mMN = 59;
      break;
    case 2:
      // Modif des secondes
      mSS--;
      if (mSS < 0) mSS = 59;
      break;
    }
    // Reconstitution de la chaine
    sprintf( sLigneModif, " Heure %02d:%02d:%02d      ", mHH, mMN, mSS);
    break;
  case MODIF_CHAR :  // Modificateur pour plusieurs caractères de 'espace' puis 0 à 9 puis A à Z puis a à z
    c = sLigneModif[iModifX];
    if      (c > 'z')              c = 'z';
    else if (c <= 'a' and c > 'Z') c = 'Z';
    else if (c <= 'A' and c > '9') c = '9';
    else if (c <= '0')             c = ' ';
    else if (c <= ' ')             c = 'z';
    else                           c--;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_DIGIT:  // Modificateur pour plusieurs caractères numériques de 0 à 9
    c = sLigneModif[iModifX];
    if      (c < '0' or c > '9')
      c = '9';
    else if (c == '0') c = '9';
    else               c--;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_DIGITB:  // Modificateur pour plusieurs caractères numériques de 0 à 9 plus blanc
    c = sLigneModif[iModifX];
    if (c != ' ' and c <= '0') c = ' ';
    else if (c <= ' ')         c = '9';
    else                       c--;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_BANDE: // Modificateur d'une bande
    c = sLigneModif[iModifX];
    if (iModifX <= 2)
    {
      // Modif du nom de la bande A-Z-a-z
      if      (c <= 'A')             c = 'z';
      else if (c <= 'a' and c > 'Z') c = 'Z';
      else if (c >  'z')             c = 'A';
      else c--;
    }
    else if (iModifX == 20)
    {
      // Modification présence ou non SMS (0 ou 1)
      if (c <= '0')
        c = '1';
      else
        c = '0';
    }
    else
    {
      // Modif des fréquences ou seuil FC de la bande
      if      (c < '0' or c > '9')
        c = '9';
      else if (c == '0') c = '9';
      else               c--;
    }
    sLigneModif[iModifX] = c;
    break;
  case MODIF_SMS :  // Modificateur pour le test SMS
    if (mSMS == 1)
    {
      mSMS = 0;
      sprintf( sLigneModif, " Test SMS NON");
    }
    else
    {
      mSMS = 1;
      sprintf( sLigneModif, " Test SMS OUI");
    }
    break;
  case MODIF_VEILLE :  // Modificateur pour le mode veille
    if (bVeille == 1)
    {
      bVeille = false;
      sprintf( sLigneModif, " Mise en veille NON  ");
    }
    else
    {
      bVeille = true;
      sprintf( sLigneModif, " Mise en veille OUI  ");
    }
    break;
  case MODIF_GAIN :  // Modificateur du gain numérique
    if (iGain == GAIN_1)
      iGain = GAIN_16;
    else
    {
      int iG = iGain;
      iG--;
      iGain = static_cast<GAINUM>(iG);
    }
    sprintf( sLigneModif, " Gain num. %s       ", GetStrGain(iGain));
    break;
  case MODIF_TEST:  // Modificateur du mode du signal de test
    if (mSignalTest == NORMAL)
      mSignalTest = TESTLoop;
    else
    {
      int iTest = mSignalTest;
      iTest--;
      mSignalTest = static_cast<SIGNAL_TEST>(iTest);
    }
    strcpy( sLigneModif, GetStrTest(mSignalTest));
    break;
  case MODIF_RAZ :  // Modificateur RAZ des données
    if (bIsRAZ)
    {
      bIsRAZ = false;
      sprintf( sLigneModif, " RAZ Datas/Files NON ");
    }
    else
    {
      bIsRAZ = true;
      sprintf( sLigneModif, " RAZ Datas/Files OUI ");
    }
    break;
  case MODIF_PARAMD :  // Modificateur de remise aux paramètres par défaut
    if (bParamDef)
    {
      bParamDef = false;
      sprintf( sLigneModif, " Param par defaut NON");
    }
    else
    {
      bParamDef = true;
      sprintf( sLigneModif, " Param par defaut OUI");
    }
    break;
  }
}

//----------------------------------------------------
// On incrémente le paramètre en modification
void GestScreen::ModifUp()
{
  char c;
  switch (ModifType)
  {
  case MODIF_DATE :   // Modificateur de type date
    switch (iChamp)
    {
    case 0:
      // Modif du jour
      mJJ++;
      if (mJJ > 31) mJJ = 1;
      break;
    case 1:
      // Modif du mois
      mMM++;
      if (mMM > 12) mMM = 1;
      break;
    case 2:
      // Modif de l'année
      mYY++;
      if (mYY > 99) mYY = 0;
      break;
    }
    // Reconstitution de la chaine
    sprintf( sLigneModif, " Date  %02d/%02d/%02d      ", mJJ, mMM, mYY);
    break;
  case MODIF_HEURE:  // Modificateur de type heure
    switch (iChamp)
    {
    case 0:
      // Modif de l'heure
      mHH++;
      if (mHH > 23) mHH = 0;
      break;
    case 1:
      // Modif des minutes
      mMN++;
      if (mMN > 59) mMN = 0;
      break;
    case 2:
      // Modif des secondes
      mSS++;
      if (mSS > 59) mSS = 0;
      break;
    }
    // Reconstitution de la chaine
    sprintf( sLigneModif, " Heure %02d:%02d:%02d      ", mHH, mMN, mSS);
    break;
  case MODIF_CHAR :  // Modificateur pour plusieurs caractères de 'espace' puis 0 à 9 puis A à Z puis a à z
    c = sLigneModif[iModifX];
    if      (c < ' ')              c = ' ';
    else if (c >= ' ' and c < '0') c = '0';
    else if (c >= '9' and c < 'A') c = 'A';
    else if (c >= 'Z' and c < 'a') c = 'a';
    else if (c >= 'z')             c = ' ';
    else c++;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_DIGIT:  // Modificateur pour plusieurs caractères numériques de 0 à 9
    c = sLigneModif[iModifX];
    if      (c < '0' or c > '9')
      c = '0';
    else if (c == '9') c = '0';
    else               c++;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_DIGITB:  // Modificateur pour plusieurs caractères numériques de 0 à 9 plus blanc
    c = sLigneModif[iModifX];
    if      (c != ' ' and c >= '9') c = ' ';
    else if (c == ' ')              c = '0';
    else                            c++;
    sLigneModif[iModifX] = c;
    break;
  case MODIF_BANDE: // Modificateur d'une bande
    c = sLigneModif[iModifX];
    if (iModifX <= 2)
    {
      // Modif du nom de la bande A-Z-a-z
      if      (c >= 'z')             c = 'A';
      else if (c >= 'Z' and c < 'a') c = 'a';
      else if (c <  'A')             c = 'A';
      else c++;
    }
    else if (iModifX == 20)
    {
      // Modification présence ou non SMS (0 ou 1)
      if (c <= '0')
        c = '1';
      else
        c = '0';
    }
    else
    {
      // Modif des fréquences et du seuil FC de la bande
      if      (c < '0' or c > '9')
        c = '0';
      else if (c == '9') c = '0';
      else               c++;
    }
    sLigneModif[iModifX] = c;
    break;
  case MODIF_SMS :  // Modificateur pour le test SMS
    if (mSMS == 1)
    {
      mSMS = 0;
      sprintf( sLigneModif, " Test SMS NON");
    }
    else
    {
      mSMS = 1;
      sprintf( sLigneModif, " Test SMS OUI");
    }
    break;
  case MODIF_VEILLE :  // Modificateur pour le mode veille
    if (bVeille == 1)
    {
      bVeille = false;
      sprintf( sLigneModif, " Mise en veille NON  ");
    }
    else
    {
      bVeille = true;
      sprintf( sLigneModif, " Mise en veille OUI  ");
    }
    break;
  case MODIF_GAIN :  // Modificateur du gain numérique
    if (iGain == GAIN_16)
      iGain = GAIN_1;
    else
    {
      int iG = iGain;
      iG++;
      iGain = static_cast<GAINUM>(iG);
    }
    sprintf( sLigneModif, " Gain num. %s       ", GetStrGain(iGain));
    break;
  case MODIF_TEST:  // Modificateur du mode du signal de test
    if (mSignalTest == TESTLoop)
      mSignalTest = NORMAL;
    else
    {
      int iTest = mSignalTest;
      iTest++;
      mSignalTest = static_cast<SIGNAL_TEST>(iTest);
    }
    strcpy( sLigneModif, GetStrTest(mSignalTest));
    break;
  case MODIF_RAZ :  // Modificateur RAZ des données
    if (bIsRAZ)
    {
      bIsRAZ = false;
      sprintf( sLigneModif, " RAZ Datas/Files NON ");
    }
    else
    {
      bIsRAZ = true;
      sprintf( sLigneModif, " RAZ Datas/Files OUI ");
    }
    break;
  case MODIF_PARAMD :  // Modificateur de remise aux paramètres par défaut
    if (bParamDef)
    {
      bParamDef = false;
      sprintf( sLigneModif, " Param par defaut NON");
    }
    else
    {
      bParamDef = true;
      sprintf( sLigneModif, " Param par defaut OUI");
    }
    break;
  }
}

//----------------------------------------------------
// Indique si un champ est en modification
bool GestScreen::IsModif()
{
  return bModif;
}

//----------------------------------------------------
// Retourne le texte associé au signal de test
char *GestScreen::GetStrTest(
  SIGNAL_TEST iTest // Valeur du signal de test
  )
{
  switch (iTest)
  {
  case NORMAL    :  // Pas de signal de test et affichage temporaire
    //              012345678901234567890 N
    sprintf(sTempC," Test Normal Aff Int ");
    break;
  case NORMAL_AFF:  // Pas de signal de test et affichage permanent
    //              012345678901234567890 n
    sprintf(sTempC," Test normal Aff Perm");
    break;
  case TESTACQ   :  // Permet de mesurer le temps d'acquisitions des échantillons
    //              012345678901234567890 A
    sprintf(sTempC," Test Acquisition    ");
    break;
  case TESTCycle  :  // Permet de tester la callback 5 ou 10ms
    //              012345678901234567890 1
    sprintf(sTempC," Test Cycle mesures  ");
    break;
  case TESTFFT   :  // Permet de tester le calcul FFT
    //              012345678901234567890 F
    sprintf(sTempC," Test FFT            ");
    break;
  case TEST200ms :  // Permet de tester la callback 200ms
    //              012345678901234567890 2
    sprintf(sTempC," Test 200ms          ");
    break;
  case TESTMes  :  // Permet de tester la durée d'une mesure + analyse
    //              012345678901234567890 ' '
    sprintf(sTempC," Test 1 Mesure       ");
    break;
  case TESTAffMax:  // Permet d'afficher les 7 fréquences de niveaux les plus élevés
    //              012345678901234567890 M
    sprintf(sTempC," Test Max Niveau     ");
    break;
  case TESTAffFM:  // Permet d'afficher les niveaux des 7 1er canaux de la bande FM
    //              012345678901234567890 a
    sprintf(sTempC," Test a Bande FM     ");
    break;
  case TESTAffGR1:  // Permet d'afficher les niveaux des 7 1er canaux de la bande GR
    //              012345678901234567890 b
    sprintf(sTempC," Test b Bande GR 1   ");
    break;
  case TESTAffGR2:  // Permet d'afficher les niveaux des 7 dernier canaux de la bande GR
    //              012345678901234567890 c
    sprintf(sTempC," Test c Bande GR 2   ");
    break;
  case TESTAffRE:  // Permet d'afficher les niveaux des 7 1er canaux de la bande RE
    //              012345678901234567890 d
    sprintf(sTempC," Test d Bande RE     ");
    break;
  case TESTLoop  :  // Permet de tester le temps de traitement du loop
    //              012345678901234567890 L
    sprintf(sTempC," Test Loop           ");
    break;
  default        :  // Oups, valeur hors limite, on corrige le paramètre !
    //              012345678901234567890 N
    sprintf(sTempC," Test Normal Aff Int ");
    pParams->iSignalTest = NORMAL;
  }
  return sTempC;
}

//----------------------------------------------------
// Retourne le texte associé au gain numérique
char *GestScreen::GetStrGain(
  GAINUM iGain // Valeur du gain
  )
{
  switch (iGain)
  {
  case GAIN_1    :
    strcpy( sTempC, "X01");
    break;
  case GAIN_2    :
    strcpy( sTempC, "X02");
    break;
  case GAIN_4    :
    strcpy( sTempC, "X04");
    break;
  default:
  case GAIN_8    :
    strcpy( sTempC, "X08");
    break;
  case GAIN_16   :
    strcpy( sTempC, "X16");
    break;
  }
  return sTempC;
}

//----------------------------------------------------
// Retourne true s'il faut effacer les données
// Positionne ensuite toujours bIsRAZ à false
bool GestScreen::isRAZDatas()
{
  bool bTemp = bIsRAZ;
  bIsRAZ = false;
  return bTemp;
}
    
//----------------------------------------------------
// Initialise les paramètres aux valeurs par défaut
void GestScreen::InitParamsDefaut()
{
    // Initialisation aux valeurs par défaut
    pParams->sControle = 5555;
    strcpy(pParams->sNomStation, "RhinoLogger");
    pParams->sSeuilRelatif = 20;
    strcpy(pParams->sCodePin, "1234");
    strcpy(pParams->sNumTel1, "          ");
    strcpy(pParams->sNumTel2, "          ");
    strcpy(pParams->sNumTel3, "          ");
    pParams->fCoefBExt = 5.0;
    pParams->fCoefBInt = 2.0;
    pParams->iSignalTest = NORMAL;
    pParams->iGain = GAIN_4;
    strcpy(pParams->sNomFM, "FM");
    pParams->fFMinB[0] = 40.0;
    pParams->fFMinB[1] = 76.0;
    pParams->fFMinB[2] = 99.0;
    pParams->fFMinB[3] = 102.6;
    pParams->fFMinB[4] = 106.6;
    pParams->fFMaxB[0] = 60.0;
    pParams->fFMaxB[1] = 86.0;
    pParams->fFMaxB[2] = 102.5;
    pParams->fFMaxB[3] = 106.5;
    pParams->fFMaxB[4] = 117.0;
    pParams->sSeuilFC[0] = 5;
    pParams->sSeuilFC[1] = 15;
    pParams->sSeuilFC[2] = 10;
    pParams->sSeuilFC[3] = 10;
    pParams->sSeuilFC[4] = 10;
    for (int i=0; i<NBBANDES; i++)
      pParams->bSMSB[i] = false;
}

const unsigned char LogoGR [] = {
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xF0,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xE0,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xC0,
0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0x80,
0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xFE, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xFC, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xF0, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFC, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xF0, 0x00, 0x00, 0x00, 0x03, 0xFF, 0xFF, 0xFC, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xF8, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0xF8, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFC, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xF8, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFE, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xF8, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xFF, 0xF8, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xFF, 0xF0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0x80, 0x00, 0x00, 0x7F, 0xFF, 0xFF, 0xF0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x80, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xE0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xC0, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xE0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xE0, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xC0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x80, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xF0, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x80, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xF8, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFC, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFC, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFE, 0x00, 0x01, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x80, 0x03, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xC0, 0x03, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xE0, 0x03, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xE0, 0x03, 0xFF, 0xFF, 0xFE, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xF0, 0x07, 0xFF, 0xFF, 0xFC, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x1F, 0xF8, 0x07, 0xFF, 0xFF, 0xFC, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x0F, 0xF8, 0x07, 0xFF, 0xFF, 0xFC, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x0F, 0xFC, 0x0F, 0xFF, 0xFF, 0xFC, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x1F, 0xFC, 0x0F, 0xFF, 0xFF, 0xFC, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x7F, 0xFC, 0x1F, 0xFF, 0xFF, 0x9C, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFC, 0x1F, 0xFF, 0xFE, 0x04, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFE, 0x3F, 0xFF, 0xFE, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0x3F, 0xFF, 0xFC, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00,
0x07, 0x90, 0x40, 0x00, 0x80, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x00,
0x04, 0x90, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x7F, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00,
0x04, 0x9E, 0x4F, 0x3C, 0x87, 0x9E, 0xF7, 0x9C, 0x00, 0x07, 0xFF, 0xFF, 0xEC, 0x00, 0x00, 0x00,
0x07, 0x12, 0x49, 0x24, 0x84, 0x94, 0xA4, 0x90, 0x00, 0x01, 0xFF, 0xFF, 0xEC, 0x00, 0x00, 0x00,
0x05, 0x92, 0x49, 0x64, 0x8C, 0x9C, 0xE7, 0x90, 0x00, 0x00, 0x03, 0xFF, 0xCC, 0x00, 0x00, 0x00,
0x04, 0x92, 0x49, 0x24, 0x84, 0xA1, 0x04, 0x10, 0x00, 0x00, 0x01, 0xFF, 0xC0, 0x00, 0x00, 0x00,
0x04, 0xD2, 0x49, 0x3C, 0xF7, 0x9C, 0xE7, 0x90, 0x00, 0x00, 0x00, 0xFF, 0xC0, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x23, 0x10, 0x00, 0x00, 0x00, 0x01, 0x1F, 0xC0, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x80, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00
};




