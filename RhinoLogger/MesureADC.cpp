/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de gestion de l'acquisition
// des échantillons via l'ADC

#include "Arduino.h"
#include "MesureADC.h"

//----------------------------------------------------
// Constructeur
// L'acquisition via DMA fonctionne sur le projet M0DMADAC mais traine beaucoup sur le projet RhinoLogger, probablement à cause de l'utilisation des timers
// L'acquisition via une boucle reste le seul moyen fiable (ATTENTION de bien sélectionner les optimisations du compilateur - cf. RhinoLogger.ino en tête de fichier)
MesureADC::MesureADC(
  const StructParams *pParams // Pointeur sur les paramètres
  )
{
  params   = pParams;
  iNbEch   = MAXECH;
  iMaxEch  = MAXECH;
  iGainNum = GAIN_8;
  pThis    = this;
  pDataOK  = TabEchA;
  pDataIn  = TabEchA;
  bBlocOK      = false;
  bBlocStop    = false;
  bBlocErreur  = false;
  bBlocEnCours = false;
  // Définition de la fréquence d'échantillonnage en Hz
#ifdef TEST_DMA
  FE = 250000;
#else
  FE = 250000;
#endif
}

//----------------------------------------------------
// Initialisation des mesures du convertisseur
void MesureADC::InitMesures()
{
  iMaxEch = MAXECH;
  iGainNum = params->iGain;
  // Init de l'ADC
  InitADC();
#ifdef TEST_DMA
  // Init du DMA
  InitDMA();
  // RAZ des pointeurs des échantillons
  pDataOK  = NULL;
  pDataIn  = NULL;
#endif
}

//----------------------------------------------------
// Initialisation du convertisseur
// (à faire après une lecture normale d'un convertisseur - analogRead)
void MesureADC::InitADC()
{
  analogRead(USINPUT);                           // Sélection de l'entrée
  ADC->CTRLA.bit.ENABLE = 0;                     // Stop ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
#ifdef TEST_DMA
  switch(iGainNum)
  {
  case GAIN_1 : ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_1X_Val;  break;
  case GAIN_2 : ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_2X_Val;  break;
  case GAIN_4 : ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_4X_Val;  break;
  case GAIN_8 : ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_8X_Val;  break;
  case GAIN_16: ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_16X_Val; break;
  }
#endif
  //ADC->REFCTRL.bit.REFSEL = ADC_REFCTRL_REFSEL_INT1V_Val; // Référence 1V
  ADC->INPUTCTRL.bit.GAIN = ADC_INPUTCTRL_GAIN_DIV2_Val;    // Référence VCC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  ADC->INPUTCTRL.bit.MUXPOS = g_APinDescription[USINPUT].ulADCChannelNumber;
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  ADC->AVGCTRL.reg  = 0x00;  // Pas de moyennage
  // Temps de lecture pour faire la Fe (SAMPLEN + 1) * (CLKADC / 2)
  // Soit T=(11+1)*((1/((48000000/2)/16))/2)
  // Normalement 11 pour obtenir 4µs (Fe = 250kHz, 512 ech en 2048µs)
#ifdef TEST_DMA
  // Avec 11 on a 4263µs pour 1024 Ech soit une Fe à 240.2kHz (projet M0DMADAC)
  // Avec 10 on a 4093µs pour 1024 Ech soit une Fe à 250.18kHz (projet M0DMADAC)
  ADC->SAMPCTRL.reg = 10;  //sample length in 1/2 CLK_ADC cycles
#else
  // Avec 10 on a 2052µs pour 512 Ech soit une Fe à 249.51kHz
  // Avec  9 on a 1967µs pour 512 Ech soit une Fe à 260.29kHz
  // Avec  8 on a 1981µs pour 512 Ech soit une Fe à 258.45kHz
  ADC->SAMPCTRL.reg = 10;  //sample length in 1/2 CLK_ADC cycles
#endif
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  #ifdef DIV8_8BITS
    ADC->CTRLB.reg = ADC_CTRLB_PRESCALER_DIV16 | ADC_CTRLB_FREERUN | ADC_CTRLB_RESSEL_8BIT;
  #endif
  #ifdef DIV8_12BITS
    ADC->CTRLB.reg = ADC_CTRLB_PRESCALER_DIV16 | ADC_CTRLB_FREERUN | ADC_CTRLB_RESSEL_12BIT;
  #endif
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
  ADC->CTRLA.bit.ENABLE = 0x01;                  // Lance l'ADC
  while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
}

//----------------------------------------------------
// Initialisation du DMA
void MesureADC::InitDMA()
{
#ifdef TEST_DMA
  PM->AHBMASK.reg |= PM_AHBMASK_DMAC ;
  PM->APBBMASK.reg |= PM_APBBMASK_DMAC ;
  NVIC_EnableIRQ( DMAC_IRQn ) ;

  DMAC->BASEADDR.reg = (uint32_t)descriptor_section;
  DMAC->WRBADDR.reg = (uint32_t)wrb;
  DMAC->CTRL.reg = DMAC_CTRL_DMAENABLE | DMAC_CTRL_LVLEN(0xf);  
  DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_TCMPL; // clear
  DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_TERR;
  DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_SUSP;
  bBlocOK     = false;
  bBlocStop   = false;
  bBlocErreur = false;
#endif
}

//----------------------------------------------------
// Lecture des échantillons via l'ADC et le DMA
void MesureADC::DMA_ADC()
{
#ifdef TEST_DMA
  uint32_t temp_CHCTRLB_reg;

  bBlocEnCours = true;
  DMAC->CHID.reg = DMAC_CHID_ID(chnl);
  DMAC->CHCTRLA.reg &= ~DMAC_CHCTRLA_ENABLE;
  DMAC->CHCTRLA.reg = DMAC_CHCTRLA_SWRST;
  DMAC->SWTRIGCTRL.reg &= (uint32_t)(~(1 << chnl));
  temp_CHCTRLB_reg = DMAC_CHCTRLB_LVL(0) | DMAC_CHCTRLB_TRIGSRC(ADC_DMAC_ID_RESRDY) | DMAC_CHCTRLB_TRIGACT_BEAT;
  DMAC->CHCTRLB.reg = temp_CHCTRLB_reg;
  DMAC->CHINTENSET.reg = DMAC_CHINTENSET_MASK ; // enable all 3 interrupts
  descriptor.descaddr = 0;
  descriptor.srcaddr = (uint32_t) &ADC->RESULT.reg;
  descriptor.btcnt =  MAXECH;
  descriptor.dstaddr = (uint32_t)pDataIn + MAXECH*2;   // end address
  descriptor.btctrl =  DMAC_BTCTRL_BEATSIZE_HWORD | DMAC_BTCTRL_DSTINC | DMAC_BTCTRL_VALID;
  memcpy( &descriptor_section[chnl], &descriptor, sizeof(dmacdescriptor));

  if (params->iSignalTest == TESTACQ)
    digitalWrite(TESTOUT, HIGH);
  // start channel
  DMAC->CHID.reg = DMAC_CHID_ID(chnl);
  DMAC->CHCTRLA.reg |= DMAC_CHCTRLA_ENABLE;
#endif
}

//----------------------------------------------------
// Lance une acquisition des échantillons
void MesureADC::LanceMesures()
{
#ifdef TEST_DMA
  // Init de la destination des données
  // Par défaut le tableau A, sinon, fliflop TableauA/TableauB
  if (pDataIn == NULL || pDataIn == TabEchB)
    pDataIn = TabEchA;
  else
    pDataIn = TabEchB;
  // Lance le DMA, les échantillons seront disponibles au prochain appel
  DMA_ADC();
#else
  pDataIn = TabEchA;
  if (params->iSignalTest == TESTACQ)
    digitalWrite(TESTOUT, HIGH);
  // Boucle de lecture des échantillons sans timer
  for (iNbEch=0; iNbEch<MAXECH; iNbEch++)
  {
    // Lecture d'un échantillon
    ADC->INTFLAG.bit.RESRDY = 1;            // Data ready flag cleared
    while( ADC->STATUS.bit.SYNCBUSY == 1 ); // Attente synchro
    ADC->SWTRIG.bit.START = 1;              // Lance la conversion
    TabImm[iNbEch] = 0;                     // Init de la partie immaginaire à 0
    while( ADC->INTFLAG.bit.RESRDY  == 0 ); // Attente fin de la conversion
    while( ADC->STATUS.bit.SYNCBUSY == 1 ); // Attente synchro
    uint32_t valueRead = ADC->RESULT.reg;   // Lecture du résultat
    while( ADC->STATUS.bit.SYNCBUSY == 1 ); // Attente synchro
    #ifdef DIV8_8BITS
      TabEchA[iNbEch] = (valueRead - 128) << iGainNum;
    #endif
    #ifdef DIV8_12BITS
      TabEchA[iNbEch] = (valueRead - 2048) << iGainNum;
    #endif
  }
  if (params->iSignalTest == TESTACQ)
    digitalWrite(TESTOUT, LOW);
#endif
}

//----------------------------------------------------
// Fonction de traitement des interruptions DMA
void MesureADC::OnITDMA()
{
    // Traitement des interruptions DMAC_CHINTENCLR_TERR DMAC_CHINTENCLR_TCMPL DMAC_CHINTENCLR_SUSP
    uint8_t active_channel;

    if (params->iSignalTest == TESTACQ)
      digitalWrite(TESTOUT, LOW);
    // disable irqs ?
    //__disable_irq();
    active_channel =  DMAC->INTPEND.reg & DMAC_INTPEND_ID_Msk; // get channel number
    DMAC->CHID.reg = DMAC_CHID_ID(active_channel);
    bBlocOK     = ((DMAC->CHINTFLAG.reg & DMAC_CHINTENCLR_TCMPL) != 0);
    bBlocStop   = ((DMAC->CHINTFLAG.reg & DMAC_CHINTENCLR_SUSP ) != 0);
    bBlocErreur = ((DMAC->CHINTFLAG.reg & DMAC_CHINTENCLR_TERR ) != 0);
    DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_TCMPL; // clear
    DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_TERR;
    DMAC->CHINTFLAG.reg = DMAC_CHINTENCLR_SUSP;
    // Fin de l'acquisition du bloc
    bBlocEnCours = false;
    //__enable_irq();
    // Si données OK, préparation du pointeur des données disponibles
    pDataOK = NULL; // Par défaut, pas de données disponibles
    if (!bBlocStop && !bBlocErreur && bBlocOK)
    {
      // Préparation des données disponibles
      pDataOK = pDataIn;
      // On ramène les données dans le domaine -128/+127 ou -2048/+2047 au lieu de 0/+255 ou 0/+4095
      for (int i=0; i<MAXECH; i++)
        #ifdef DIV8_8BITS
          pDataOK[i] = (pDataOK[i] - 128);
        #endif
        #ifdef DIV8_12BITS
          pDataOK[i] = (pDataOK[i] - 2048);
        #endif
    }
}

//----------------------------------------------------
// Retourne true si les échantillons sont lus
bool MesureADC::lectureOK()
{
  return (!bBlocStop && !bBlocErreur && bBlocOK && !bBlocEnCours);
}

//----------------------------------------------------
// Retourne le tableau des mesures (partie réelle)
short *MesureADC::getEchs()
{
  return (short *)pDataOK;
}

//----------------------------------------------------
// Retourne le tableau des mesures (partie immaginaire à 0)
short *MesureADC::getImm()
{
#ifdef TEST_DMA
  // RAZ du tableau (le calcul FFT change le tableau, il faut donc le remettre à 0)
  memset( TabImm, 0, sizeof(short)*MAXECH);
#endif
  return TabImm;
}

//----------------------------------------------------
// Fonction standard d'interruption du DMA
void DMAC_Handler()
{
  // Appel de la fonction de traitement de la classe
  MesureADC::pThis->OnITDMA();
}

// Instance des variables statiques
MesureADC *MesureADC::pThis=NULL;          // Pointeur sur l'instance de la classe


