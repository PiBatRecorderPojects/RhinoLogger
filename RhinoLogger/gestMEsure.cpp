/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de gestion des périodes de mesures
// Gère les périodes 10ms (ou 5ms), 200ms, 1s, 1mn, 1h et 1 jour

#include <SPI.h>
#include <SD.h>
#include <DHT.h>
#include "utilit.h"
#include "gestMesure.h"
#include "calcul_fft.h"

// Déclaration des tables de transformation des niveaux en dB
extern const short NivToDbA[];
extern const short NivToDbB[];

// Booléen pour les signaux de test
bool bTestCycle  = false;
bool bTEST200ms = false;
bool bTEST1sec  = false;

// Constantes de gestion date/heure via l'horloge interne au processeur
// Sauvegardées dans la mémoire flash du processeur
extern const byte seconds = 0;
extern const byte minutes = 0;
extern const byte hours   = 0;
extern const byte day     = 1;
extern const byte month   = 1;
extern const byte year    = 16;

// RTC interne au processeur et valeurs de l'heure et la date
extern RTCZero rtcZero;

// Mémo de la configuration de l'ADC
extern int iADC_CTRLBreg;
extern int iADC_AVGCTRLreg;
extern int iADC_SAMPCTRLreg;
extern int iADC_REFCTRLreg;

// Réservation d'une zone en flash pour la sauvegarde du contexte des mesures
// ATTENTION Cette zone est effacée à chaque chargement d'un nouveau programme
FlashStorage(MesuresFlash, StructAllMesures);

//----------------------------------------------------
// Constructeur du timer, utilise le timer 5 pour 
// cadencer à 8ms, Timer DIV64 et Count 92 = 7.949ms, par défaut
GestMesures::GestMesures(
  StructParams *pParams // Pointeur sur les paramètres
  ): ZeroTimer( 5, TC_CTRLA_PRESCALER_DIV64, 92)
  , mesureADC( pParams)
{
  params = pParams;
  bRunMesures = false;
  // Init de la pin 13 en sortie pour les mesures à l'oscilloscope
  pinMode( TESTOUT, OUTPUT);
  // Init du temps du timer par défaut
  ltTimer = 5;
  // Récupération des pointeurs sur les tableaux des échantillons
  pEch = mesureADC.getEchs();
  pImm = mesureADC.getImm();
  // Init des tables des indices des bandes surveillées
  // Init de l'heure et de la date par défaut
  rtcZero.begin(); // initialize RTC
  // Init des mesures par défaut
  mesures.memoAnnexes.fTemp      = -50;      // Température en degrés
  mesures.memoAnnexes.fHumid     = 0;        // Pourcentage d'humidité
  mesures.memoAnnexes.fTempMin   = 100;      // Température en degrés
  mesures.memoAnnexes.fHumidMin  = 200;      // Pourcentage d'humidité
  mesures.memoAnnexes.fTempMax   = -100;     // Température en degrés
  mesures.memoAnnexes.fHumidMax  = -100;     // Pourcentage d'humidité
  mesures.memoAnnexes.fBatExt    = 0;        // Tension de la batterie extérieur en volt
  mesures.memoAnnexes.fBatInt    = 0;        // Tension de la batterie interne en volt
  // RAZ de l'erreur courante
  mesures.Erreur[0] = 0;
  idxFreq = 0;
}

//----------------------------------------------------
// Construction d'un SMS vide
void GestMesures::InitSMS()
{
  // Init de la date et l'heure courante
  sprintf( strDate , "%02d/%02d/%02d", rtcZero.getDay()  , rtcZero.getMonth()  , rtcZero.getYear());
  sprintf( strHeure, "%02d:%02d:%02d", rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  // Construction du SMS vide
  sprintf( strSMS, "%s %c", params->sNomStation, 0x0d);
  sprintf( sTempB  , "%s %s%c", strDate, strHeure, 0x0d);
  strcat(strSMS, sTempB);
  if (strlen(mesures.Erreur) > 0)
    sprintf(sTempB, "Erreur : %s%c", mesures.Erreur, 0x0d);
  else
    sprintf(sTempB, "Pas d'erreur%c", 0x0d);
  strcat(strSMS, sTempB);
  strcat(strSMS, "Pas d'informations !");
}

//----------------------------------------------------
// Initialisation des mesures
void GestMesures::InitMesures(
  bool bRAZ // Indique s'il faut faire une RAZ des données suite à une demande opérateur
  )
{
  // Init FE et nombre d'échantillons
  fFE = (float)mesureADC.getFE() / 1000.0;
  iNbEch = 512;
  iSizeFFT = 9;
  // Timer DIV64 et Count 117 = 10ms, 93 = 8.025ms, 92 = 7.949ms
  SetPeriod( TC_CTRLA_PRESCALER_DIV64, 92);
  ltTimer = 8;
  // Calcul de la précision d'un pas et de la valeur d'un demi pas
  fPrecis = (fFE/2.0)/(iNbEch/2.0);
  fDemi = fPrecis / 2.0;
  iPrecis = (int)(fPrecis * 1000);
  iDemi = iPrecis / 2;
  // Calcul des indices des bandes de surveillance dans les résultats de FFT
  // On travaille ensuite sur des indices et non des fréquences, c'est plus facile et rapide
  float fIndice;
  for (int i=0; i<NBBANDES; i++)
  {
    fIndice      = (((float)params->fFMinB[i] - fDemi) / fPrecis) + 0.5;
    TabIdxMin[i] = (short)fIndice;
    fIndice      = (((float)params->fFMaxB[i] - fDemi) / fPrecis) + 0.5;
    TabIdxMax[i] = (short)fIndice;
  }
  
  // Affichage des paramètres de mesure dans le log
  float fMin, fMax;
  char sFE[15];
  char sFMin[15];
  char sFMax[15];
  char sBande[][5] = {"FM", "GR", "RE", "EP", "PR"};
  dtostrf( fFE, 6, 2, sFE);
  dtostrf( fPrecis, 6, 2, sFMin);
  dtostrf( fDemi, 6, 2, sFMax);
  sprintf( sTempA, "FE %skHz, %d Ech, Precision %skHz, Demi pas %skHz, Seuil relatif %ddb", sFE, iNbEch, sFMin, sFMax, params->sSeuilRelatif);
  Serial.println(sTempA);
  MemoLog(sTempA);
  for (int i=0; i<NBBANDES; i++)
  {
    // Affichage des bandes
    sSeuilQFC[i] = params->sSeuilFC[i];
    fMin = fDemi + ((float)TabIdxMin[i] * fPrecis);
    fMax = fDemi + ((float)TabIdxMax[i] * fPrecis);
    dtostrf( params->fFMinB[i], 6, 2, sFMin);
    dtostrf( params->fFMaxB[i], 6, 2, sFMax);
    dtostrf( fMin, 6, 2, sTempB);
    dtostrf( fMax, 6, 2, sTempC);
    sprintf( sTempA, "Bande %s, Min %s [%d]=%skHz - Max %s [%d]=%skHz, SFC %d, M %d", sBande[i], sFMin, TabIdxMin[i], sTempB, sFMax, TabIdxMax[i], sTempC, sSeuilQFC[i], params->bSMSB[i]);
    Serial.println(sTempA);
    MemoLog(sTempA);
    // RAZ du tableau des activités par heure pour les SMS optionnels
    for (int h=0; h<24; h++)
      mesures.TbActivJ[i][h] = 0;
  }
  if (idxFreq == 0)
    // Par défaut, l'affichage des fréquences en debug est positionnée en début de la bande GR
    idxFreq = TabIdxMin[BGR];
  // Init du temps courant
  currentSec = rtcZero.getSeconds();
  currentMn  = rtcZero.getMinutes();
  currentHh  = rtcZero.getHours();
  // Init de l'indice de l'heure courante
  if (currentHh >= 13)
    iH = currentHh - 13;
  else
    iH = currentHh + 11;
  // Init de la date et l'heure courante
  sprintf( strDate , "%02d/%02d/%02d", rtcZero.getDay()  , rtcZero.getMonth()  , rtcZero.getYear());
  sprintf( strHeure, "%02d:%02d:%02d", rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  // RAZ des tableaux de détection
  for (int i=0; i<MAXECH/2; i++)
  {
    Lst25Det[i] = 0;
    LstSommeDet[i] = 0;
  }
  // RAZ des infos
  lms = 0;
  RAZMemo200ms();
  RAZMemoS();
  RAZMemoT( &mesures.memoM);
  RAZMemoT( &mesures.memoH);
  RAZMemoT( &mesures.memoJ);
  RAZDebugNivMax();
  RAZDebugFreq(true);
  iCompteur5S = 0;
  // Init des noms des fichiers csv (le nom de fichier des jours n'a que l'année)
  if (rtcZero.getYear() == 0)
  {
    strcpy(mesures.Erreur, "Erreur RTC");
    MemoLog( mesures.Erreur);
  }
  sprintf( FileNameJ , "JJ%02d_Log.csv", rtcZero.getYear());
  sprintf( FileNameH , "HH%02d%02d%02d.csv", rtcZero.getYear(), rtcZero.getMonth(), rtcZero.getDay());
  sprintf( FileNameMN, "MM%02d%02d%02d.csv", rtcZero.getYear(), rtcZero.getMonth(), rtcZero.getDay());
  // Lecture éventuelle du contexte précédent
  if (!bRAZ)
    // Récupération du contexte
    LectureContexte();
  else
  {
    // Pas de récupération du contexte, on a donc des données vierges
    MemoLog("RAZ des données et fichiers");
    // RAZ des fichiers présents sauf le log
    while (true)
    {
      // Liste des fichiers à effacer
      char lstFilesNames[10][80];
      int iNbFiles = 0;
      File root = SD.open("/");;
      // Recherche des fichiers
      while (true)
      {
        File entry =  root.openNextFile();
        if (!entry)
          // Plus de fichier
          break;
        if (strstr( entry.name(), "Log.txt") == NULL and !entry.isDirectory())
        {
          // Mémo du nom du fichier
          strcpy( lstFilesNames[iNbFiles], entry.name());
          iNbFiles++;
          if (iNbFiles >= 10)
            break;
        }
        entry.close();
      }
      if (iNbFiles == 0)
        // Plus de fichier
        break;
      else
      {
          // On efface les fichiers
          for (int i=0; i<iNbFiles; i++)
            SD.remove( lstFilesNames[i]);
          iNbFiles = 0;
      }
    }
  }
  // Lecture Température, Humidité et Voltage avant de passer en mesures
  LectureTHV();
  // Mémo de l'heure de lancement des mesures
  MemoLog( "Lancement mesures");
  mesures.iEchMax = 0;
  // RAZ des infos de bruit par canaux
  RAZNoise();
  mesBruitSD.ModeAffMes = MODEMESBRUIT;
  // On lance les mesures des échantillons
  mesureADC.InitMesures();
#ifdef TEST_DMA
  // Lance une acquisition en tache de fond via le DMA
  mesureADC.LanceMesures();
#endif
  // Init du timer des périodes de surveillance
  bRunMesures = true;
  initTimer();
  enable(true);
}

//----------------------------------------------------
// Test d'écriture de la carte SD
void GestMesures::TestFileSD()
{
  File dataFile1, dataFile2;
  char filename[20];
  // Test écriture fichier  
  // Création du nom du fichier
  sprintf( filename, "Test.txt");
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile1 = SD.open(filename, FILE_WRITE);
  if (!dataFile1)
  {
    //                           012345678901234567890
    strcpy( mesBruitSD.sFileSD, "Err. creation fichier");
  }
  else
  {
    // Ecriture dans le fichier
    dataFile1.println("Test ecriture");
    // Vide les buffers et ferme le fichier
    dataFile1.flush();
    dataFile1.close();
    // Test lecture du fichier
    // Ouverture du fichier
    dataFile2 = SD.open(filename, FILE_READ);
    if (!dataFile2)
    {
      //                           012345678901234567890
      strcpy( mesBruitSD.sFileSD, "Err. lecture fichier");
    }
    else
    {
      // Lecture du fichier
      ReadLine( dataFile2, sTempA, 254);
      // Ferme le fichier
      dataFile2.close();
      if (strstr( sTempA, "Test ecriture") == NULL)
        //                           012345678901234567890
        strcpy( mesBruitSD.sFileSD, "Erreur fichier");
      else
        //                           012345678901234567890
        strcpy( mesBruitSD.sFileSD, "Test fichier OK");
    }
    // Effacement du fichier
    SD.remove( filename);
  }
  // Memo du résultat dans le log
  MemoLog( mesBruitSD.sTypeSD);
  MemoLog( mesBruitSD.sFileSD);
  // Mémorisation de la ram libre
  int iFreeRam = GetFreeRam();
  mesBruitSD.fFreeRam = (float)iFreeRam / 1024.0;
  dtostrf( mesBruitSD.fFreeRam, 5, 2, sTempB);
  sprintf( sTempA, "RAM libre %skO", sTempB);
  MemoLog( sTempA);
}

//----------------------------------------------------
// Fonction virtuelle exécutée sur l'évenement timer 8ms
void GestMesures::timerHandler()
{
  if (bRunMesures)
  {
    //Serial.println("GestMesures::timerHandler");
    if (params->iSignalTest == TESTMes)
      digitalWrite(TESTOUT, HIGH);
#ifdef TEST_DMA
    // Teste si des échantillons sont disponibles (lecture en 2.048ms et le timer est de 8ms donc un bloc est dispo)
    pEch = NULL;
    if (mesureADC.lectureOK())
      pEch = mesureADC.getEchs();
    /*else
      // Non, donc réinit de l'acquisition
      mesureADC.InitMesures();*/
    // Lance une nouvelle acquisition en tache de fond via le DMA
    mesureADC.LanceMesures();
    // Si des échantillons précédents sont disponibles
    if (pEch != NULL)
    {
      // Init des immaginaires
      pImm =  mesureADC.getImm();
#else
    // Acquisition des échantillons via une boucle
    mesureADC.LanceMesures();
#endif
      // Traitement des échantillons
      if (params->iSignalTest == TESTFFT)
        digitalWrite(TESTOUT, HIGH);
      // FFT des échantillons
      fix_fft(pEch, pImm, iSizeFFT);
      if (params->iSignalTest == TESTFFT)
        digitalWrite(TESTOUT, LOW);
      // Test de présence d'activité
      TestActivites();
      if (params->iSignalTest == TESTMes)
        digitalWrite(TESTOUT, LOW);
#ifdef TEST_DMA
    }
#endif
    // Calcul du temps
    lms += ltTimer;
    if (lms >= 200)
    {
      // Appel de la méthode de traitement toute les 200ms
      CallBack200ms();
      lms = 0;
    }
    if (params->iSignalTest == TESTCycle)
    {
      if (bTestCycle)
        digitalWrite(TESTOUT, HIGH);
      else
        digitalWrite(TESTOUT, LOW);
      bTestCycle = !bTestCycle;
    }
  }
}

//----------------------------------------------------
// Fonction de traitement des périodes 1s, 1h et 1j appelée
// dans la boucle loop du programme principal
// Retourne true si traitement une seconde
bool GestMesures::loop()
{
  bool bReturn = false;
  // Test de changement des secondes
  if (bRunMesures and currentSec != rtcZero.getSeconds())
  {
    // Mémo de la seconde suivante
    currentSec = rtcZero.getSeconds();
    bReturn = true;
    /*Serial.print("Current ");
    Serial.print(currentSec);
    Serial.print(", Horloge ");
    Serial.println(rtcZero.getSeconds());*/
    // On suspend les mesures pour les traitements 1s, 1mn, 1h et 1j éventuels
    bRunMesures = false;
    if (params->iSignalTest <= NORMAL_AFF)
        // Pour montrer que le système fonctionne
        digitalWrite(TESTOUT, HIGH);
    // Pas de traitement avant la fin de la mesure puis affichage bruit
    if (mesBruitSD.ModeAffMes >= MODEMESAFFMES)
      // Nouvelle seconde, on appelle la fonction de traitement
      CallBack1s();
    if (params->iSignalTest <= NORMAL_AFF)
        // Pour montrer que le système fonctionne
        digitalWrite(TESTOUT, LOW);
    // Test du changement des minutes
    if (currentMn != rtcZero.getMinutes())
    {
      // Nouvelle minute, on appelle la fonction de traitement
      currentMn  = rtcZero.getMinutes();
      //Serial.println("CallBack1mn");
      // Pas de traitement avant la fin de la mesure puis affichage bruit
      if (mesBruitSD.ModeAffMes >= MODEMESAFFMES)
        CallBack1mn();
      //Serial.println("CallBack1mn OK");
      // Test du changement des heures
      if (currentHh  != rtcZero.getHours())
      {
        // Pas de traitement avant la fin de la mesure puis affichage bruit
        if (mesBruitSD.ModeAffMes >= MODEMESAFFMES)
          // Nouvelle heure, on appelle la fonction de traitement
          CallBack1h();
        // Et on met à jour la nouvelle heure
        currentHh  = rtcZero.getHours();
        // Test d'un nouveau jour
        if (currentHh == 12)
        {
          // Nouveau jour, on appelle la fonction de traitement
          //Serial.println("CallBack1j");
          // Pas de traitement avant la fin de la mesure puis affichage bruit
          if (mesBruitSD.ModeAffMes >= MODEMESAFFMES)
            CallBack1j();
          //Serial.println(" OK");
        }
      }
    }
  }
  // On reprend les mesures
  bRunMesures = true;
  return bReturn;
}

//----------------------------------------------------
// Fonction de transformation des niveaux en dB
inline short GestMesures::GetNivDB( int iNiv)
{
  // La table NivToDbA est valide de 1 à 1023 de -120.0dB à -59.7dB.
  // La table NivToDbB est valide de 1024 à 52224 de -59.7dB à -25.6dB.
  int iNivDB = 0;
  // Prise en compte de la valeur absolue pour prendre en compte les magnitude négative
  int iNiveau = abs(iNiv);
  if (iNiveau < MAXTBDBA)
    // de 1 à 1023, c'est le niveau direct en dizième de dB
    iNivDB = NivToDbA[iNiveau];
  else
  {
    iNiveau = (iNiveau-MAXTBDBA)/10;
    if (iNiveau < MAXTBDBB)
      // de 1024 à 52224, c'est le niveau direct en dizième de dB
      iNivDB = NivToDbB[iNiveau];
    else
      // Niveau trop élevé, saturation, on impose -25dB
      iNivDB = -250;
  }
  return iNivDB;
}

//----------------------------------------------------
// Test de présence d'activités
// Exécuté après chaque acquisition+FFT
void GestMesures::TestActivites()
{
  // Le bruit moyen est calculé à chaque début d'heure ou au lancement des mesures
  if (!bNoiseOK)
    CalculBruit();
  else
  {
    switch (params->iSignalTest)
    {
      case TESTAffMax:  // Permet d'afficher les 7 fréquences de niveaux les plus élevés
        // Memo des niveaux max
        MemoDebugNivMax();
        break;
      case TESTAffFM :  // Permet d'afficher les niveaux des 7 1er canaux de la bande FM
        // Memo des niveaux des fréquences
        idxFreq = TabIdxMin[BFM];
        MemoDebugFreq();
        break;
      case TESTAffGR1:  // Permet d'afficher les niveaux des 7 1er canaux de la bande GR
        // Memo des niveaux des fréquences
        idxFreq = TabIdxMin[BGR];
        MemoDebugFreq();
        break;
      case TESTAffGR2:  // Permet d'afficher les niveaux des 7 dernier canaux de la bande GR
        // Memo des niveaux des fréquences
        idxFreq = TabIdxMax[BGR] - 7;
        MemoDebugFreq();
        break;
      case TESTAffRE :  // Permet d'afficher les niveaux des 7 1er canaux de la bande RE
        // Memo des niveaux des fréquences
        idxFreq = TabIdxMin[BRE];
        MemoDebugFreq();
        break;
      default:
        if (bNoiseOK)
        {
          // Table d'ordre d'examen des bandes
          // On commence par GR, prioritaire, puis RE, EP et, pour finir, si GR n'est pas détecté, FM et PR
          uint8_t iTabBandes[] = { BGR, BRE, BEP, BFM, BPR};
          bool bGR = false;
          // Pour chaque bande, on mémorise le niveau max et l'indice
          // pour ne prendre en compte qu'une détection dans chaque bande
          short LstIdxMin[NBBANDES]; // Indice min de détection dans une bande
          short LstIdxMax[NBBANDES]; // Indice max de détection dans une bande
          short LstNivMax[NBBANDES]; // Niveau max dans une bande
          short iNivDB;              // Niveau de chaque canaux
          short iNbFM = 0;           // Nombre de détections dans la bande FM
          bool  bDetect;             // Indicateur de détection
          // Test des différentes bandes
          int iB = 0;
          for (int b=0; b<NBBANDES; b++)
          {
            iB = iTabBandes[b];
            // Si détection dans la bande GR, on ne fait pas de détection sur les bandes FM et PR, 
            // pour éviter la fondamentale et l'harmonique 2
            // Fondamentale (~40kHz 38/43kHz) dans la bande FM et H2 (~120kHz 114/129kHz) dans la bande PR
            if (bGR and iB == BFM)
              break;
            LstIdxMin[iB] = 0;
            LstIdxMax[iB] = 0;
            LstNivMax[iB] = MINDBNS;
            for (int i=TabIdxMin[iB]; i<=TabIdxMax[iB]; i++)
            {
              bDetect = false;
              // Sur ce canal, on enlève la dernière détection sur les 25 mémorisées, si bit de poids faible à 1, on décrémente la somme
              if (Lst25Det[i] & 0x00000001 > 0 and LstSommeDet[i] > 0)
                LstSommeDet[i]--;
              // On décale les 25 dernières détections du canal pour perdre la dernière
              Lst25Det[i] = Lst25Det[i] >> 1;
              // Récupération du niveau du canal en dB
              iNivDB = GetNivDB(pEch[i]);
              // Test si le niveau est au-dessus du seuil (à partir de V1.0, chaque canal détecté compte)
              if (iNivDB > iTabSeuil[i])
              {
                bDetect = true;
                if (iB == BGR)
                  bGR = true;
                // On mémorise les indices min et max de détection
                if (LstIdxMin[iB] == 0)
                  LstIdxMin[iB] = i;
                LstIdxMax[iB] = i;
                // On incrémente la somme des 25 dernières détections du canal
                LstSommeDet[i]++;
                // Et on injecte un 1 sur le bit 24 pour mémoriser cette détection
                // 10987654321098765432109876543210
                // 00000001000000000000000000000000
                //    0   1   0   0   0   0   0   0
                Lst25Det[i] = Lst25Det[i] | 0x01000000;
                if (iB == BFM)
                {
                  // Pour la bande FM impossible de détecter vraiment une FM avec 75% du temps aveugle !
                  // La bande est considérée active si on a au moins autant de détections que le seuil FC de la bande
                  iNbFM++;
                }
                else
                {
                  // Sur bande autre que FM, on teste sur chaque canal la présence d'une FC dans les derniers 200ms
                  int iNbDet = LstSommeDet[i];
                  if (iNbDet >= sSeuilQFC[iB])
                    mesures.memo200ms.LstContacts[iB] = 1;
                  else
                  {
                    // On prend en compte les 2 canaux adjacents pour des cris à cheval sur 2 canaux et prise en compte de l'effet doppler
                    if (i > TabIdxMin[iB] && (iNbDet + LstSommeDet[i-1]) >= sSeuilQFC[iB])
                      mesures.memo200ms.LstContacts[iB] = 1;
                    else if (i < TabIdxMax[iB] && (iNbDet + LstSommeDet[i+1]) >= sSeuilQFC[iB])
                      mesures.memo200ms.LstContacts[iB] = 1;
                  }
                }
              }
              if (iNivDB >= LstNivMax[iB])
                // On mémorise le niveau max
                LstNivMax[iB] = iNivDB;
            } // Fin For canaux d'une bande
            if (iB == BFM && iNbFM >= sSeuilQFC[iB])
              // Détection d'une FM
              mesures.memo200ms.LstContacts[iB] = 1;
            // On mémorise le niveau max de la bande
            if (LstNivMax[iB] > mesures.memo200ms.LstNivMax[iB])
              mesures.memo200ms.LstNivMax[iB] = LstNivMax[iB];
            // En cas de détection, on mémorise les infos FMin et FMax
            if (bDetect)
            {
              // On mémorise les indices min et max des détections
              if (LstIdxMin[iB] < mesures.memo200ms.LstFMin[iB])
                mesures.memo200ms.LstFMin[iB] = LstIdxMin[iB];
              if (LstIdxMax[iB] > mesures.memo200ms.LstFMax[iB])
                mesures.memo200ms.LstFMax[iB] = LstIdxMax[iB];
            }
          } // Fin For des bandes
        }
    } // Fin Switch
  }
}

//----------------------------------------------------
// Calcul du bruit moyen
void GestMesures::CalculBruit()
{
  // On aditionne les mesures sur chaques canaux des bandes d'intérets
  for (int i=TabIdxMin[BFM]; i<TabIdxMax[BPR]; i++)
  {
    int iNiveau  = GetNivDB(pEch[i]);
    if (iNiveau > MINDBNS)
    {
      iTabNoise[i] += iNiveau;
      iTabNbNoise[i]++;
    }
  }
  iNbNoise++;
  if (iNbNoise >= MAXMESURESNOISE)
  {
    // Préparation des infos bruit
    mesBruitSD.iNoiseMin = 250;
    mesBruitSD.iNoiseMax = MINDBNS;
    mesBruitSD.iNbPicNoise = 0;
    // Calcul du bruit moyen sur toute la bande
    int iMaxBruit = 0;
    int iBruit = 0;
    int iNb = 0;
    // Calcul de la moyenne de bruit pour chaque canaux de 40 à 120kHz
    for (int i = TabIdxMin[BFM]; i<TabIdxMax[BPR]; i++)
    {
      // Calcul du bruit moyen du canal
      iTabNoise[i] /= iTabNbNoise[i];
      if (mesBruitSD.iNoiseMin > iTabNoise[i])
        mesBruitSD.iNoiseMin = iTabNoise[i];
      if (mesBruitSD.iNoiseMax < iTabNoise[i])
      {
        mesBruitSD.iNoiseMax = iTabNoise[i];
        iMaxBruit = i;
      }
      iBruit += iTabNoise[i];
      iNb++;
    }
    // Fréquence du max de bruit
    mesBruitSD.fFreqMaxNoise = fDemi + (fPrecis * (float)iMaxBruit);
    // Moyenne globale
    iBruit = iBruit / iNb;
    // Mémo du bruit moyen
    mesures.memoM.iBruitMoyen = iBruit;
    mesBruitSD.iNoiseMoy = iBruit;
    // Calcul du bruit moyen max et du nombre de canaux au-dessus du bruit moyen + 5dB
    int iPicBruit = mesBruitSD.iNoiseMoy + 50;
    iBruit += MAXDIFBRUIT;
    // Calcul du seuil de chaque canaux
    for (int i = TabIdxMin[BFM]; i<TabIdxMax[BPR]; i++)
    {
      // Calcul du seuil de détection du canal
      // ATTENTION, si un canal est très occupé pendant le test (cris nombreux), le bruit moyen du canal risque d'être trop élevé
      // Pour éviter cela, le bruit moyen d'un canal n'est jamais supérieur au bruit moyen global + MAXDIFBRUIT
      /*if (iTabNoise[i] > iBruit)
        iTabSeuil[i] = iBruit + (params->sSeuilRelatif * 10);
      else*/
        iTabSeuil[i] = iTabNoise[i] + (params->sSeuilRelatif * 10);
      // Calcul du nombre de canaux supérieur au bruit moyen + 5dB
      if (iTabNoise[i] > iPicBruit)
        mesBruitSD.iNbPicNoise++;
    }
    // Memo du bruit dans le fichier log
    sprintf( sTempA, "Bruit (dB) min %-03d, moy %-03d, max %-03d", mesBruitSD.iNoiseMin/10, mesBruitSD.iNoiseMoy/10, mesBruitSD.iNoiseMax/10);
    MemoLog( sTempA);
    dtostrf( mesBruitSD.fFreqMaxNoise, 5, 1, sTempB);
    if (mesBruitSD.iNbPicNoise > 1)
      sprintf( sTempA, "Max bruit %skHz %dPics", sTempB, mesBruitSD.iNbPicNoise);
    else
      sprintf( sTempA, "Max bruit %skHz %dPic", sTempB, mesBruitSD.iNbPicNoise);
    MemoLog( sTempA);
    // Memo du bruit dans le fichier bruit.csv
    MemoFichierBruit();
    // Fin du calcul du bruit
    mesBruitSD.ModeAffMes = MODEMESAFFBR;
    bNoiseOK = true;
    Serial.println("Fin calcul bruit, passage en mode affichage bruit et SD");
  }
}

//----------------------------------------------------
// Mémorisation du bruit du bruit des canaux dans un fichier
void GestMesures::MemoFichierBruit()
{
  File dataFile;  // Fichier de sauvegarde des infos
  char filename[80];
  float fFreq;

  // Informe l'opérateur de l'écriture
  digitalWrite(LED_SD, HIGH);
  // Création du nom du fichier
  strcpy( filename, "TBBRUIT.csv");
  // Si le fichier existe, on efface l'ancien
  if (SD.exists( filename))
    SD.remove( filename);
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile = SD.open(filename, FILE_WRITE);
  if (!dataFile)
  {
    Serial.print  (F("Creation du fichier "));
    Serial.print  (filename);
    Serial.println(F(" impossible !"));
    //                       123456789012345678901"
    strcpy( mesures.Erreur, "Ecriture SD err. Br.");
    MemoLog( mesures.Erreur);
  }
  else
  {
    // Ecriture de la 1er ligne du fichier
    dataFile.println("Idx,F,BMoy");

    // Ecriture des infos de bruit
    for (int i = TabIdxMin[BFM]; i<=TabIdxMax[BPR]; i++)
    {
      fFreq  = fDemi + (fPrecis * (float)i);
      dtostrf( fFreq, 3, 2, sTempB);
      sprintf(sTempA, "%d,%s,%d", i, sTempB, iTabNoise[i]/10);
      dataFile.println(sTempA);
    }
    // Vide les buffers et ferme le fichier
    dataFile.flush();
    dataFile.close();
  }
  digitalWrite(LED_SD, LOW);
}

//----------------------------------------------------
// Fonction de traitement sur 200 ms
// On a 25 cycles de mesures effectuées pour 512 échantillons
void GestMesures::CallBack200ms()
{
  // Vérification si détection dans les différentes bandes
  for (int iB = 0; iB<NBBANDES; iB++)
  {
    if (mesures.memo200ms.LstContacts[iB] > 0)
    {
      // On mémorise la détection de la bande pour la période 1s
      mesures.memoSec.LstContacts[iB] = true;
      // On mémorise les indices min et max des détections
      if (mesures.memo200ms.LstFMin[iB] < mesures.memoSec.LstFMin[iB])
        mesures.memoSec.LstFMin[iB] = mesures.memo200ms.LstFMin[iB];
      if (mesures.memo200ms.LstFMax[iB] > mesures.memoSec.LstFMax[iB])
        mesures.memoSec.LstFMax[iB] = mesures.memo200ms.LstFMax[iB];
    }
    // On mémorise le niveau max de la bande
    if (mesures.memo200ms.LstNivMax[iB] > mesures.memoSec.LstNivMax[iB])
      mesures.memoSec.LstNivMax[iB] = mesures.memo200ms.LstNivMax[iB];
  }

  // RAZ des infos pour une nouvelle période 200ms
  RAZMemo200ms();
    
  if (params->iSignalTest == TEST200ms)
  {
    if (bTEST200ms)
    {
      digitalWrite(TESTOUT, HIGH);
      bTEST200ms = false;
    }
    else
    {
      digitalWrite(TESTOUT, LOW);
      bTEST200ms = true;
    }
  }
}

//----------------------------------------------------
// Fonction de traitement sur 1 seconde
void GestMesures::CallBack1s()
{
  // Transfert des infos de la seconde passée dans la minute courante
  iCompteur5S++;
  for (int iB=0; iB<NBBANDES; iB++)
  { 
    // Si détection dans la bande
    if (mesures.memoSec.LstContacts[iB])
    {
      // Ajout d'une seconde positive et la minute positive passe à un
      mesures.memoM.LstSecPos[iB]++;
      mesures.memoM.LstMnPos [iB]=1;
      // Memo des contacts
      mesures.memoSec.LstContacts[iB] += 1;
      // On mémorise les indices min et max des détections
      if (mesures.memoSec.LstFMin[iB] < mesures.memoM.LstFMin[iB])
        mesures.memoM.LstFMin[iB] = mesures.memoSec.LstFMin[iB];
      if (mesures.memoSec.LstFMax[iB] > mesures.memoM.LstFMax[iB])
        mesures.memoM.LstFMax[iB] = mesures.memoSec.LstFMax[iB];
    }
    // Memo des contacts
    if (iCompteur5S >= 5)
    {
      if (mesures.memoSec.LstContacts[iB] > 0)
        mesures.memoM.LstContacts[iB] += 1;
      mesures.memoSec.LstContacts[iB] = 0;
    }
    // On mémorise le niveau max même si aucune détection
    if (mesures.memoM.LstNivMax[iB] < mesures.memoSec.LstNivMax[iB])
      mesures.memoM.LstNivMax[iB] = mesures.memoSec.LstNivMax[iB];
  }
  // Traitement compteur des contacts
  if (iCompteur5S >= 5)
    iCompteur5S = 0;
  // Mémo de la seconde courante dans la précédente
  mesures.saveSec = mesures.memoSec;
  // RAZ des infos de la nouvelle seconde courante
  RAZMemoS();
  // Init de l'heure courante
  sprintf( strHeure, "%02d:%02d:%02d", rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
}

//----------------------------------------------------
// Fonction de traitement sur 1 mn
void GestMesures::CallBack1mn()
{
  // Transfert des infos de la minute passée dans l'heure courante
  for (int iB=0; iB<NBBANDES; iB++)
  { 
    mesures.memoH.LstSecPos  [iB] += mesures.memoM.LstSecPos[iB];
    mesures.memoH.LstMnPos   [iB] += mesures.memoM.LstMnPos[iB];
    mesures.memoH.LstContacts[iB] += mesures.memoM.LstContacts[iB];
    if (mesures.memoH.LstNivMax[iB] < mesures.memoM.LstNivMax[iB])
      mesures.memoH.LstNivMax[iB] = mesures.memoM.LstNivMax[iB];
    // On mémorise les indices min et max des détections
    if (mesures.memoM.LstFMin[iB] < mesures.memoH.LstFMin[iB])
      mesures.memoH.LstFMin[iB] = mesures.memoM.LstFMin[iB];
    if (mesures.memoM.LstFMax[iB] > mesures.memoH.LstFMax[iB])
      mesures.memoH.LstFMax[iB] = mesures.memoM.LstFMax[iB];
  }
  mesures.memoH.iBruitMoyen = mesures.memoM.iBruitMoyen;
  //Serial.println("MemoSD");
  // Mémo des infos de la minute sur la carte SD
  MemoSD( FileNameMN, &mesures.memoM);
  // RAZ des infos de la nouvelle minute courante
  RAZMemoT( &mesures.memoM);
  // On remet le bruit moyen qui ne change que toutes les heures
  mesures.memoM.iBruitMoyen = mesures.memoH.iBruitMoyen;
  if (params->iSignalTest >= TESTAffFM and params->iSignalTest <= TESTAffRE)
    // RAZ des infos de debug Freq
    RAZDebugFreq(true);
}

//----------------------------------------------------
// Fonction de traitement sur 1 h
void GestMesures::CallBack1h()
{
  // Transfert des infos de l'heure passée dans le jour courant
  for (int iB=0; iB<NBBANDES; iB++)
  { 
    mesures.memoJ.LstSecPos  [iB] += mesures.memoH.LstSecPos[iB];
    mesures.memoJ.LstMnPos   [iB] += mesures.memoH.LstMnPos[iB];
    mesures.memoJ.LstContacts[iB] += mesures.memoH.LstContacts[iB];
    if (mesures.memoJ.LstNivMax[iB] < mesures.memoH.LstNivMax[iB])
      mesures.memoJ.LstNivMax[iB] = mesures.memoH.LstNivMax[iB];
    // On mémorise les indices min et max des détections
    if (mesures.memoH.LstFMin[iB] < mesures.memoJ.LstFMin[iB])
      mesures.memoJ.LstFMin[iB] = mesures.memoH.LstFMin[iB];
    if (mesures.memoH.LstFMax[iB] > mesures.memoJ.LstFMax[iB])
      mesures.memoJ.LstFMax[iB] = mesures.memoH.LstFMax[iB];
    // On mémorise pour le SMS optionnel
    if (currentHh >= 12)
      iH = currentHh - 12;
    else
      iH = currentHh + 12;
    mesures.TbActivJ[iB][iH] = (short)mesures.memoH.LstMnPos[iB];
  }
  mesures.memoJ.iBruitMoyen = mesures.memoH.iBruitMoyen;
  // Mémo des infos de l'heure sur la carte SD
  MemoSD( FileNameH, &mesures.memoH);
  // On récupère l'heure courante de l'horloge sauvegardée
  byte second, minute, hour, day, month, year;
  readDS3231time( &second, &minute, &hour, &day, &month, &year);
  // Et on met à jour la date et l'heure du processeur
  rtcZero.setTime(hour, minute, second);
  rtcZero.setDate(day, month, year);
  // Init de la date courante
  sprintf( strDate , "%02d/%02d/%02d", rtcZero.getDay()  , rtcZero.getMonth()  , rtcZero.getYear());
  // RAZ des infos de la nouvelle heure courante
  RAZMemoT( &mesures.memoH);
  // RAZ des infos de bruit par canaux pour forcer une nouvelle mesure de bruit à chaque heure
  //RAZNoise();
}

//----------------------------------------------------
// Fonction de traitement sur 1 jour
void GestMesures::CallBack1j()
{
  // Mémo des infos du jour sur la carte SD
  MemoSD( FileNameJ, &mesures.memoJ, true);
  // Création du SMS de synthèse journalier
  CreateSMSSynthese();
  // Création des éventuels SMS de synthèse par bande
  for (int iB=0; iB<NBBANDES; iB++)
  {
    if (params->bSMSB[iB])
    {
      // Création du SMS
      CreateSMSBande(iB);
      //Serial.println(strBSMS[iB]);
    }
  }
  // RAZ du tableau des activités par heure pour le SMS optionnel
  for (int iB=0; iB<NBBANDES; iB++)
  {
    for (int j=0; j<24; j++)
      mesures.TbActivJ[iB][j] = 0;
  }
  // RAZ des min/max des températures et humidités
  mesures.memoAnnexes.fTempMin   = 100;
  mesures.memoAnnexes.fHumidMin  = 200;
  mesures.memoAnnexes.fTempMax   = -100;
  mesures.memoAnnexes.fHumidMax  = -100;
  // Init de la date et l'heure courante
  sprintf( strDate , "%02d/%02d/%02d", rtcZero.getDay()  , rtcZero.getMonth()  , rtcZero.getYear());
  sprintf( strHeure, "%02d:%02d:%02d", rtcZero.getHours(), rtcZero.getMinutes(), rtcZero.getSeconds());
  // Init des nouveaux noms de fichier csv
  sprintf( FileNameH , "HH%02d%02d%02d.csv", rtcZero.getYear(), rtcZero.getMonth(), rtcZero.getDay());
  sprintf( FileNameMN, "MM%02d%02d%02d.csv", rtcZero.getYear(), rtcZero.getMonth(), rtcZero.getDay());
  // RAZ des infos du nouveau jour courant
  RAZMemoT( &mesures.memoJ);
}

//----------------------------------------------------
// Fonction de mémorisation des infos dans un fichier csv
void GestMesures::MemoSD(
  char *pFileName,          // Nom du fichier
  StructMemoPeriodeT *data, // Données à écrire
  bool bJour                // Indique si c'est un fichier jour
  )
{
  File dataFile;  // Fichier de sauvegarde des infos

  // Informe l'opérateur de l'écriture
  digitalWrite(LED_SD, HIGH);
  //Serial.println("LectureTHV");
  // Lecture des infos complémentaires
  LectureTHV();
  //Serial.println("LectureTHV OK");
  
  // Test si le fichier existe
  bool bEntete = true;
  if (SD.exists(pFileName))
    bEntete = false;
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile = SD.open(pFileName, FILE_WRITE);
  if (! dataFile)
  {
    Serial.print  (F("Creation du fichier "));
    Serial.print  (pFileName);
    Serial.println(F(" impossible !"));
    //                       123456789012345678901"
    strcpy( mesures.Erreur, "Ecriture SD erreur");
    MemoLog( mesures.Erreur);
  }
  else
  {
    if (bEntete)
    {
      if (bJour)
        // Ecriture de la 1er ligne du fichier
        dataFile.println(F("Date\tHeure\tSFM\tMFM\tCFM\tNFM\tfFM\tFFM\tSGR\tMGR\tCGR\tNGR\tfGR\tFGR\tSRE\tMRE\tCRE\tNRE\tfRE\tFRE\tSEP\tMEP\tCEP\tNEP\tfEP\tFEP\tSPR\tMPR\tCPR\tNPR\tfPR\tFPR\tBRUIT\tTMin\tTMax\tHMin\tHMax\tB. Ext.\tB. Int.\tMinBr\tMaxBr\tNbPB\tFrMax"));
      else
        // Ecriture de la 1er ligne du fichier
        dataFile.println(F("Date\tHeure\tSFM\tMFM\tCFM\tNFM\tfFM\tFFM\tSGR\tMGR\tCHR\tNGR\tfGR\tFGR\tSRE\tMRE\tCRE\tNRE\tfRE\tFRE\tSEP\tMEP\tCEP\tNEP\tfEP\tFEP\tSPR\tMPR\tCPR\tNPR\tfPR\tFPR\tBRUIT\tT\tH\tB. Ext.\tB. Int."));
    }

    // Ecriture de la ligne
    // Date, Heure
    sprintf(sTempA, "%s\t%s\t", data->strDate, data->strHeure);
    dataFile.print(sTempA);
    // Les infos des différentes espèces SFM,MFM,NFM,SGR,MGR,NGR,SRE,MRE,NRE,SEP,MEP,NEP,SPR,MPR,NPR,
    int fMin, fMax;
    for (int iB=0; iB<NBBANDES; iB++)
    {
      if (data->LstFMin[iB] < MAXECH)
        fMin = iDemi + (data->LstFMin[iB] * iPrecis);
      else
        fMin = 0;
      if (data->LstFMax[iB] > 0)
        fMax = iDemi + (data->LstFMax[iB] * iPrecis);
      else
        fMax = 0;
      sprintf(sTempA, "%d\t%d\t%d\t%d\t%d\t%d\t", data->LstSecPos[iB], data->LstMnPos[iB], data->LstContacts[iB], (data->LstNivMax[iB]/10), fMin, fMax);
      dataFile.print(sTempA);
    }
    if (bJour)
    {
      // Infos complémentaires BRUIT,TMin,TMax,HMin,HMax,B. Ext.,B. Int.
      fMax = (int)(mesBruitSD.fFreqMaxNoise * 1000);
      sprintf(sTempA, "%d\t%d\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%d", (data->iBruitMoyen/10), (int)mesures.memoAnnexes.fTempMin, (int)mesures.memoAnnexes.fTempMax,
        (int)mesures.memoAnnexes.fHumidMin, (int)mesures.memoAnnexes.fHumidMax, mesures.memoAnnexes.sBatExt, mesures.memoAnnexes.sBatInt,
        mesBruitSD.iNoiseMin/10, mesBruitSD.iNoiseMax/10, mesBruitSD.iNbPicNoise, fMax);
    }
    else
      // Infos complémentaires BRUIT,T,H,B. Ext.,B. Int.
      sprintf(sTempA, "%d\t%d\t%d\t%s\t%s", (data->iBruitMoyen/10), (int)mesures.memoAnnexes.fTemp, (int)mesures.memoAnnexes.fHumid, 
        mesures.memoAnnexes.sBatExt, mesures.memoAnnexes.sBatInt);
    dataFile.println(sTempA);
    // Vide les buffers et ferme le fichier
    dataFile.flush();
    dataFile.close();
  }
  digitalWrite(LED_SD, LOW);
}

//----------------------------------------------------
// Lecture des mesures complémentaires
void GestMesures::LectureTHV(
  bool bInitADC // True pour initialiser l'ADC en mode mesure, false sinon
  )
{
    // Lecture de la température et de l'humidité
    // Trois boucles de lecture pour 3 tentatives max
    bool bErreurDHT = false;
    for (int i=0; i<3; i++)
    {
      DHT dht(DHTPIN, DHTTYPE); // Capteur de température et humidité
      dht.begin();
      delay(1000);
      // La lecture du capteur prend 250ms
      // Les valeurs lues peuvent etre vieilles de jusqu'a 2 secondes (le capteur est lent)
      float h = dht.readHumidity();     // on lit l'hygrometrie
      float t = dht.readTemperature();  // on lit la temperature en celsius (par defaut)
      //On verifie si la lecture a echoue, si non on quitte la boucle
      if (isnan(h) || isnan(t))
      {
        bErreurDHT = true;
        mesures.memoAnnexes.fTemp      = -50.0;
        mesures.memoAnnexes.fHumid     = 0.0;
      }
      else
      {
        bErreurDHT = false;
        mesures.memoAnnexes.fTemp      = t;
        mesures.memoAnnexes.fHumid     = h;
        // Init des min et max de la journée
        if (t < mesures.memoAnnexes.fTempMin)
          mesures.memoAnnexes.fTempMin = t;
        if (t > mesures.memoAnnexes.fTempMax)
          mesures.memoAnnexes.fTempMax = t;
        if (h < mesures.memoAnnexes.fHumidMin)
          mesures.memoAnnexes.fHumidMin = h;
        if (h > mesures.memoAnnexes.fHumidMax)
          mesures.memoAnnexes.fHumidMax = h;
        break;
      }
      delay(200);
    }
    if (bErreurDHT)
    {
      //                       123456789012345678901"
      strcpy( mesures.Erreur, "Erreur sonde DHT");
      MemoLog( mesures.Erreur);
    }

    if (bInitADC)
    {
      // Restitution du réglage de l'ADC pour les lectures normales
      ADC->CTRLA.bit.ENABLE = 0;                     // Stop ADC
      while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
      ADC->CTRLB.reg    = iADC_CTRLBreg;
      ADC->AVGCTRL.reg  = iADC_AVGCTRLreg;
      ADC->SAMPCTRL.reg = iADC_SAMPCTRLreg;
      ADC->REFCTRL.reg  = iADC_REFCTRLreg;
      ADC->CTRLA.bit.ENABLE = 1;                     // Lance l'ADC
      while( ADC->STATUS.bit.SYNCBUSY == 1 );        // Attente synchro
      analogReadResolution(10);
      analogReference(AR_DEFAULT);
      analogRead( KEYBOARD);
    }
    
    // Lecture de la batterie interne
    float measuredvbat = analogRead(INTPWR);
    measuredvbat *= params->fCoefBInt;      // Prise en compte du diviseur de tension
    measuredvbat *= 3.3;    // Prise en compte de la tension de référence
    measuredvbat /= 1024;   // Conversion en volts
    if (measuredvbat < 0)
      measuredvbat = 0;
    mesures.memoAnnexes.fBatInt = measuredvbat;  // Tension de la batterie extérieur en volt
    dtostrf( measuredvbat, 3, 1, mesures.memoAnnexes.sBatInt);
    SwapDecimal(mesures.memoAnnexes.sBatInt);

    // Lecture de la batterie externe
    measuredvbat = analogRead(EXTPWR);
    measuredvbat *= params->fCoefBExt;      // Prise en compte du diviseur de tension
    measuredvbat *= 3.3;    // Prise en compte de la tension de référence
    measuredvbat /= 1024;   // Conversion en volts
    measuredvbat += 0.66;   // Prise en compte chute de tension dans la diode de protection
    if (measuredvbat < 0)
      measuredvbat = 0;
    mesures.memoAnnexes.fBatExt = measuredvbat;  // Tension de la batterie extérieure en volt
    dtostrf( measuredvbat, 4, 1, mesures.memoAnnexes.sBatExt);
    SwapDecimal(mesures.memoAnnexes.sBatExt);

    if (bInitADC)
      // Réinitialisation de l'ADC pour les acquisitions
      mesureADC.InitADC();
}

//----------------------------------------------------
// Remplace le point décimal par une virgule
char *GestMesures::SwapDecimal(
  char *pStr    // Chaîne à modifier
  )
{
  char *p = strchr( pStr, '.');
  if (p != NULL)
    // Point trouvé, on le remplace par une virgule
    *p = ',';
  return pStr;
}


//----------------------------------------------------
// Construction du SMS de synthèse journalier
void GestMesures::CreateSMSSynthese()
{
  // Composition du SMS :
  // 000000000011111111112222222222
  // 012345678901234567890123456789
  // NOMSTATION Bruit -100dB -100/-100  33 char max
  // 12/06/16 12:00:00                  18 char
  // FM 5618 -70dB                      15 char max
  // GR 24628 -75dB
  // RE 0 -
  // EP 12 -76dB
  // PR 8 -85dB
  // T15/17 H80/82 Ext12.6V Int3.7V     29 char
  // Total 155 char max sans erreur (un SMS fait 160 char au maximum)

  // Constitution du SMS
  sprintf( strSMS, "%s Bruit %ddB %d/%d%c", params->sNomStation, mesures.memoJ.iBruitMoyen/10, mesBruitSD.iNoiseMin/10, mesBruitSD.iNoiseMax/10, 0x0d);
  sprintf( sTempA, "%s %s%c", strDate, strHeure, 0x0d);
  strcat(strSMS, sTempA);
  for (int i=0; i<NBBANDES; i++)
  {
    switch (i)
    {
    case BFM: strcpy( sTempB, params->sNomFM); break;
    case BGR: strcpy( sTempB, "GR"); break;
    case BRE: strcpy( sTempB, "RE"); break;
    case BEP: strcpy( sTempB, "EP"); break;
    case BPR: strcpy( sTempB, "PR"); break;
    }
    sprintf( sTempA, "%s %d %ddB%c", sTempB, mesures.memoJ.LstMnPos[i], mesures.memoJ.LstNivMax[i]/10, 0x0d);
    strcat(strSMS, sTempA);
  }
  sprintf( sTempA, "T%d/%d H%d/%d Ext%sV Int%sV", (int)mesures.memoAnnexes.fTempMin, (int)mesures.memoAnnexes.fTempMax, (int)mesures.memoAnnexes.fHumidMin,
    (int)mesures.memoAnnexes.fHumidMax, mesures.memoAnnexes.sBatExt, mesures.memoAnnexes.sBatInt);
  strcat(strSMS, sTempA);
  if (strlen(mesures.Erreur) > 0)
  {
    sprintf(sTempA, "%c%s", 0x0d, mesures.Erreur);
    strcat(strSMS, sTempA);
  }
  #ifdef TSTGSM
    sprintf( sTempA, "Taille SMS : %d", strlen(strSMS));
    MemoLog( sTempA);
  #endif
  // Vérification de la taille
  if (strlen(strSMS) > 160)
  {
    // Mémo de l'incident
    sprintf( sTempA, "SMS trop long (%d caracteres)", strlen(strSMS));
    MemoLog( sTempA);
    // On tronque à 160 caractères maximum
    strSMS[159] = 0;
  }
}

//----------------------------------------------------
// Construction du SMS de synthèse par bande
void GestMesures::CreateSMSBande(
  int iB  // Indice de la bande
  )
{
  // Composition du SMS :
  // 000000000011111111112222222222
  // 012345678901234567890123456789
  // 12/06/16 GR                   12 char
  // 13-60                          6 char
  // 14-60                          6 char
  // 15-60                          6 char
  // 16-60                          6 char
  // 17-60                          6 char
  // 18-60                          6 char
  // 19-60                          6 char
  // 20-60                          6 char
  // 21-60                          6 char
  // 22-60                          6 char
  // 23-60                          6 char
  // 00-60                          6 char
  // 01-60                          6 char
  // 02-60                          6 char
  // 03-60                          6 char
  // 04-60                          6 char
  // 05-60                          6 char
  // 06-60                          6 char
  // 07-60                          6 char
  // 08-60                          6 char
  // 09-60                          6 char
  // 10-60                          6 char
  // 11-60                          6 char
  // 12-60                          6 char
  //                                Total 24*6=144 + 12 =156 char (un SMS fait 160 char au maximum)

  int iAct;
  // Nom de la bande
  switch (iB)
  {
  case BFM: strcpy( sTempB, params->sNomFM); break;
  case BGR: strcpy( sTempB, "GR"); break;
  case BRE: strcpy( sTempB, "RE"); break;
  case BEP: strcpy( sTempB, "EP"); break;
  case BPR: strcpy( sTempB, "PR"); break;
  }
  // Constitution du SMS
  sprintf( strBSMS[iB]  , "%s %s%c", strDate, sTempB, 0x0d);
  for (int i=0, h=0; i<24; i++)
  {
    if (i < 12)
      h = i + 12;
    else
      h = i - 12;
    iAct = mesures.TbActivJ[iB][i];
    sprintf( sTempA, "%02d %02d%c", h, iAct, 0x0d);
    strcat(strBSMS[iB], sTempA);
  }
  MemoLog( strBSMS[iB]);
  // Vérification de la taille
  if (strlen(strBSMS[iB]) > 160)
  {
    // Mémo de l'incident
    sprintf( sTempA, "SMS bande %d trop long (%d caracteres)", iB, strlen(strSMS));
    MemoLog( sTempA);
    // On tronque
    strncpy(strBSMS[iB], strBSMS[iB], 159); 
  }
}

//----------------------------------------------------
// RAZ des infos de la période 200ms
void GestMesures::RAZMemo200ms()
{
  for (int i=0; i<NBBANDES; i++)
  {
    mesures.memo200ms.LstNivMax  [i] = MINDBNS;
    mesures.memo200ms.LstContacts[i] = 0;
    mesures.memo200ms.LstFMin    [i] = MAXECH;
    mesures.memo200ms.LstFMax    [i] = 0;
  }
  mesures.memo200ms.iFFMax = 0;
}

//----------------------------------------------------
// RAZ des infos de la seconde
void GestMesures::RAZMemoS()
{
  for (int i=0; i<NBBANDES; i++)
  {
    mesures.memoSec.LstNivMax  [i] = MINDBNS;
    mesures.memoSec.LstContacts[i] = false;
    mesures.memoSec.LstFMin    [i] = MAXECH;
    mesures.memoSec.LstFMax    [i] = 0;
  }
}

//----------------------------------------------------
// RAZ des infos de mémorisation
void GestMesures::RAZMemoT(
  StructMemoPeriodeT *memoX  // Pointeur sur les infos à effacer
  )
{
  // RAZ des bandes et du bruit moyen
  for (int i=0; i<NBBANDES; i++)
  {
    memoX->LstNivMax  [i] = MINDBNS;
    memoX->LstSecPos  [i] = 0;
    memoX->LstMnPos   [i] = 0;
    memoX->LstContacts[i] = 0;
    memoX->LstFMin    [i] = MAXECH;
    memoX->LstFMax    [i] = 0;
  }
  memoX->iBruitMoyen = 0;
  // Mémo de l'heure de départ de la période
  strcpy( memoX->strDate , strDate);
  strcpy( memoX->strHeure, strHeure);
}

//----------------------------------------------------
// Fonction de mémorisation des infos de debug des niveaux max
void GestMesures::MemoDebugNivMax()
{
  short iNivDB;
  // Pour l'ensemble de la gamme surveillée
  for (int i = TabIdxMin[BFM]; i<TabIdxMax[BPR]; i++)
  {
    
    for (int j = 0; j<NBLINE; j++)
    {
      if (pEch[i] > mesDebug[j].iLevelMax)
      {
        // On pousse les niveaux existants vers le fond
        for (int k = j; k<(NBLINE-1); k++)
          mesDebug[k+1] = mesDebug[k];
        // Ajout du niveau a la liste
        mesDebug[j].iLevelMax = pEch[i];
        // On passe en dB
        iNivDB = GetNivDB(pEch[i]);
        mesDebug[j].idBMax    = iNivDB;
        mesDebug[j].idxMax    = i;
        mesDebug[j].fFreqMax  = fDemi + (fPrecis * (float)i);
        break;
      }
    }
  }  
}

//----------------------------------------------------
// Fonction de mémorisation des infos de debug des fréquences
void GestMesures::MemoDebugFreq()
{
  int j;
  short iNivDB;
  if (bNoiseOK)
  {
    //Serial.print("MemoDebugFreq idxFreq");
    // Pour les 7 lignes affichées
    for (int i = 0; i<NBLINE; i++)
    {
      j = idxFreq + i;
      // On passe en dB
      iNivDB = GetNivDB(pEch[j]);
      if (iNivDB > mesFreq[i].idB)
      {
        // Ajout du niveau a la liste
        mesFreq[i].idB       = iNivDB;
      }
      if (iNivDB > mesFreq[i].idBMax)
        mesFreq[i].idBMax  = iNivDB;
      mesFreq[i].iNoise    = iTabNoise[j];
      mesFreq[i].idxMax    = j;
      mesFreq[i].fFreqMax  = fDemi + (fPrecis * (float)j);
    }
  }
}

//----------------------------------------------------
// RAZ des infos de debug d'affichage des niveaux max
void GestMesures::RAZDebugNivMax()
{
  for (int i=0; i<NBLINE; i++)
  {
    mesDebug[i].iLevelMax = -255;
    mesDebug[i].idBMax    = MINDBNS;
    mesDebug[i].idxMax    = 0;
    mesDebug[i].fFreqMax  = 0.0;
  }
}

//----------------------------------------------------
// RAZ des infos de debug d'affichage des fréquences par bloc de 7
void GestMesures::RAZDebugFreq(
  bool bMax1mn  // Pour effacer le max toute les minutes
  )
{
  int j;
  for (int i=0; i<NBLINE; i++)
  {
    j = idxFreq + i;
    mesFreq[i].idB       = MINDBNS;
    if (bMax1mn)
      mesFreq[i].idBMax  = MINDBNS;
    mesFreq[i].idxMax    = 0;
    mesFreq[i].fFreqMax  = 0.0;
  }
}

//----------------------------------------------------
// RAZ des infos du bruit
void GestMesures::RAZNoise()
{
  // RAZ des infos de bruit par canaux
  for (int i=0; i<MAXECH/2; i++)
  {
    iTabNoise[i] = 0;
    iTabNbNoise[i] = 0;
  }
  // Pour forcer le calcul du bruit sur les prochaines mesures
  bNoiseOK = false;
  iNbNoise = 0;
  mesures.memoM.iBruitMoyen = 0;
}

//----------------------------------------------------
// Sauvegarde des résultats partiel en quittant le mode mesure
void GestMesures::SauveContexte()
{
  // Les mesures sont sauvegardées en flash
  mesures.iEchMax = 5555; // Pour indiquer que la flash possède bien une sauvegarde
  mesures.iJour   = rtcZero.getDay();
  
  MesuresFlash.write( mesures);
  mesures.iEchMax = 0;
}

//----------------------------------------------------
// Lecture des résultats partiels en reprenant le mode mesure
void GestMesures::LectureContexte()
{
  // Les mesures sont lues en flash dans un résultat temporaire
  // Lecture de la flash et vérification si elle est à 0
  StructAllMesures mes = MesuresFlash.read();
  if (mes.iEchMax == 5555 and mes.iJour == rtcZero.getDay())
  {
    // Lecture correcte et dans le même jour, on initialise les mesures avec celles de la sauvegarde flash
    // (si le contexte n'est pas du même jour, on ne le prend pas en compte, il est perdu)
    mesures = mes;
    mesures.iEchMax = 0;
    // RAZ de la flash pour indiquer qu'il ny a plus de sauvegarde
    mes.iEchMax = 0;
    MesuresFlash.write( mes);
  }
}

//----------------------------------------------------
// Mémorisation d'une info dans le LOG
void GestMesures::MemoLog(
  char *pStr    // Info à sauvegarder
  )
{
  File dataFile;  // Fichier de sauvegarde des infos
  char filename[25];
  char txt[255];

  // Création du nom du fichier
  sprintf( filename, "Log.txt");
  // Ouverture du fichier (création s'il n'existe pas)
  dataFile = SD.open(filename, FILE_WRITE);
  if (!dataFile)
  {
    Serial.print  (F("Creation du fichier "));
    Serial.print  (filename);
    Serial.println(F(" impossible !"));
    //                       123456789012345678901"
    strcpy( mesures.Erreur, "Ecriture SD err. log");
    MemoLog( mesures.Erreur);
  }
  else
  {
    // Fomatage de la chaine
    sprintf( txt, "%s %s %s", strDate, strHeure, pStr);
    // Ecriture dans le fichier
    dataFile.println(txt);
    // Vide les buffers et ferme le fichier
    dataFile.flush();
    dataFile.close();
  }
}

//----------------------------------------------------
// Tables de transformation des niveaux en dB. Chaque valeur est exprimée en dixième de dB
// La table NivToDbA est valide de 1 à 1023 de -120.0dB à -59.7dB.
// La table NivToDbB est valide de 1024 à 52224 de -59.7dB à -25.6dB.
// Au delà, il y a saturation et le niveau est fixé à -25dB
const short NivToDbA[] = {
-1200, -1139, -1104, -1079, -1060, -1044, -1030, -1019, -1009, -1000, -991, -984, -977, -970, -964, -959, -953, -948, -944, -939, 
-935, -931, -927, -923, -920, -917, -913, -910, -907, -904, -901, -898, -896, -893, -891, -888, -886, -884, -881, -879, 
-877, -875, -873, -871, -869, -867, -865, -863, -861, -860, -858, -856, -855, -853, -851, -850, -848, -847, -845, -844, 
-842, -841, -840, -838, -837, -836, -834, -833, -832, -830, -829, -828, -827, -826, -824, -823, -822, -821, -820, -819, 
-818, -817, -816, -815, -814, -813, -812, -811, -810, -809, -808, -807, -806, -805, -804, -803, -802, -801, -800, -800, 
-799, -798, -797, -796, -795, -794, -794, -793, -792, -791, -790, -790, -789, -788, -787, -787, -786, -785, -784, -784, 
-783, -782, -782, -781, -780, -779, -779, -778, -777, -777, -776, -775, -775, -774, -773, -773, -772, -772, -771, -770, 
-770, -769, -768, -768, -767, -767, -766, -765, -765, -764, -764, -763, -763, -762, -761, -761, -760, -760, -759, -759, 
-758, -758, -757, -757, -756, -755, -755, -754, -754, -753, -753, -752, -752, -751, -751, -750, -750, -749, -749, -748, 
-748, -747, -747, -747, -746, -746, -745, -745, -744, -744, -743, -743, -742, -742, -741, -741, -741, -740, -740, -739, 
-739, -738, -738, -738, -737, -737, -736, -736, -735, -735, -735, -734, -734, -733, -733, -733, -732, -732, -731, -731, 
-731, -730, -730, -729, -729, -729, -728, -728, -728, -727, -727, -726, -726, -726, -725, -725, -725, -724, -724, -723, 
-723, -723, -722, -722, -722, -721, -721, -721, -720, -720, -720, -719, -719, -719, -718, -718, -718, -717, -717, -717, 
-716, -716, -716, -715, -715, -715, -714, -714, -714, -713, -713, -713, -712, -712, -712, -711, -711, -711, -710, -710, 
-710, -709, -709, -709, -709, -708, -708, -708, -707, -707, -707, -706, -706, -706, -706, -705, -705, -705, -704, -704, 
-704, -703, -703, -703, -703, -702, -702, -702, -702, -701, -701, -701, -700, -700, -700, -700, -699, -699, -699, -698, 
-698, -698, -698, -697, -697, -697, -697, -696, -696, -696, -696, -695, -695, -695, -694, -694, -694, -694, -693, -693, 
-693, -693, -692, -692, -692, -692, -691, -691, -691, -691, -690, -690, -690, -690, -689, -689, -689, -689, -688, -688, 
-688, -688, -688, -687, -687, -687, -687, -686, -686, -686, -686, -685, -685, -685, -685, -684, -684, -684, -684, -684, 
-683, -683, -683, -683, -682, -682, -682, -682, -682, -681, -681, -681, -681, -680, -680, -680, -680, -680, -679, -679, 
-679, -679, -678, -678, -678, -678, -678, -677, -677, -677, -677, -677, -676, -676, -676, -676, -675, -675, -675, -675, 
-675, -674, -674, -674, -674, -674, -673, -673, -673, -673, -673, -672, -672, -672, -672, -672, -671, -671, -671, -671, 
-671, -670, -670, -670, -670, -670, -669, -669, -669, -669, -669, -668, -668, -668, -668, -668, -668, -667, -667, -667, 
-667, -667, -666, -666, -666, -666, -666, -665, -665, -665, -665, -665, -665, -664, -664, -664, -664, -664, -663, -663, 
-663, -663, -663, -663, -662, -662, -662, -662, -662, -661, -661, -661, -661, -661, -661, -660, -660, -660, -660, -660, 
-660, -659, -659, -659, -659, -659, -658, -658, -658, -658, -658, -658, -657, -657, -657, -657, -657, -657, -656, -656, 
-656, -656, -656, -656, -655, -655, -655, -655, -655, -655, -654, -654, -654, -654, -654, -654, -654, -653, -653, -653, 
-653, -653, -653, -652, -652, -652, -652, -652, -652, -651, -651, -651, -651, -651, -651, -650, -650, -650, -650, -650, 
-650, -650, -649, -649, -649, -649, -649, -649, -648, -648, -648, -648, -648, -648, -648, -647, -647, -647, -647, -647, 
-647, -647, -646, -646, -646, -646, -646, -646, -645, -645, -645, -645, -645, -645, -645, -644, -644, -644, -644, -644, 
-644, -644, -643, -643, -643, -643, -643, -643, -643, -642, -642, -642, -642, -642, -642, -642, -641, -641, -641, -641, 
-641, -641, -641, -640, -640, -640, -640, -640, -640, -640, -639, -639, -639, -639, -639, -639, -639, -639, -638, -638, 
-638, -638, -638, -638, -638, -637, -637, -637, -637, -637, -637, -637, -637, -636, -636, -636, -636, -636, -636, -636, 
-635, -635, -635, -635, -635, -635, -635, -635, -634, -634, -634, -634, -634, -634, -634, -634, -633, -633, -633, -633, 
-633, -633, -633, -632, -632, -632, -632, -632, -632, -632, -632, -631, -631, -631, -631, -631, -631, -631, -631, -630, 
-630, -630, -630, -630, -630, -630, -630, -629, -629, -629, -629, -629, -629, -629, -629, -629, -628, -628, -628, -628, 
-628, -628, -628, -628, -627, -627, -627, -627, -627, -627, -627, -627, -626, -626, -626, -626, -626, -626, -626, -626, 
-626, -625, -625, -625, -625, -625, -625, -625, -625, -624, -624, -624, -624, -624, -624, -624, -624, -624, -623, -623, 
-623, -623, -623, -623, -623, -623, -623, -622, -622, -622, -622, -622, -622, -622, -622, -622, -621, -621, -621, -621, 
-621, -621, -621, -621, -621, -620, -620, -620, -620, -620, -620, -620, -620, -620, -619, -619, -619, -619, -619, -619, 
-619, -619, -619, -618, -618, -618, -618, -618, -618, -618, -618, -618, -617, -617, -617, -617, -617, -617, -617, -617, 
-617, -617, -616, -616, -616, -616, -616, -616, -616, -616, -616, -615, -615, -615, -615, -615, -615, -615, -615, -615, 
-615, -614, -614, -614, -614, -614, -614, -614, -614, -614, -614, -613, -613, -613, -613, -613, -613, -613, -613, -613, 
-612, -612, -612, -612, -612, -612, -612, -612, -612, -612, -611, -611, -611, -611, -611, -611, -611, -611, -611, -611, 
-611, -610, -610, -610, -610, -610, -610, -610, -610, -610, -610, -609, -609, -609, -609, -609, -609, -609, -609, -609, 
-609, -608, -608, -608, -608, -608, -608, -608, -608, -608, -608, -608, -607, -607, -607, -607, -607, -607, -607, -607, 
-607, -607, -606, -606, -606, -606, -606, -606, -606, -606, -606, -606, -606, -605, -605, -605, -605, -605, -605, -605, 
-605, -605, -605, -605, -604, -604, -604, -604, -604, -604, -604, -604, -604, -604, -603, -603, -603, -603, -603, -603, 
-603, -603, -603, -603, -603, -603, -602, -602, -602, -602, -602, -602, -602, -602, -602, -602, -602, -601, -601, -601, 
-601, -601, -601, -601, -601, -601, -601, -601, -600, -600, -600, -600, -600, -600, -600, -600, -600, -600, -600, -600, 
-599, -599, -599, -599, -599, -599, -599, -599, -599, -599, -599, -598, -598, -598, -598, -598, -598, -598, -598, -598, 
-598, -598, -598, -597  
};
const short NivToDbB[] = {
-597, -596, -596, -595, -594, -593, -592, -592, -591, -590, -589, -588, -588, -587, -586, -585, -585, -584, -583, -583, 
-582, -581, -580, -580, -579, -578, -578, -577, -576, -576, -575, -574, -574, -573, -572, -572, -571, -571, -570, -569, 
-569, -568, -567, -567, -566, -566, -565, -565, -564, -563, -563, -562, -562, -561, -561, -560, -559, -559, -558, -558, 
-557, -557, -556, -556, -555, -555, -554, -554, -553, -553, -552, -552, -551, -551, -550, -550, -549, -549, -548, -548, 
-547, -547, -546, -546, -545, -545, -544, -544, -543, -543, -543, -542, -542, -541, -541, -540, -540, -539, -539, -539, 
-538, -538, -537, -537, -536, -536, -536, -535, -535, -534, -534, -534, -533, -533, -532, -532, -532, -531, -531, -530, 
-530, -530, -529, -529, -528, -528, -528, -527, -527, -527, -526, -526, -525, -525, -525, -524, -524, -524, -523, -523, 
-523, -522, -522, -521, -521, -521, -520, -520, -520, -519, -519, -519, -518, -518, -518, -517, -517, -517, -516, -516, 
-516, -515, -515, -515, -514, -514, -514, -513, -513, -513, -512, -512, -512, -511, -511, -511, -511, -510, -510, -510, 
-509, -509, -509, -508, -508, -508, -507, -507, -507, -507, -506, -506, -506, -505, -505, -505, -504, -504, -504, -504, 
-503, -503, -503, -502, -502, -502, -502, -501, -501, -501, -501, -500, -500, -500, -499, -499, -499, -499, -498, -498, 
-498, -497, -497, -497, -497, -496, -496, -496, -496, -495, -495, -495, -495, -494, -494, -494, -494, -493, -493, -493, 
-493, -492, -492, -492, -492, -491, -491, -491, -491, -490, -490, -490, -490, -489, -489, -489, -489, -488, -488, -488, 
-488, -487, -487, -487, -487, -486, -486, -486, -486, -485, -485, -485, -485, -485, -484, -484, -484, -484, -483, -483, 
-483, -483, -482, -482, -482, -482, -482, -481, -481, -481, -481, -480, -480, -480, -480, -480, -479, -479, -479, -479, 
-479, -478, -478, -478, -478, -477, -477, -477, -477, -477, -476, -476, -476, -476, -476, -475, -475, -475, -475, -475, 
-474, -474, -474, -474, -473, -473, -473, -473, -473, -472, -472, -472, -472, -472, -471, -471, -471, -471, -471, -470, 
-470, -470, -470, -470, -470, -469, -469, -469, -469, -469, -468, -468, -468, -468, -468, -467, -467, -467, -467, -467, 
-466, -466, -466, -466, -466, -466, -465, -465, -465, -465, -465, -464, -464, -464, -464, -464, -464, -463, -463, -463, 
-463, -463, -462, -462, -462, -462, -462, -462, -461, -461, -461, -461, -461, -460, -460, -460, -460, -460, -460, -459, 
-459, -459, -459, -459, -459, -458, -458, -458, -458, -458, -458, -457, -457, -457, -457, -457, -457, -456, -456, -456, 
-456, -456, -456, -455, -455, -455, -455, -455, -455, -454, -454, -454, -454, -454, -454, -453, -453, -453, -453, -453, 
-453, -452, -452, -452, -452, -452, -452, -451, -451, -451, -451, -451, -451, -451, -450, -450, -450, -450, -450, -450, 
-449, -449, -449, -449, -449, -449, -449, -448, -448, -448, -448, -448, -448, -447, -447, -447, -447, -447, -447, -447, 
-446, -446, -446, -446, -446, -446, -446, -445, -445, -445, -445, -445, -445, -445, -444, -444, -444, -444, -444, -444, 
-443, -443, -443, -443, -443, -443, -443, -442, -442, -442, -442, -442, -442, -442, -441, -441, -441, -441, -441, -441, 
-441, -441, -440, -440, -440, -440, -440, -440, -440, -439, -439, -439, -439, -439, -439, -439, -438, -438, -438, -438, 
-438, -438, -438, -438, -437, -437, -437, -437, -437, -437, -437, -436, -436, -436, -436, -436, -436, -436, -436, -435, 
-435, -435, -435, -435, -435, -435, -434, -434, -434, -434, -434, -434, -434, -434, -433, -433, -433, -433, -433, -433, 
-433, -433, -432, -432, -432, -432, -432, -432, -432, -432, -431, -431, -431, -431, -431, -431, -431, -431, -430, -430, 
-430, -430, -430, -430, -430, -430, -429, -429, -429, -429, -429, -429, -429, -429, -428, -428, -428, -428, -428, -428, 
-428, -428, -427, -427, -427, -427, -427, -427, -427, -427, -427, -426, -426, -426, -426, -426, -426, -426, -426, -425, 
-425, -425, -425, -425, -425, -425, -425, -425, -424, -424, -424, -424, -424, -424, -424, -424, -423, -423, -423, -423, 
-423, -423, -423, -423, -423, -422, -422, -422, -422, -422, -422, -422, -422, -422, -421, -421, -421, -421, -421, -421, 
-421, -421, -421, -420, -420, -420, -420, -420, -420, -420, -420, -420, -419, -419, -419, -419, -419, -419, -419, -419, 
-419, -418, -418, -418, -418, -418, -418, -418, -418, -418, -418, -417, -417, -417, -417, -417, -417, -417, -417, -417, 
-416, -416, -416, -416, -416, -416, -416, -416, -416, -416, -415, -415, -415, -415, -415, -415, -415, -415, -415, -414, 
-414, -414, -414, -414, -414, -414, -414, -414, -414, -413, -413, -413, -413, -413, -413, -413, -413, -413, -413, -412, 
-412, -412, -412, -412, -412, -412, -412, -412, -412, -411, -411, -411, -411, -411, -411, -411, -411, -411, -411, -410, 
-410, -410, -410, -410, -410, -410, -410, -410, -410, -409, -409, -409, -409, -409, -409, -409, -409, -409, -409, -408, 
-408, -408, -408, -408, -408, -408, -408, -408, -408, -408, -407, -407, -407, -407, -407, -407, -407, -407, -407, -407, 
-406, -406, -406, -406, -406, -406, -406, -406, -406, -406, -406, -405, -405, -405, -405, -405, -405, -405, -405, -405, 
-405, -405, -404, -404, -404, -404, -404, -404, -404, -404, -404, -404, -404, -403, -403, -403, -403, -403, -403, -403, 
-403, -403, -403, -403, -402, -402, -402, -402, -402, -402, -402, -402, -402, -402, -402, -401, -401, -401, -401, -401, 
-401, -401, -401, -401, -401, -401, -400, -400, -400, -400, -400, -400, -400, -400, -400, -400, -400, -400, -399, -399, 
-399, -399, -399, -399, -399, -399, -399, -399, -399, -398, -398, -398, -398, -398, -398, -398, -398, -398, -398, -398, 
-398, -397, -397, -397, -397, -397, -397, -397, -397, -397, -397, -397, -397, -396, -396, -396, -396, -396, -396, -396, 
-396, -396, -396, -396, -396, -395, -395, -395, -395, -395, -395, -395, -395, -395, -395, -395, -395, -394, -394, -394, 
-394, -394, -394, -394, -394, -394, -394, -394, -394, -393, -393, -393, -393, -393, -393, -393, -393, -393, -393, -393, 
-393, -393, -392, -392, -392, -392, -392, -392, -392, -392, -392, -392, -392, -392, -391, -391, -391, -391, -391, -391, 
-391, -391, -391, -391, -391, -391, -391, -390, -390, -390, -390, -390, -390, -390, -390, -390, -390, -390, -390, -390, 
-389, -389, -389, -389, -389, -389, -389, -389, -389, -389, -389, -389, -389, -388, -388, -388, -388, -388, -388, -388, 
-388, -388, -388, -388, -388, -388, -387, -387, -387, -387, -387, -387, -387, -387, -387, -387, -387, -387, -387, -386, 
-386, -386, -386, -386, -386, -386, -386, -386, -386, -386, -386, -386, -386, -385, -385, -385, -385, -385, -385, -385, 
-385, -385, -385, -385, -385, -385, -384, -384, -384, -384, -384, -384, -384, -384, -384, -384, -384, -384, -384, -384, 
-383, -383, -383, -383, -383, -383, -383, -383, -383, -383, -383, -383, -383, -383, -382, -382, -382, -382, -382, -382, 
-382, -382, -382, -382, -382, -382, -382, -382, -381, -381, -381, -381, -381, -381, -381, -381, -381, -381, -381, -381, 
-381, -381, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -380, -379, -379, -379, 
-379, -379, -379, -379, -379, -379, -379, -379, -379, -379, -379, -378, -378, -378, -378, -378, -378, -378, -378, -378, 
-378, -378, -378, -378, -378, -378, -377, -377, -377, -377, -377, -377, -377, -377, -377, -377, -377, -377, -377, -377, 
-377, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -376, -375, -375, -375, -375, 
-375, -375, -375, -375, -375, -375, -375, -375, -375, -375, -375, -374, -374, -374, -374, -374, -374, -374, -374, -374, 
-374, -374, -374, -374, -374, -374, -374, -373, -373, -373, -373, -373, -373, -373, -373, -373, -373, -373, -373, -373, 
-373, -373, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -372, -371, -371, 
-371, -371, -371, -371, -371, -371, -371, -371, -371, -371, -371, -371, -371, -371, -370, -370, -370, -370, -370, -370, 
-370, -370, -370, -370, -370, -370, -370, -370, -370, -370, -369, -369, -369, -369, -369, -369, -369, -369, -369, -369, 
-369, -369, -369, -369, -369, -369, -369, -368, -368, -368, -368, -368, -368, -368, -368, -368, -368, -368, -368, -368, 
-368, -368, -368, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, -367, 
-366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -366, -365, -365, -365, 
-365, -365, -365, -365, -365, -365, -365, -365, -365, -365, -365, -365, -365, -365, -364, -364, -364, -364, -364, -364, 
-364, -364, -364, -364, -364, -364, -364, -364, -364, -364, -364, -363, -363, -363, -363, -363, -363, -363, -363, -363, 
-363, -363, -363, -363, -363, -363, -363, -363, -363, -362, -362, -362, -362, -362, -362, -362, -362, -362, -362, -362, 
-362, -362, -362, -362, -362, -362, -362, -361, -361, -361, -361, -361, -361, -361, -361, -361, -361, -361, -361, -361, 
-361, -361, -361, -361, -361, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, -360, 
-360, -360, -360, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, -359, 
-359, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, -358, 
-357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -357, -356, -356, 
-356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -356, -355, -355, -355, 
-355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -355, -354, -354, -354, 
-354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -354, -353, -353, -353, -353, 
-353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -353, -352, -352, -352, -352, 
-352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -352, -351, -351, -351, -351, 
-351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -351, -350, -350, -350, -350, 
-350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -350, -349, -349, -349, -349, 
-349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -349, -348, -348, -348, 
-348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -348, -347, -347, 
-347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -347, -346, 
-346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, -346, 
-345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, -345, 
-345, -345, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, -344, 
-344, -344, -344, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, -343, 
-343, -343, -343, -343, -343, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, -342, 
-342, -342, -342, -342, -342, -342, -342, -342, -341, -341, -341, -341, -341, -341, -341, -341, -341, -341, -341, -341, 
-341, -341, -341, -341, -341, -341, -341, -341, -341, -341, -340, -340, -340, -340, -340, -340, -340, -340, -340, -340, 
-340, -340, -340, -340, -340, -340, -340, -340, -340, -340, -340, -340, -340, -339, -339, -339, -339, -339, -339, -339, 
-339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -339, -338, -338, -338, -338, 
-338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, -338, 
-337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, -337, 
-337, -337, -337, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, -336, 
-336, -336, -336, -336, -336, -336, -336, -335, -335, -335, -335, -335, -335, -335, -335, -335, -335, -335, -335, -335, 
-335, -335, -335, -335, -335, -335, -335, -335, -335, -335, -335, -334, -334, -334, -334, -334, -334, -334, -334, -334, 
-334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -334, -333, -333, -333, -333, 
-333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, -333, 
-333, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, -332, 
-332, -332, -332, -332, -332, -332, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, 
-331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -331, -330, -330, -330, -330, -330, -330, -330, -330, -330, 
-330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -330, -329, -329, -329, 
-329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, -329, 
-329, -329, -329, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, -328, 
-328, -328, -328, -328, -328, -328, -328, -328, -328, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, 
-327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -327, -326, -326, -326, -326, -326, 
-326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, -326, 
-326, -326, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, -325, 
-325, -325, -325, -325, -325, -325, -325, -325, -325, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, 
-324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -324, -323, -323, -323, 
-323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, -323, 
-323, -323, -323, -323, -323, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, 
-322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -322, -321, -321, -321, -321, -321, -321, -321, 
-321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, -321, 
-321, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -320, 
-320, -320, -320, -320, -320, -320, -320, -320, -320, -320, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, 
-319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -319, -318, 
-318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, -318, 
-318, -318, -318, -318, -318, -318, -318, -318, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, 
-317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -317, -316, -316, 
-316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, -316, 
-316, -316, -316, -316, -316, -316, -316, -316, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, 
-315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -315, -314, 
-314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, -314, 
-314, -314, -314, -314, -314, -314, -314, -314, -314, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, 
-313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, -313, 
-313, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, 
-312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -312, -311, -311, -311, -311, -311, -311, -311, -311, 
-311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, -311, 
-311, -311, -311, -311, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, 
-310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -310, -309, -309, -309, -309, 
-309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, -309, 
-309, -309, -309, -309, -309, -309, -309, -309, -309, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, 
-308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, -308, 
-308, -308, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, 
-307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -307, -306, -306, -306, -306, -306, 
-306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, -306, 
-306, -306, -306, -306, -306, -306, -306, -306, -306, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, 
-305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, -305, 
-305, -305, -305, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, 
-304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -304, -303, -303, 
-303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, 
-303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -303, -302, -302, -302, -302, -302, -302, -302, 
-302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, -302, 
-302, -302, -302, -302, -302, -302, -302, -302, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, 
-301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, -301, 
-301, -301, -301, -301, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, 
-300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, -300, 
-299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, 
-299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -299, -298, -298, -298, 
-298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, 
-298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -298, -297, -297, -297, -297, -297, -297, 
-297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, 
-297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -297, -296, -296, -296, -296, -296, -296, -296, -296, -296, 
-296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, -296, 
-296, -296, -296, -296, -296, -296, -296, -296, -296, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, 
-295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, -295, 
-295, -295, -295, -295, -295, -295, -295, -295, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, 
-294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, -294, 
-294, -294, -294, -294, -294, -294, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, 
-293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, -293, 
-293, -293, -293, -293, -293, -293, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, 
-292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, -292, 
-292, -292, -292, -292, -292, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, 
-291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, -291, 
-291, -291, -291, -291, -291, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, 
-290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, -290, 
-290, -290, -290, -290, -290, -290, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, 
-289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, -289, 
-289, -289, -289, -289, -289, -289, -289, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, 
-288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, -288, 
-288, -288, -288, -288, -288, -288, -288, -288, -288, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, 
-287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, 
-287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -287, -286, -286, -286, -286, -286, -286, -286, -286, -286, 
-286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, 
-286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -286, -285, -285, -285, -285, -285, -285, -285, 
-285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, 
-285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -285, -284, -284, -284, -284, 
-284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, 
-284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, -284, 
-283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, 
-283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, -283, 
-283, -283, -283, -283, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, 
-282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, -282, 
-282, -282, -282, -282, -282, -282, -282, -282, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, 
-281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, 
-281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -281, -280, -280, -280, -280, -280, -280, -280, 
-280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, 
-280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -280, -279, 
-279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, 
-279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, -279, 
-279, -279, -279, -279, -279, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, 
-278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, 
-278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -278, -277, -277, -277, -277, -277, -277, -277, -277, 
-277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, 
-277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -277, -276, 
-276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, 
-276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, -276, 
-276, -276, -276, -276, -276, -276, -276, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, 
-275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, 
-275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -275, -274, -274, -274, -274, -274, 
-274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, 
-274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, -274, 
-274, -274, -274, -274, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, 
-273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, 
-273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -273, -272, -272, -272, -272, -272, -272, -272, 
-272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, 
-272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, -272, 
-272, -272, -272, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, 
-271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, 
-271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -271, -270, -270, -270, -270, -270, -270, 
-270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, 
-270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, -270, 
-270, -270, -270, -270, -270, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, 
-269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, 
-269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -269, -268, -268, -268, -268, 
-268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, 
-268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, -268, 
-268, -268, -268, -268, -268, -268, -268, -268, -268, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, 
-267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, 
-267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, -267, 
-267, -267, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, 
-266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, 
-266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -266, -265, -265, -265, -265, -265, 
-265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, 
-265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, -265, 
-265, -265, -265, -265, -265, -265, -265, -265, -265, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, 
-264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, 
-264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, -264, 
-264, -264, -264, -264, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, 
-263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, 
-263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, -263, 
-262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, 
-262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, 
-262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -262, -261, -261, -261, -261, 
-261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, 
-261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, 
-261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -261, -260, -260, -260, -260, -260, -260, -260, -260, 
-260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, 
-260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -260, 
-260, -260, -260, -260, -260, -260, -260, -260, -260, -260, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, 
-259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, 
-259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, -259, 
-259, -259, -259, -259, -259, -259, -259, -259, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, 
-258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, 
-258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, -258, 
-258, -258, -258, -258, -258, -258, -258, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, 
-257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, 
-257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, -257, 
-257, -257, -257, -257, -257, -257, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, 
-256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256, -256
};


