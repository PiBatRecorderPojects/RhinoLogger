/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

//------------------------------------------------
// Regroupement de différents utilitaires

#include <stdio.h>
#include <Arduino.h>
#include <SD.h>
#include "wiring_private.h"
#include "const.h"


#ifndef UTILIT_H
#define UTILIT_H

// Chaines de caractères générique
extern char sTempA[255], sTempB[255], sTempC[255];

//------------------------------------------------
// Retourne la taille de la mémoire libre
int GetFreeRam ();


//----------------------------------------------------
// Classe de gestion d'un GSM type SIM800L
// Permet l'envoie d'un SMS
class GSMManager
{
  public:
    //----------------------------------------------------
    // Constructeur du gestionnaire
    GSMManager();

    // Liste des séquences à exécuter par le GSM
    enum SEQUENCESGSM
    {
      // GSM en standby
      STANDBYGSM    = 0,  // GSM en attente d'une séquence à exécuter
      // Séquences de réveil du GSM
      STARTGSM      = 1,  // Mise en route du GSM avec attente 10s
      TESTATI       = 2,  // Envoie de la commande ATI et attente retour SIM800
      TESTPIN       = 3,  // Envoi de la commande AT+CPIN? pour vérifier si le code PIN est déjà connue
      ENTERPIN      = 4,  // Envoi du code PIN avec la commande AT+CPIN=
      ATTENTERESEAU = 5,  // Test présence réseau pendant 30s
      TESTOPERATEUR = 6,  // Test de l'opérateur
      TESTSIGNAL    = 7,  // Test de l'intensité du signal
      STARTGSMOK    = 8,  // Démarrage GSM OK
      // Séquences d'émission d'un SMS
      STARTSMS      = 10,  // Passage en mode SMS
      NUMEROSMS     = 11,  // Numérotation du destinataire
      TXTSMS        = 12,  // Texte du SMS
      SMSOK         = 13,  // Emission SMS OK
      // Séquences de passage en GPRS
      SETGPRS       = 20,  // Passage en GPRS
      CONFGPRS      = 21,  // Configuration du mode GPRS (nom du point d'accès ou APN)
      OPENGPRS      = 22,  // Ouverture du mode GPRS
      TESTGPRS      = 23,  // Test de l'adresse IP du mode GPRS
      GPRSOK        = 24,  // Mode GPRS OK
      // Séquences d'émission d'un mail avec pièce jointe
      STARTMAIL     = 30,  // Passage en mode mail
      TIMEOUTMAIL   = 31,  // Init du timeout
      SETSSLMAIL    = 32,  // Init du mode SSL
      SMTPMAIL      = 33,  // Init de serveur SMTP
      COMPTEMAIL    = 34,  // Adresse mail et mot de passe du compte
      MAILFROM      = 35,  // Adresse mail de l'émetteur
      MAILTO        = 36,  // Adresse du destinataire
      SUJETMAIL     = 37,  // Sujet du mail
      LENGTHMAIL    = 38,  // Taille en caractères du corps du message
      TXTMAIL       = 39,  // Texte du mail
      FILENAME      = 40,  // Nom de la pièce jointe
      SENDMAIL      = 41,  // Emission du mail
      DATAFILE      = 42,  // Contenue de la pièce jointe
      ENDMAIL       = 43,  // Fin de l'émission du mail
      // Séquences d'arrêt du mode GPRS
      STOPGPRS      = 50,  // Arret du mode GPRS
      STOPGPRSOK    = 51,  // Mode GPRS stoppé
      // Séquences à exécuter pour l'arrêt du GSM
      RAZSMS        = 60,  // RAZ des SMS reçus et envoyés
      WAKEUP_GSM    = 61,  // Extinction du GSM
      WAKEUPGSMOK   = 62,  // Extinction du GSM OK
      // GSM en erreur
      ERRORGSM      = 100  // GSM en erreur
    };
    
    //----------------------------------------------------
    // Démarre le GSM (Mise à 1 de la broche Enable du régulateur)
    // Lance la séquence suivante en tâche de fond :
    // - Réveil du GSM (Enable à 1)
    // - Répétition de la commande ATI pour vérifier la présence du GSM
    // - Saisie éventuel du code PIN
    // - Vérification opérateur et niveau
    // La séquence est terminée lorsque IsWakeupOK retourne true ou lorsque IsError retourne true
    void StartGSM(
      char *pCodePin  // Code PIN (4 chiffres) de la carte SIM
      );
    
    //----------------------------------------------------
    // Envoie du SMS vers un n°
    // La séquence exécutée est la suivante :
    // - Envoie du SMS
    // La séquence est terminée lorsque IsSMSOK retourne true ou lorsque IsError retourne true
    void SendSMS(
      char *pNumTel,  // Numéro de téléphone destinataire
      char *pTxtSMS   // Texte du SMS
      );
    
    //----------------------------------------------------
    // Passage en mode GPRS
    // La séquence exécutée est la suivante :
    // - Ouverture du mode GPRS et configuration du point d'accès (APN)
    // - Test de l'adresse IP
    // La séquence est terminée lorsque IsGPRS retourne true
    void StartGPRS(
      char *pAPN  // Pointeur sur le nom du point d'accès
      );
    
    //----------------------------------------------------
    // Passage en mode Emission d'un mail
    // La séquence exécutée est la suivante :
    // - Ouverture du mode GPRS et configuration du point d'accès (APN)
    // - Test de l'adresse IP
    // La séquence est terminée lorsque IsSendMail retourne true
    void SendMail(
      char *pAdrCompte,   // Pointeur sur l'adresse du compte
      char *pMotDePasse,  // Pointeur sur le mot de passe du compte
      char *pSujet,       // Pointeur sur le sujet
      char *pTexte,       // Pointeur sur le texte du mail
      char *pFileName     // Pointeur sur le nom du fichier joint
      );
    
    //----------------------------------------------------
    // Arret du mode GPRS
    // La séquence est terminée lorsque IsGPRS retourne false
    void StopGPRS();
    
    //----------------------------------------------------
    // Arret du GSM, on coupe l'alimentation en mettant Enable du régulateur à 0)
    // La séquence exécutée est la suivante :
    // - Effacement des SMS reçus ou envoyés
    // - Arret de l'alimentation
    // La séquence est terminée lorsque IsStarted retourne false ou lorsque IsError retourne true
    void StopGSM();
    
    //----------------------------------------------------
    // Exécution des commandes GSM
    // Retourne true si la commande est correctement exécutée
    bool CommandeGSM(
      char *psCmd,        // Commande à exécuter (NULL si aucune)
      char *psReponseA,   // Réponse A à tester (NULL si aucune)
      char *psReponseB,   // Réponse B à tester (NULL si aucune)
      int iSeqSuivanteA,  // Séquence suivante si la réponse A est OK
      int iSeqSuivanteB,  // Séquence suivante si la réponse B est OK
      int iTempoSuivante, // Tempo avant la prochaine commande en ms
      int iTempoErreur,   // Tempo en cas d'erreur avant la prochaine tentative
      int iNbBoucles      // Nombre max de boucle avant erreur
      );
      
    //----------------------------------------------------
    // A appeler régulièrement pour exécuter les séquences GSM demandées
    void GestionGSM();
    
    //----------------------------------------------------
    // Retourne true si le GSM est démarré (true) ou stoppé (false)
    bool IsStarted() {return bStart;};
    
    //----------------------------------------------------
    // Retourne true si le code PIN est OK
    bool IsPinOK() {return bPinOK;};
    
    //----------------------------------------------------
    // Retourne true si le réseau est bon
    bool IsNetwork() {return bNetwork;};
    
    //----------------------------------------------------
    // Retourne true si le SMS est envoyé
    bool IsSMSOK() {return bSMSOK;};
    
    //----------------------------------------------------
    // Retourne le nom de l'opérateur
    char *GetOperatorName() {return OpName;};
    
    //----------------------------------------------------
    // Retourne la qualité du signal de réception
    // De 0 à 8 (0 pas de réception, 8 réception excellente)
    int GetSignalQuality() {return iSignalQ;};

    //----------------------------------------------------
    // Retourne true si la séquence de démarrage est terminée
    bool IsWakeupOK() {return bWakeup;};
    
    //----------------------------------------------------
    // Retourne true si la séquence démarrée est en erreur
    bool IsError() {return bError;};
    
    //----------------------------------------------------
    // Retourne true si le mode GPRS est actif
    bool IsGPRS() {return bGPRS;};
    
    //----------------------------------------------------
    // Retourne la séquence courrante
    int GetSequence() {return iSeqGSM;};
    
    //----------------------------------------------------
    // Retourne l'éventuelle séquence en erreur
    int GetSequenceError() {return iSeqError;};
    
    //----------------------------------------------------
    // Retourne le code de retour de l'émission du mail
    int GetiCodeMail() {return iCodeMail;};

    //----------------------------------------------------
    // Retourne une éventuelle chaine d'erreur GSM
    char *GetErrorGSM() {return sErrorGSM;};
    
  protected:
    //----------------------------------------------------
    // Lecture de la ligne série du GSM
    // Retourne le nombre de caractères lus
    int LectureGSM(
      char *psReponse,    // Pointeur sur la chaine de reponse
      int iMaxChar,       // Taille de la chaine de reponse
      int iDelayMax,      // Temps d'attente max en secondes
      ATTENTEGSM attente=ATT_OK  // Type de fin de chaine attendue
      );

  int iSeqGSM;        // Séquence en cours d'exécution
  int iSeqError;      // Eventuelle séquence en erreur
  int iTimeFinSeq;    // Temps de fin d'attente d'une temporisation en ms
  int iBouclesMax;    // Nombre de boucles autorisées pour cette séquenec pour attendre un résultat correct
  int iBoucles;
  int iCodeMail;      // Code de retour d'émission du mail
  bool bStart;        // Indique si le GSM est démarré (true) ou stoppé (false)
  bool bPinOK;        // Indique si le code PIN est OK
  bool bNetwork;      // Indique si le réseau est bon
  bool bWakeup;       // Indique que la séquence de réveil est bonne
  bool bError;        // Indique une erreur dans la séquence demandée
  char OpName[15];    // Nom de l'opérateur
  bool bSMSOK;        // Indique si le SMS est envoyé
  bool bGPRS;         // Indique si le mode GPRS est actif
  bool bMailOK;       // Indique si l'émission du mail est OK
  int  iSignalQ;      // Qualité du signal
  char *pCPin;        // Pointeur sur le code Pin
  char *pNumero;      // Pointeur sur le numéro de téléphone à appeler
  char *pTexteSMS;    // Pointeur sur le texte du SMS à envoyer
  char *psAPN;        // Pointeur sur le nom du point d'accès
  char *psAdrCompte;  // Pointeur sur l'adresse du compte
  char *psMotDePasse; // Pointeur sur le mot de passe du compte
  char *psSujet;      // Pointeur sur le sujet
  char *psTexte;      // Pointeur sur le texte du mail
  char *psFileName;   // Pointeur sur le nom du fichier joint
  //Uart *pGSM;
  char sErrorGSM[255];// Eventuelle description erreur GSM...
};

//------------------------------------------------
// Procédure de lecture d'une ligne dans un fichier
// ReadFile, le fichier à lire
// cLine, buffer de réception de la ligne
// iSize, taille du buffer en caractères
// Retourne le nombre de caractères lus
byte ReadLine(File ReadFile, char *cLine, byte iSize);

//------------------------------------------------
// Utilitaires pour la gestion de l'horloge externe

//------------------------------------------------
// Init de la date et de l'heure sur l'horloge externe
void setDS3231time(
  byte second,      // Secondes
  byte minute,      // Minutes
  byte hour,        // Heures
  byte day,         // Jour du mois
  byte month,       // Mois
  byte year);       // Année (format raccourci)

//------------------------------------------------
// Lecture de la date et de l'heure sur l'horloge externe
void readDS3231time(
  byte *second,      // Secondes
  byte *minute,      // Minutes
  byte *hour,        // Heures
  byte *day,         // Jour du mois
  byte *month,       // Mois
  byte *year);       // Année (format raccourci)


/*
  Copyright (c) 2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Concatenate after macro expansion
#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define Flash(name, size) \
  __attribute__((__aligned__(256))) \
  static const uint8_t PPCAT(_data,name)[(size+255)/256*256] = { }; \
  FlashClass name(PPCAT(_data,name), size);

#define FlashStorage(name, T) \
  __attribute__((__aligned__(256))) \
  static const uint8_t PPCAT(_data,name)[(sizeof(T)+255)/256*256] = { }; \
  FlashStorageClass<T> name(PPCAT(_data,name));

class FlashClass {
public:
  FlashClass(const void *flash_addr = NULL, uint32_t size = 0);

  void write(const void *data) { write(flash_address, data, flash_size); }
  void erase()                 { erase(flash_address, flash_size);       }
  void read(void *data)        { read(flash_address, data, flash_size);  }

  void write(const volatile void *flash_ptr, const void *data, uint32_t size);
  void erase(const volatile void *flash_ptr, uint32_t size);
  void read(const volatile void *flash_ptr, void *data, uint32_t size);

private:
  void erase(const volatile void *flash_ptr);

  const uint32_t PAGE_SIZE, PAGES, MAX_FLASH, ROW_SIZE;
  const volatile void *flash_address;
  const uint32_t flash_size;
};

template<class T>
class FlashStorageClass {
public:
  FlashStorageClass(const void *flash_addr) : flash(flash_addr, sizeof(T)) { };

  // Write data into flash memory.
  // Compiler is able to optimize parameter copy.
  inline void write(T data)  { flash.erase(); flash.write(&data); }

  // Read data from flash into variable.
  inline void read(T *data)  { flash.read(data); }

  // Overloaded version of read.
  // Compiler is able to optimize copy-on-return.
  inline T read() { T data; read(&data); return data; }

private:
  FlashClass flash;
};



/*
  dtostrf - Emulation for dtostrf function from avr-libc
  Copyright (c) 2015 Arduino LLC.  All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

char *dtostrf(double val, int width, unsigned int prec, char *sout);


#endif
