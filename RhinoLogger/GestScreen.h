/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Classe de gestion de l'afficheur OLED

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH1106.h>
#include "const.h"
#include "utilit.h"

// Type de modificateurs
enum MODIF_TYPES {
  MODIF_DATE   =  0,  // Modificateur de type date
  MODIF_HEURE  =  1,  // Modificateur de type heure
  MODIF_CHAR   =  2,  // Modificateur pour plusieurs caractères de ., a à z, A à Z et 0 à 9
  MODIF_DIGIT  =  3,  // Modificateur pour plusieurs caractères numériques de 0 à 9
  MODIF_DIGITB =  4,  // Modificateur pour plusieurs caractères numériques de 0 à 9 plus blanc
  MODIF_SMS    =  5,  // Modificateur pour le test SMS
  MODIF_GAIN   =  6,  // Modificateur du gain numérique
  MODIF_TEST   =  7,  // Modificateur du mode du signal de test
  MODIF_VEILLE =  8,  // Modificateur pour le passage en mode veille
  MODIF_RET    =  9,  // Modificateur pour le retour en mode mesure
  MODIF_BANDE  = 10,  // Modificateur pour une bande, char pour le nom, digit pour le reste
  MODIF_RAZ    = 11,  // Modificateur RAZ des données
  MODIF_PARAMD = 12,  // Modificateur de remise aux paramètres par défaut
  NO_MODIF     = 13   // Pas de modificateur, ligne vide
};

// Liste des indices des modificateurs
enum MODIF_LIST {
  IDX_RETOUR  =  0,  // Retour vers les mesures
  IDX_VEILLE  =  1,  // Modificateur de mise en veille
  IDX_DATE    =  2,  // Modificateur de la date
  IDX_HEURE   =  3,  // Modificateur de l'heure
  IDX_CODEPIN =  4,  // Modificateur du code pin
  IDX_NUMA    =  5,  // Modificateur du n° de téléphone A
  IDX_NUMB    =  6,  // Modificateur du n° de téléphone B
  IDX_NUMC    =  7,  // Modificateur du n° de téléphone C
  IDX_SMS     =  8,  // Modificateur du test SMS
  IDX_SEUIL   =  9,  // Modificateur du seuil relatif et QFC
  IDX_GAIN    = 10,  // Modificateur du gain numérique
  IDX_COEFEXT = 11,  // Modificateur du coefficient de mesure de la batterie externe
  IDX_COEFINT = 12,  // Modificateur du coefficient de mesure de la batterie interne
  IDX_NOM     = 13,  // Modificateur du nom de station
  IDX_TEST    = 14,  // Modificateur du signal de test
  IDX_FRFM    = 15,  // Modificateur des fréquences et SMS de la bande FM (nom modifiable)
  IDX_FRGR    = 16,  // Modificateur des fréquences et SMS de la bande GR
  IDX_FRRE    = 17,  // Modificateur des fréquences et SMS de la bande RE
  IDX_FREP    = 18,  // Modificateur des fréquences et SMS de la bande EP
  IDX_FRPR    = 19,  // Modificateur des fréquences et SMS de la bande PR
  IDX_RAZ     = 20,  // Modificateur RAZ des données
  IDX_PARAMD  = 21,  // Modificateur de remise aux paramètres par défaut
  IDX_APN     = 22,  // Modificateur du point d'accès
  IDX_SMTP    = 23,  // Modificateur du serveur SMTP
  IDX_PORT    = 24,  // Modificateur du port SMTP
  IDX_COMPTE  = 25,  // Modificateur du compte
  IDX_ADRESSE = 26,  // Modificateur de l'adresse
  IDX_VIDE    = 27   // Ligne vide
};

//----------------------------------------------------
// Classe de gestion de l'affichage OLED
class GestScreen
{
  public:
    //----------------------------------------------------
    // Constructeur du gestionnaire d'écran OLED
    GestScreen(
      StructParams *pPar // Pointeur sur les paramètres
      );

    //----------------------------------------------------
    // Initialisation de l'écran
    void InitScreen();

    //----------------------------------------------------
    // Affichage du logo de départ
    void AffLogo();

    //----------------------------------------------------
    // Affichage du mode veille
    void AffVeille();

    //----------------------------------------------------
    // Affichage de la page d'infos
    void AffInfos(
      StructAllMesures *pMesures // Pointeur sur les mesures
      );

    //----------------------------------------------------
    // Affichage d'une ligne de paramètres
    void AffLigneParam(
      int iNumLigne,      // Indice de la ligne
      char *psLigne,      // Pointeur sur la chaine de la ligne
      char *psChamps,     // Chaine avec des x symbolisant l'emplacement des modifications (X début d'un champ x, suite d'un même champ)
      MODIF_LIST idx,     // Indice du modificateur courant
      MODIF_TYPES ModType // Type du modificateur courant
      );

    //----------------------------------------------------
    // Affichage d'infos du bruit et de la carte SD
    void AffBruitAndSD(
      StructNoiseAndSD   *pMesNoiseSD, // Pointeur sur les mesures
      StructAllMesures   *pMesures     // Pointeur sur les mesures
      );

    //----------------------------------------------------
    // Affichage des paramètres page 1
    void AffParamsP1();

    //----------------------------------------------------
    // Affichage des paramètres page 2
    void AffParamsP2(
      StructAllMesures   *pMesures // Pointeur sur les mesures
      );

    //----------------------------------------------------
    // Affichage des paramètres page 3
    void AffParamsP3();

    //----------------------------------------------------
    // Affichage des paramètres page 4
    void AffParamsP4();

    //----------------------------------------------------
    // Affichage du mode attente Reset
    void AffAttenteReset();

    //----------------------------------------------------
    // Affichage du mode SMS
    void AffSMS(
      bool bTest,           // True pour indiquer un test SMS, false pour un SMS journalier
      GSMManager *pGestSMS  // Pointeur sur le gestionnaire de GSM
      );

    //----------------------------------------------------
    // Affichage en mode debug des 7 niveaux max
    void AffDebugNivMax(
      StructDebug *pMesDebug, // Infos de debug à afficher
      short iBruitMoyen       // Bruit moyen
      );

    //----------------------------------------------------
    // Affichage en mode debug des fréquences par lot de 7
    void AffDebugFreq(
      StructDebFr *pMesDebug, // Infos de debug à afficher
      short iBruitMoyen       // Bruit moyen
      );

    //----------------------------------------------------
    // RAZ de l'écran
    void RAZDisplay();

    //----------------------------------------------------
    // Stoppe l'affichage
    void StopDisplay();

    //----------------------------------------------------
    // Force l'affichage
    void StartDisplay();

    //----------------------------------------------------
    // Indique si l'affichage est actif
    bool IsActif();

    //----------------------------------------------------
    // Indique si un champ est en modification
    bool IsModif();

    //----------------------------------------------------
    // Passe en modification d'un paramètre
    // Retourne true si on passe en modif
    bool SetModifOn();

    //----------------------------------------------------
    // Valide la modification d'un paramètre
    void ValidModif();

    //----------------------------------------------------
    // On passe au paramètre suivant
    // Retourne true si on doit passer à la page suivante
    bool ScrollDown();

    //----------------------------------------------------
    // On passe au paramètre précédent
    // Retourne true si on doit passer à la page précédente
    bool ScrollUp();

    //----------------------------------------------------
    // On passe au caractère suivant
    void ScrollRight();

    //----------------------------------------------------
    //On passe au caractère précédent
    void ScrollLeft();

    //----------------------------------------------------
    // On décrémente le paramètre en modification
    void ModifDown();

    //----------------------------------------------------
    // On incrémente le paramètre en modification
    void ModifUp();

    //----------------------------------------------------
    // Retourne la ligne active
    int GetLigneActive() {return iLigneActive;};

    //----------------------------------------------------
    // Init de la ligne active
    void SetLigneActive(
      int iLine = 0 // Ligne active (0 par défaut)
      );

    //----------------------------------------------------
    // Retourne le texte associé au signal de test
    char *GetStrTest(
      SIGNAL_TEST iTest // Valeur du signal de test
      );

    //----------------------------------------------------
    // Retourne le texte associé au gain numérique
    char *GetStrGain(
      GAINUM iGain // Valeur du gain
      );
  
    //----------------------------------------------------
    // Retourne true si le mode Test SMS est actif
    bool GetTestSMS() {return bTestSMS;};

    //----------------------------------------------------
    // Retourne true si on est en mode veille
    int GetModeVeille() {return bModeVeille;};

    //----------------------------------------------------
    // Stoppe le mode Test SMS
    void StopTestSMS();

    //----------------------------------------------------
    // Retourne true si la dernière modif concerne un coefficient
    bool GetModifCoeff() {return bModifCoeff;};
    
    //----------------------------------------------------
    // Retourne true s'il faut effacer les données
    // Positionne ensuite toujours bIsRAZ à false
    bool isRAZDatas();
    
    //----------------------------------------------------
    // Initialise les paramètres aux valeurs par défaut
    void InitParamsDefaut();

  protected:
    // Gestionnaire de l'écran OLED (sans reset)
    Adafruit_SH1106 screen;

    // Pointeur sur les paramètres
    StructParams *pParams;
    
    // Indique si l'init de l'écran est OK
    bool bInit;

    // Indique si on est en modif d'un paramètre
    bool bModif;

    // Indique si on est en test SMS
    bool bTestSMS;

    // Indique si on est en mode veille
    bool bModeVeille;

    // Indique si l'affichage est actif (en mode normal, l'affichage se coupe à la 1ère transition d'un minute)
    bool bActif;

    // Indique si la dernière modification concerne un coefficient
    bool bModifCoeff;

    // Ligne et caractère en mode modification (0 si pas de modif)
    int iModifY, iModifX;

    // Ligne active
    int iLigneActive;

    // Chaine avec des x symbolisant l'emplacement des modifications (X début d'un champ x, suite d'un même champ)
    // pour la ligne active en cours de modif
    char sChamps[35];

    // Chaîne de la ligne en cours de modification
    char sLigneModif[35];

    // Type du modificateur courant
    MODIF_TYPES ModifType;

    // Indice du modificateur courant
    MODIF_LIST idxModif;

    // Valeurs des paramètres qui seront modifiées
    int mHH, mMN, mSS, mJJ, mMM, mYY, mNbEch, mSMS, mDebug;
    SIGNAL_TEST mSignalTest;
    GAINUM iGain;
    bool bVeille;
    bool bParamDef, bIsRAZ;

    // Numéro du champ en modification
    int iChamp;

    // Minute d'arrivée d'une erreur
    int iMnErreur;
    // Sauvegarde de la dernière erreur
    char sErreur[MAXLINE];

    // Mémo du pointeur sur les mesures courantes
    StructAllMesures *pmesures;
};

