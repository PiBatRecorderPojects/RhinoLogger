/*
RhinoLogger Copyright (c) 2016 Vrignault Jean-Do.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/////////////////////////////////////////////////////////
// Définition de la classe de getsion des timers
// et des séquences temporelles du programme
// N'effectue aucun traitement, il faut redéfinir
// la classe et les méthodes de gestion des différentes
// séquences pour y effectuer des traitements

#include "timer.h"

// Pointeur sur les éventuelles instances des trois timers
ZeroTimer *pTimer3 = NULL;
ZeroTimer *pTimer4 = NULL;
ZeroTimer *pTimer5 = NULL;

//----------------------------------------------------
// Constructeur du timer
ZeroTimer::ZeroTimer(
  uint8_t  tn,         // N° du timer (3, 4 ou 5) Attention le 4 semble utilisé pour la fonction micros()
  uint16_t prescaler,  // Prédivision de l'horloge
  uint8_t  period)     // n ms x 5.587 (approximativement) pour un prescaler TC_CTRLA_PRESCALER_DIV64
{
    // Mémo des infos du timer
    _timernum  = tn;
    _prescaler = prescaler;
    _period    = period;
}

//----------------------------------------------------
// Initialisation de nouvelles valeurs pour le prescaler et la période
void ZeroTimer::SetPeriod(
  uint16_t prescaler,  // Prédivision de l'horloge
  uint8_t  period)     // n ms x 5.587 (approximativement) pour un prescaler TC_CTRLA_PRESCALER_DIV64
{
    // Mémo des infos du timer
    _prescaler = prescaler;
    _period    = period;
}
      
//----------------------------------------------------
// Fonction d'initialisation du timer
// Retourne true si le timer est OK
bool ZeroTimer::initTimer()
{
  switch (_timernum)
  {
    case 3:
      return InitTimer3();
    case 4:
      return InitTimer4();
    case 5:
      return InitTimer5();
    default:
      return false;
  }
}

//----------------------------------------------------
// Fonction d'initialisation du timer 3
// Retourne true si le timer est OK
bool ZeroTimer::InitTimer3()
{
  if (pTimer3 == NULL)
  {
    pTimer3 = this;
    // Set up the generic clock (GCLK4) used to clock timers
    REG_GCLK_GENDIV = GCLK_GENDIV_DIV(64) |         // Divide the 48MHz clock source by divisor 128
                      GCLK_GENDIV_ID(4);            // Select Generic Clock (GCLK) 4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    REG_GCLK_GENCTRL = GCLK_GENCTRL_IDC |           // Set the duty cycle to 50/50 HIGH/LOW
                       GCLK_GENCTRL_GENEN |         // Enable GCLK4
                       GCLK_GENCTRL_SRC_DFLL48M |   // Set the 48MHz clock source
                       GCLK_GENCTRL_ID(4);          // Select GCLK4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    // Feed GCLK4 to TC2 and TC3
    REG_GCLK_CLKCTRL = GCLK_CLKCTRL_CLKEN |         // Enable GCLK4 to TC2 and TC3
                       GCLK_CLKCTRL_GEN_GCLK4 |     // Select GCLK4
                       GCLK_CLKCTRL_ID_TCC2_TC3;    // Feed the GCLK4 to TC2 and TC3
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
   
    REG_TC3_CTRLA |= TC_CTRLA_MODE_COUNT8;          // Set the counter to 8-bit mode
    while (TC3->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    REG_TC3_COUNT8_CC0 = 0xFF;                      // Max
    while (TC3->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC3_COUNT8_CC1 = 0xFF;                      // Max
    while (TC3->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC3_COUNT8_PER = _period;
    while (TC3->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    NVIC_SetPriority(TC3_IRQn, 0);    // Set the Nested Vector Interrupt Controller (NVIC) priority for TC3 to 0 (highest)
    NVIC_EnableIRQ(TC3_IRQn);         // Connect TC3 to Nested Vector Interrupt Controller (NVIC)
  
    REG_TC3_INTFLAG |= TC_INTFLAG_MC1 | TC_INTFLAG_MC0 | TC_INTFLAG_OVF;        // Clear the interrupt flags
    REG_TC3_INTENSET = TC_INTENSET_MC1 | TC_INTENSET_MC0 | TC_INTENSET_OVF;     // Enable TC3 interrupts
    return true;
  }
  else
    return false;
}

//----------------------------------------------------
// Fonction d'initialisation du timer 4
// Retourne true si le timer est OK
bool ZeroTimer::InitTimer4()
{
  if (pTimer4 == NULL)
  {
    pTimer4 = this;
    // Set up the generic clock (GCLK4) used to clock timers
    REG_GCLK_GENDIV = GCLK_GENDIV_DIV(64) |        // Divide the 48MHz clock source by divisor 128
                      GCLK_GENDIV_ID(4);            // Select Generic Clock (GCLK) 4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    REG_GCLK_GENCTRL = GCLK_GENCTRL_IDC |           // Set the duty cycle to 50/50 HIGH/LOW
                       GCLK_GENCTRL_GENEN |         // Enable GCLK4
                       GCLK_GENCTRL_SRC_DFLL48M |   // Set the 48MHz clock source
                       GCLK_GENCTRL_ID(4);          // Select GCLK4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    // Feed GCLK4 to TC4 and TC5
    REG_GCLK_CLKCTRL = GCLK_CLKCTRL_CLKEN |         // Enable GCLK4 to TC4 and TC5
                       GCLK_CLKCTRL_GEN_GCLK4 |     // Select GCLK4
                       GCLK_CLKCTRL_ID_TC4_TC5;     // Feed the GCLK4 to TC4 and TC5
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
   
    REG_TC4_CTRLA |= TC_CTRLA_MODE_COUNT8;           // Set the counter to 8-bit mode
    while (TC4->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    REG_TC4_COUNT8_CC0 = 0xFF;                      // Max
    while (TC4->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC4_COUNT8_CC1 = 0xFF;                      // Max
    while (TC4->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC4_COUNT8_PER = _period;
    while (TC4->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    NVIC_SetPriority(TC4_IRQn, 0);    // Set the Nested Vector Interrupt Controller (NVIC) priority for TC4 to 0 (highest)
    NVIC_EnableIRQ(TC4_IRQn);         // Connect TC4 to Nested Vector Interrupt Controller (NVIC)
  
    REG_TC4_INTFLAG |= TC_INTFLAG_MC1 | TC_INTFLAG_MC0 | TC_INTFLAG_OVF;        // Clear the interrupt flags
    REG_TC4_INTENSET = TC_INTENSET_MC1 | TC_INTENSET_MC0 | TC_INTENSET_OVF;     // Enable TC4 interrupts
    return true;
  }
  else
    return false;
}

//----------------------------------------------------
// Fonction d'initialisation du timer 5
// Retourne true si le timer est OK
bool ZeroTimer::InitTimer5()
{
  if (pTimer5 == NULL)
  {
    pTimer5 = this;
    // Set up the generic clock (GCLK4) used to clock timers
    REG_GCLK_GENDIV = GCLK_GENDIV_DIV(64) |         // Divide the 48MHz clock source by divisor 128
                      GCLK_GENDIV_ID(4);            // Select Generic Clock (GCLK) 4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    REG_GCLK_GENCTRL = GCLK_GENCTRL_IDC |           // Set the duty cycle to 50/50 HIGH/LOW
                       GCLK_GENCTRL_GENEN |         // Enable GCLK4
                       GCLK_GENCTRL_SRC_DFLL48M |   // Set the 48MHz clock source
                       GCLK_GENCTRL_ID(4);          // Select GCLK4
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
  
    // Feed GCLK4 to TC4 and TC5
    REG_GCLK_CLKCTRL = GCLK_CLKCTRL_CLKEN |         // Enable GCLK4 to TC4 and TC5
                       GCLK_CLKCTRL_GEN_GCLK4 |     // Select GCLK4
                       GCLK_CLKCTRL_ID_TC4_TC5;     // Feed the GCLK4 to TC4 and TC5
    while (GCLK->STATUS.bit.SYNCBUSY);              // Wait for synchronization
   
    REG_TC5_CTRLA |= TC_CTRLA_MODE_COUNT8;           // Set the counter to 8-bit mode
    while (TC5->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    REG_TC5_COUNT8_CC0 = 0xFF;                      // Max
    while (TC5->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC5_COUNT8_CC1 = 0xFF;                      // Max
    while (TC5->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
    REG_TC5_COUNT8_PER = _period;
    while (TC5->COUNT8.STATUS.bit.SYNCBUSY);        // Wait for synchronization
  
    NVIC_SetPriority(TC5_IRQn, 0);    // Set the Nested Vector Interrupt Controller (NVIC) priority for TC5 to 0 (highest)
    NVIC_EnableIRQ(TC5_IRQn);         // Connect TC4 to Nested Vector Interrupt Controller (NVIC)
  
    REG_TC5_INTFLAG |= TC_INTFLAG_MC1 | TC_INTFLAG_MC0 | TC_INTFLAG_OVF;        // Clear the interrupt flags
    REG_TC5_INTENSET = TC_INTENSET_MC1 | TC_INTENSET_MC0 | TC_INTENSET_OVF;     // Enable TC5 interrupts
    return true;
  }
  else
    return false;
}

//----------------------------------------------------------------
// Lance ou stoppe le timer
void ZeroTimer::enable(
  bool bEnable
  )
{
  // Test s'il y a vraiment un changement
  if (_bEnable != bEnable)
  {
    switch (_timernum)
    {
      case 3:
        if (bEnable)
          REG_TC3_CTRLA |= _prescaler | TC_CTRLA_ENABLE;   // Enable TC3
        else
          REG_TC3_CTRLA &= ~TC_CTRLA_ENABLE;              // Disable TC3
        while (TC3->COUNT8.STATUS.bit.SYNCBUSY);          // Wait for synchronization
        break;
      case 4:
        if (bEnable)
          REG_TC4_CTRLA |= _prescaler | TC_CTRLA_ENABLE;   // Enable TC4
        else
          REG_TC4_CTRLA &= ~TC_CTRLA_ENABLE;              // Disable TC4
        while (TC4->COUNT8.STATUS.bit.SYNCBUSY);          // Wait for synchronization
        break;
      case 5:
        if (bEnable)
          REG_TC5_CTRLA |= _prescaler | TC_CTRLA_ENABLE;   // Enable TC5
        else
          REG_TC5_CTRLA &= ~TC_CTRLA_ENABLE;              // Disable TC5
        while (TC5->COUNT8.STATUS.bit.SYNCBUSY);          // Wait for synchronization
        break;
    }
    _bEnable = bEnable;
  }
}

//----------------------------------------------------------------
// CallBack générale du Timer TC3
void TC3_Handler()
{
    // Check for overflow (OVF) interrupt
    if (TC3->COUNT8.INTFLAG.bit.OVF && TC3->COUNT8.INTENSET.bit.OVF)             
      REG_TC3_INTFLAG = TC_INTFLAG_OVF;         // Clear the OVF interrupt flag
  
    // Check for match counter 0 (MC0) interrupt
    if (TC3->COUNT8.INTFLAG.bit.MC0 && TC3->COUNT8.INTENSET.bit.MC0)             
      REG_TC3_INTFLAG = TC_INTFLAG_MC0;         // Clear the MC0 interrupt flag
  
    // Check for match counter 1 (MC1) interrupt
    if (TC3->COUNT8.INTFLAG.bit.MC1 && TC3->COUNT8.INTENSET.bit.MC1)           
      REG_TC3_INTFLAG = TC_INTFLAG_MC1;        // Clear the MC1 interrupt flag
  
    if (pTimer3 != NULL)
      // Appel de la méthode de traitement virtuelle
      pTimer3->timerHandler();
}

//----------------------------------------------------------------
// CallBack générale du Timer TC4
void TC4_Handler()
{     
    // Check for overflow (OVF) interrupt
    if (TC4->COUNT8.INTFLAG.bit.OVF && TC4->COUNT8.INTENSET.bit.OVF)             
      REG_TC4_INTFLAG = TC_INTFLAG_OVF;         // Clear the OVF interrupt flag
  
    // Check for match counter 0 (MC0) interrupt
    if (TC4->COUNT8.INTFLAG.bit.MC0 && TC4->COUNT8.INTENSET.bit.MC0)             
      REG_TC4_INTFLAG = TC_INTFLAG_MC0;         // Clear the MC0 interrupt flag
  
    // Check for match counter 1 (MC1) interrupt
    if (TC4->COUNT8.INTFLAG.bit.MC1 && TC4->COUNT8.INTENSET.bit.MC1)           
      REG_TC4_INTFLAG = TC_INTFLAG_MC1;        // Clear the MC1 interrupt flag
  
    if (pTimer4 != NULL)
      // Appel de la méthode de traitement virtuelle
      pTimer4->timerHandler();
}

//----------------------------------------------------------------
// CallBack générale du Timer TC5
void TC5_Handler()
{     
    // Check for overflow (OVF) interrupt
    if (TC5->COUNT8.INTFLAG.bit.OVF && TC5->COUNT8.INTENSET.bit.OVF)             
      REG_TC5_INTFLAG = TC_INTFLAG_OVF;         // Clear the OVF interrupt flag
  
    // Check for match counter 0 (MC0) interrupt
    if (TC5->COUNT8.INTFLAG.bit.MC0 && TC5->COUNT8.INTENSET.bit.MC0)             
      REG_TC5_INTFLAG = TC_INTFLAG_MC0;         // Clear the MC0 interrupt flag
  
    // Check for match counter 1 (MC1) interrupt
    if (TC5->COUNT8.INTFLAG.bit.MC1 && TC5->COUNT8.INTENSET.bit.MC1)           
      REG_TC5_INTFLAG = TC_INTFLAG_MC1;        // Clear the MC1 interrupt flag

    if (pTimer5 != NULL)
      // Appel de la méthode de traitement virtuelle
      pTimer5->timerHandler();
}

//----------------------------------------------------
// Fonction virtuelle exécutée sur l'évenement timer
void ZeroTimer::timerHandler()
{
  
}
