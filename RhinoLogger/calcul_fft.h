#ifndef CALCUL_FFT_H
#define CALCUL_FFT_H

/*
  fix_fft() - perform forward/inverse fast Fourier transform.
  fr[n],fi[n] are real and imaginary arrays, both INPUT AND
  RESULT (in-place FFT), with 0 <= n < 2**m.
*/
int fix_fft(short fr[], short fi[], int m);


#endif 

